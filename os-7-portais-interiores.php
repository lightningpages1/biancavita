<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <?php require('default/headerIniciantesVsl.php'); ?>
    <title>Os 7 Portais Interiores</title>
    <link rel="stylesheet" href="css/portaisInteriores/preload.css">
    <link rel="stylesheet" id="delayedcss" href="css/portaisInteriores/delayed.css" disabled>
</head>

<body>
    <script src="vendor/lightbox/fslightbox.js"></script>
    <?php require('contents/portaisInteriores_content.php'); ?>
    <!-- Swiper JS -->
    <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>

    <!-- Initialize Swiper -->
    <script>
        var swiper = new Swiper(".mySwiper", {
            centeredSlides: true,
            loop: true,
            slidesPerView: "1",
            spaceBetween: 10,
            breakpoints: {
                765: {slidesPerView: "3"}
            },
            navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
            },
            autoplay: {
                slidesPerView: 1,
                disableOnInteraction: false,
            },
        });

        var swiperCellphone = new Swiper(".swiperCellphone", {
            grabCursor: true,
            centeredSlides: true,
            loop: true,
            slidesPerView: "1",
            navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
            },
            autoplay: {
                slidesPerView: 1,
                disableOnInteraction: false,
            },
        });
    </script>

    <script>
        let vw = window.innerWidth * 0.01;
        // document.documentElement.style.setProperty('--vh', `${vh}px`);
        document.documentElement.style.setProperty('--vw', `${vw}px`);
    </script>

    <!-- ANIMACOES -->
    <script>
        const observer = new IntersectionObserver(entries => {
            entries.forEach(entry => {
                if (entry.isIntersecting) {
                    if (entry.target.classList.contains('set-transition-down-up')) {
                        entry.target.classList.add('transition-down-up');
                    }
                }
            });
        });
        
        // OBERVERS TRANSITIONS
        observer.observe(document.querySelector('#setTransition'));
        observer.observe(document.querySelector('#secdSection'));

        // ReadyFunction
        document.onreadystatechange = function () {
            if (document.readyState == "interactive") {
                cssToActivate = document.getElementById('delayedcss')
                cssToActivate.removeAttribute('disabled');
            }
        }

    </script>

    <!-- LIGHTBOX -->
    <script>
        const lightbox = new FsLightbox();

        // set up props, like sources, types, events etc.
        lightbox.props.type = 'image';
        lightbox.props.sources = ['assets/portaisInteriores/reportagemSuperInteressante.webp', 'assets/portaisInteriores/reportagemGE.webp', 'assets/portaisInteriores/reportagemVeja.webp', 'assets/portaisInteriores/reportagemUNA.webp', 'assets/portaisInteriores/reportagemG1.webp', 'assets/portaisInteriores/reportagemSuperInteressante2.webp'];
        lightbox.props.onInit = () => console.log('Lightbox initialized!');

        function openLightBox(sequence) {
            lightbox.open(sequence);
        }
    </script>

</body>

<?php require('default/footer_v2.php'); ?>

</html>