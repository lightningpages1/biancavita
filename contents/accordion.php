<div class="tab">
  <input id="tab-1" type="checkbox">
  <label for="tab-1">Será que o treinamento Yoga Para Iniciantes é para mim?</label>
  <div class="content text-weight-medium"><p>SIM! O treinamento Yoga Para Iniciantes foi feito para ensinar qualquer pessoa que deseje se aperfeiçoar na prática da Yoga, independente do nível de experiência.</p></div>
</div>

<div class="tab">
  <input id="tab-2" type="checkbox">
  <label for="tab-2">E se eu não estiver qualificada para praticar Yoga?</label>
  <div class="content text-weight-medium"><p>
        Qualquer pessoa pode fazer Yoga! Não importa a sua idade, a sua condição financeira, ou até mesmo o corpo que você tenha. 
        <br><br> 
        A Yoga é uma prática para todas as pessoas, independente da sua preparação física ou qualquer outra “desculpa” que você possa pensar ser um impeditivo.
    </p></div>
</div>

<div class="tab">
  <input id="tab-3" type="checkbox">
  <label for="tab-3">Minhas informações pessoais estão protegidas? Posso comprar sem medo?</label>
  <div class="content text-weight-medium"><p>SIM! Nós utilizamos uma plataforma com criptografia inteligente, e seus dados não serão salvos em nenhum local. É só realizar a compra, e pronto. Você já terá seu acesso e forma 100% segura.</p></div>
</div>

<div class="tab">
  <input id="tab-4" type="checkbox">
  <label for="tab-4">Como funciona a garantia? Tenho 7 dias sem pagar nada por isso?</label>
  <div class="content text-weight-medium"><p>SIM! Você terá 7 dias de garantia e não pagará 1 centavo por esse período de teste. Se não gostar por qualquer motivo que seja, ou achar que não quer emagrecer mais, é só enviar um email para o seguinte contato: <b>contato@biancavita.com</b></p></div>
</div>

<div class="tab">
  <input id="tab-5" type="checkbox">
  <label for="tab-5">Em quanto tempo posso esperar resultados visíveis?</label>
  <div class="content text-weight-medium"><p>
    O resultado que alguém pode ter através da Yoga depende de vários fatores, inclusive do que você espera atingir em quanto tempo. 
    <br><br>
    Algumas alunas veem resultados satisfatórios na primeira prática, outras apenas após algumas semanas.
  </p></div>
</div>

<div class="tab">
  <input id="tab-6" type="checkbox">
  <label for="tab-6">Como vou acessar o conteúdo do treinamento?</label>
  <div class="content text-weight-medium"><p>Assim que você realizar a compra por cartão de crédito, o acesso será enviado diretamente para você através do email cadastrado no momento da sua aquisição. Conteúdos especiais normalmente ficam na área de membros.</p></div>
</div>

<div class="tab">
  <input id="tab-7" type="checkbox">
  <label for="tab-7">Quais são as formas de pagamento?</label>
  <div class="content text-weight-medium"><p>Você pode realizar o pagamento através do cartão de crédito ou boleto bancário. Através do cartão, você recebe o acesso imediato. Através do boleto, você recebe o seu acesso em até 72h, assim que o pagamento for confirmado.</p></div>
</div>