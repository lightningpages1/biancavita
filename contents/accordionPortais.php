<div class="tab">
  <input id="tab-1" type="checkbox">
  <label for="tab-1">Preciso entender alguma coisa de Yoga, Meditação ou Chakras para as aulas?</label>
  <div class="content text-weight-medium"><p>Com o meu método de aulas que você não precisa ter nenhum conhecimento antes de começar. São aulas para todos os níveis. Esse curso foi desenvolvido inclusive para os iniciantes que não tem nenhuma experiência.</p></div>
</div>

<div class="tab">
  <input id="tab-2" type="checkbox">
  <label for="tab-2">Por quanto tempo tenho acesso ao curso?</label>
  <div class="content text-weight-medium"><p>Você poderá praticar quando, onde e como quiser por 1 ano de acesso liberado.
    </p></div>
</div>

<div class="tab">
  <input id="tab-3" type="checkbox">
  <label for="tab-3">Será que esse curso é pra mim?</label>
  <div class="content text-weight-medium"><p>Esta dúvida é muito comum, e posso te afirmar que várias outras pessoas que tiveram essa dúvida, compraram e me disseram que valeu muito investir nele. E com a garantia incondicional de 7 dias, você pode degustar a vontade! … ou você fica super satisfeita(o) com o curso ou terá seu dinheiro 100% de volta!</p></div>
</div>

<div class="tab">
  <input id="tab-4" type="checkbox">
  <label for="tab-4">E se eu quiser cancelar a minha compra?</label>
  <div class="content text-weight-medium"><p>Dentro de 7 dias você pode cancelar fácil e por qualquer motivo. Sem nem precisar se explicar! Basta mandar um e-mail para o suporte do curso solicitando seu cancelamento e imediatamente, com apenas 1 clique, fazemos seu cancelamento e enviamos um e-mail.</p></div>
</div>

<div class="tab">
  <input id="tab-5" type="checkbox">
  <label for="tab-5">Consigo assistir pelo celular?</label>
  <div class="content text-weight-medium"><p>Com certeza! Tanto no computador ou celular funciona tranquilamente.</p></div>
</div>

<div class="tab">
  <input id="tab-6" type="checkbox">
  <label for="tab-6">E se eu tiver alguma dúvida ou dificuldade?</label>
  <div class="content text-weight-medium"><p>Sem problemas! Você poderá mandar entrar em contato comigo na área de membros com qualquer dúvida. Estou aqui para te ajudar!</p></div>
</div>

<div class="tab">
  <input id="tab-7" type="checkbox">
  <label for="tab-7">Depois de comprar o que eu faço?</label>
  <div class="content text-weight-medium"><p>Assim que efetuar sua compra e seu pagamento for confirmado você receberá no mesmo momento um e-mail com seu acesso ao curso liberado. Nosso sistema leva no máximo 48 horas para confirmar um pagamento (exceto boleto) e sua garantia só começa a valer a partir do momento que você receber o e-mail com suas informações de acesso.</p></div>
</div>

<div class="tab">
  <input id="tab-8" type="checkbox">
  <label for="tab-8">Paguei por boleto, quanto tempo demora para liberar a compra?</label>
  <div class="content text-weight-medium"><p>Os boletos levam, em média, 3 dias úteis para serem compensados e, após a compensação, será enviado automaticamente um e-mail com seu acesso ao curso.</p></div>
</div>

<div class="tab">
  <input id="tab-9" type="checkbox">
  <label for="tab-9">Não estou achando o email com os dados de acesso ao curso. O que eu faço?</label>
  <div class="content text-weight-medium"><p>As caixas de e-mail possuem abas e pastas que separam remetentes já conhecidos e remetentes novos, como o meu. Então, o email pode estar em uma dessas caixas ou na caixa de spam. Clique no local de pesquisa e procure por Bianca Vita para achar suas informações de acesso ao curso. Caso isso não resolva, mande um e-mail para: contato@biancavita.com</p></div>
</div>