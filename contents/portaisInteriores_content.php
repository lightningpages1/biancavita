<div class="container-fluid primeiroBackground positionRelative">

    <div class="row">
        <div class="col-2 col-md-4 col-xl-5"></div>
        <div class="col-8 col-md-4 col-xl-2 text-center mt-3">
            <img src="./assets/portaisInteriores/logoPortais.webp" alt="" class="card-img logoYoga">
        </div>
        <div class="col-2 col-md-4 col-xl-5"></div>
    </div>

    <div class="row mt-4 justify-content-center">
        <!-- <div class="col-md-2 col-xl-3"></div> -->
        <div class="col-md-8 col-xl-8 containerTitle text-color-white text-center">
            <span class="f-size-2-2 lh-1 text-weight-bolder montserrat titleText">
                Acesse os <span class="azulCiano">seus 7 portais<br class="hideBr showBr"> internos</span> através <br class="hideElement"> da<br class="hideBr showBr"> meditação e permita<br class="hideBr showBr"> que a sua vida <br class="hideElement"> flua<br class="hideBr showBr"> melhor!
            </span>
            <div class="f-size-1-1 mt-2 interSemi titleText2 containerTitle2">
                Aprenda como a prática da meditação<br class="hideBr showBr"> pode servir como a chave secreta para<br class="hideBr showBr"> você <br class="hideElement"> regular seu corpo e mente, mesmo<br class="hideBr showBr"> que você seja um iniciante na meditação.
            </div>
        </div>
        <!-- <div class="col-md-2 col-xl-3"></div> -->
    </div>

    <div class="row mt-4 set-transition-down-up justify-content-center" id="setTransition">
        <div class="col-md-8 col-lg-5 col-xl-5">
            <lite-vimeo class="cianShinyBorderButtons" videoid="736132303" title="Iniciar Vídeo"></lite-vimeo>
        </div>
    </div>


    <div class="row mt-4 justify-content-center">
        <!-- <div class="col-md-2 col-xl-3"></div> -->
        <div class=" text-center text-color-white">
            <div class="mt-1 f-size-1-3 montserrat lh-25px textVideo text-weight-medium">
                Descubra como meditar da forma correta para acessar seus 7 portais <br class="hideElement"> internos e aliviar os males do século como ansiedade, estresse, insônia e <br class="hideElement"> depressão.
            </div>
        </div>
        <!-- <div class="col-md-2 col-xl-3"></div> -->
    </div>

    <br><br>

</div>

<!-- Segundo Container Faltam Icones  -->
<div class="container-fluid segundoBackground positionRelative">
    <div class="row text-color-white justify-content-center align-items-center">
        <div class="containerIconesIniciais col-xl-2 col-lg-2 col-md-3 col-sm-12 d-flex justify-content-center align-items-center flex-column marginTop">
            <div>
                <div class="d-flex justify-content-center mb-2">
                    <img src="./assets/portaisInteriores/portaisInteriores/compraSegura.webp" alt="" class="card-img iconsSecondContainer">
                </div>
            </div>
            <div class="ms-2 containerHeight text-center">
                <p class="azulCiano montserrat f-size-1-7 text-weight-bolder lh-1">Compra segura</p>
                <p class="textosIconesIniciais text-weight-medium montserrat">Somos parceiros oficiais da<br class="hideText"> maior e melhor<br class="hideBr showBr"> plataforma de<br class="hideText"> treinamentos online do Brasil.</p>
            </div>
        </div>

        <div class="containerIconesIniciais col-xl-2 col-lg-2 col-md-2 col-sm-12 d-flex justify-content-center align-items-center flex-column marginTop">
            <div>
                <div class="d-flex justify-content-center mb-2">
                    <img src="./assets/portaisInteriores/portaisInteriores/experiencia.webp" alt="" class="card-img iconsSecondContainer">
                </div>
            </div>
            <div class="ms-2 containerHeight text-center">
                <p class="azulCiano montserrat f-size-1-7 text-weight-bolder lh-1">21 anos de experiência</p>
                <p class="textosIconesIniciais text-weight-medium montserrat">Aprenda com quem dedica<br class="hideText"> tempo, esforço e<br class="hideBr showBr"> recursos<br class="hideText"> financeiros para melhorar a<br class="hideText"><br class="hideBr showBr"> vida das brasileiras.</p>
            </div>
        </div>

        <div class="containerIconesIniciais col-xl-2 col-lg-2 col-md-2 col-sm-12 d-flex justify-content-center align-items-center flex-column marginTopNotebook">
            <div>
                <div class="d-flex justify-content-center mb-2">
                    <img src="./assets/portaisInteriores/portaisInteriores/notebook.webp" alt="" class="card-img iconsSecondContainer">
                </div>
            </div>
            <div class="ms-2 containerHeight text-center">
                <p class="azulCiano montserrat f-size-1-7 text-weight-bolder lh-1">100% online</p>
                <p class="textosIconesIniciais text-weight-medium montserrat">Acesse quando e onde quiser.<br class="hideText"> Você escolhe<br class="hideBr showBr"> onde praticar.</p>
            </div>
        </div>

        <div class="containerIconesIniciais col-xl-2 col-lg-2 col-md-2 col-sm-12 d-flex justify-content-center align-items-center flex-column marginTop">
            <div>
                <div class="d-flex justify-content-center mb-2">
                    <img src="./assets/portaisInteriores/portaisInteriores/gratis.webp" alt="" class="card-img iconsSecondContainer">
                </div>
            </div>
            <div class="ms-2 containerHeight text-center">
                <p class="azulCiano montserrat f-size-1-7 text-weight-bolder lh-1">7 dias grátis</p>
                <p class="textosIconesIniciais f-size-1 text-weight-medium montserrat">Sem contrato, sem burocracia.<br class="hideText"> São 7 dias de<br class="hideBr showBr"> Degustação para<br class="hideText"> você decidir se é para você.</p>
            </div>
        </div>

        <div class="containerIconesIniciais col-xl-2 col-lg-2 col-md-2 col-sm-12 d-flex justify-content-center align-items-center flex-column marginTop">
            <div>
                <div class="d-flex justify-content-center mb-2">
                    <img src="./assets/portaisInteriores/acessoImediatoIcon.webp" alt="" class="card-img iconsSecondContainer">
                </div>
            </div>
            <div class="ms-2 containerHeight text-center">
                <p class="azulCiano montserrat f-size-1-7 text-weight-bolder lh-1">Acesso por 1 ano</p>
                <p class="textosIconesIniciais text-weight-medium montserrat">12 meses de acesso completo<br class="hideText"> para começar.<br class="hideBr showBr"> A meditação te<br class="hideText"> espera.</p>
            </div>
        </div>
    </div>

    <br><br><br><br>

    <div class="elementor-shape elementor-shape-bottom" data-negative="false"> <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 3990.2 277.1" preserveAspectRatio="none"> <path class="elementor-shape-fill ha-shape-divider" style="fill: #fff" opacity="0.7" enable-background="new    " d="M0.1,197.8c12.1,1.7,23,3.5,33.8,5.3c0.9,0.1,1.9-0.1,2.6-0.8
 c6.5-6.2,17.2-9.5,27.2-8.2c0.3,0,0.7,0.1,1,0.3c1.6,0.7,3.1,1.5,4.7,2.2c1,0.5,2.3,0.3,3.2-0.4c11.5-8.5,35.2,1,46.8-7.3
 c1-0.7,2.3-0.7,3.3-0.1c11.8,7.4,28.3,10.1,42.8,6.8c1.2-0.3,2.4,0.3,3.2,1.3c3.7,5.1,13.4,6,21.3,5.7c7.7-0.3,17.2,0.1,21.6,4.7
 c1,1.1,2.7,1.2,3.9,0.4c1.2-0.8,2.3-1.6,3.5-2.3c1.3-0.9,3-0.6,4,0.5c4.7,5.6,12.2,9.6,20.5,10.9c2.4,0.4,4.9,0.6,6.7,1.9
 c2.7,1.9,2.6,5.3,4.2,7.9c2.6,4.2,10.5,4.9,14.8,2c1-0.7,2.4-0.7,3.4,0.1c0.6,0.4,1.1,0.8,1.7,1.2c1.3,0.9,3,0.7,4.1-0.5
 c1.1-1.2,2.1-2.4,3.2-3.6c1-1.2,2.7-1.4,4-0.5c1.9,1.3,3.9,2.6,5.9,3.9c1.5,1,3.6,0.5,4.4-1.2c2.9-5.9,11-10.1,18.9-9.2
 c9.7,1.1,18.7,8.6,27.4,8.8c2.3,0.1,3.8,2.6,2.6,4.6c0,0,0,0.1-0.1,0.1c-1.2,2,0.3,4.5,2.6,4.6c7.7,0.2,15.3,3.5,19.3,8.8
 c2.2,2.9,4.9,7,9.1,6.3c2.4-0.4,3.9-2.2,5.7-3.5c5.5-3.7,14.8-0.6,16.8,4.5c0.6,1.5,2.2,2.3,3.7,1.9c4.8-1.4,9.7-2.8,14.6-4.1
 c1-0.3,2.1,0,2.8,0.7c6.8,6.2,16.7,10.4,27.1,11.3c1.3,0.1,2.3,1,2.7,2.3c0.5,1.8,0.9,3.6,1.4,5.4c0.5,1.9,2.6,2.9,4.4,2
 c7.3-3.8,15.1-6.9,23.1-9.4c1.8-0.6,2.7-2.7,1.8-4.3c-1.6-3-0.3-7.6,2.9-10.1c4.2-3.5,10.1-5.7,13.2-9.7c0.6-0.8,1.4-1.2,2.4-1.3
 c0.8,0,1.6,0,2.4-0.1c1.2,0,2.2-0.7,2.7-1.8c0.5-1.2,1.1-2.3,1.6-3.5c0.3-0.6,0.8-1.1,1.4-1.4c11.4-5.8,24-10.1,37.2-12.6
 c14.5-12.2,35.4-19.5,56.5-19.7c2.7-0.1,5.4,0.1,8-0.4c2.7-0.5,5.2-1.5,7.8-2.3c2.7-0.8,4.7-3.1,6.8-4.6c0.6-0.4,1.2-0.8,2-0.7
 c1.7,0.2,1.8,2.5,3.4,2.8c1.8,0.2,3-2.4,4.7-1.8c0.6,0.2,1,0.9,1.6,1.1c1.6,0.6,3-1.8,4.8-1.5c0.6,0.1,1.1,0.6,1.6,0.9
 c0.7,0.4,1.6,0.5,2.5,0.6c2.9,0.4,5.9,0.8,8.8,1.2c2.9,0.4,6,0.8,8.6,2c2.1,1,3.8,2.5,4.5,4.3c0.3,0.8,1,1.3,1.8,1.2
 c4.4-0.6,8.9-0.2,13.1,1.1c-0.3,0.1-0.5,0.3-0.3,0.6c0.4,0.4,1.3,0.3,1.9,0.1c0.6-0.1,1.6,0,1.7,0.5c-0.8,0.3-1.6,0.8-2.1,1.4
 c2.6,0.3,5.3,0.4,7.9,0.4c1.1,0,1.9,1.1,1.6,2.2c-0.2,0.7-0.3,1.4-0.3,2.1c0,1.4,1.8,2.2,2.8,1.2c0,0,0,0,0,0
 c0.7-0.7,1.9-0.6,2.5,0.2c1.2,1.6,2.4,3.2,3.6,4.9c0.7,1.1,2.3,1,2.9-0.2c0-0.1,0.1-0.1,0.1-0.2c0.5-1,2-1.2,2.7-0.3
 c0.9,1,1.9,2.1,2.8,3.1c0.7,0.7,1.8,0.7,2.5-0.1c2.4-2.6,8-3.4,10.7-6.1c2-2,2.5-5.1,5.4-6.1c0.3-0.1,0.7-0.2,1.1-0.1
 c0.4,0.1,0.7,0.4,0.9,0.8c1.3,1.9,2.5,3.7,3.8,5.6c0.5,0.7,1,1.5,1.9,1.7c1.5,0.3,2.8-0.8,3.7-1.8c1.6-1.9,3.1-3.7,4.7-5.6
 c0.5-0.6,1-1.2,1.9-1.5c2.4-0.7,3.8,1.9,5.9,3c0.8,0.4,1.8,0.1,2.2-0.7c0.5-0.9,0.6-2,1-3c0.4-1.4,1.6-3.1,3.4-3
 c0.7,0,1.3,0.3,2,0.2c0.4-0.1,0.8-0.4,0.8-0.8c0-0.4,0-0.8,0.3-1.1c2.8-3.3,5.7-5.8,8.6-9.2c1.9-2.4,5.2-5,8.3-3.8
 c1.9,0.8,2.7,2.9,4.7,3.6c3.3,1.2,6.2-2.1,8.9-4c3.2-2.3,9.3-2.2,11.4,0.5c0.5,0.7,1.4,0.7,2.2,0.3c4.1-2.4,9.3-3.2,14.4-3.8
 c4.6-0.5,9.3-0.9,13.8,0.1c3.8,0.9,7.4,3.1,8.7,6.1c0.4,0.8,1.2,1.2,2.1,0.9c5.1-1.7,10.2-3.5,15.4-5.3c1.4-0.5,3-1,4.6-0.9
 c3.8,0.4,5.7,4.3,8.9,5.6c0.9,0.3,1.8-0.1,2.2-1c0.9-2.4,5.1-3.8,8.6-3.7c4.1,0.1,8.4,1.3,12.2,0.1c5.5-1.7,7-7,7.3-11.8
 c0.1-1,1-1.7,2-1.5c3.1,0.7,6,2,8.3,3.8c1.1,0.8,2.6,0.1,2.7-1.3c0.1-1.3,0-2.6-0.3-3.8c-0.2-0.9,0.4-1.9,1.4-2
 c2.1-0.2,4.2-1.8,4.9-3.6c1-2.3,0.7-4.9,1.4-7.2c1.3-4,5.9-7.2,11-7.7c0.8-0.1,1.5,0.4,1.8,1.1c1.7,5.3,2.6,10.8,2.5,16.3
 c0,0.9,0.7,1.7,1.6,1.7c7.3,0.4,14.7,0.5,22,0.2c0.7,0,1.3,0.3,1.5,0.9c1.9,3.8,5,7.8,9.8,7.4c2.1-0.2,3.9-1.2,6-1.6
 c6.2-1.1,11.2,3.8,14,8.3c0.8,1.3,1.9,2.8,3.6,3c1.1,0.2,2.3-0.2,3.4-0.6c3.8-1.3,7.5-2.7,11.3-4c4.6-1.6,8.1-3.6,12.1-5.8
 c0.5-0.3,1.2-0.3,1.7,0c2.5,1.6,5.9,2.2,9,2.6c15.2,2.1,30.5,3.6,45.8,4.5c2.1,0.1,4.4,0.1,6.1-0.9c2.1-1.3,2-3.6,1.3-5.8
 c-0.4-1.1,0.4-2.2,1.6-2.2c7.5-0.2,15.1-0.2,22.6-0.1c2.1,0,4.4,0.1,6.2,1c1.8,0.9,2.9,2.5,4.3,3.9c3.9,3.9,10.8,5.5,16.7,4.2
 c1-0.2,2.1,0.1,2.8,0.8c7.5,7.1,14.1,14.7,19.8,22.8c1.3,1.9,4.1,1.9,5.3-0.1c0.7-1.2,1.6-2.2,2.9-3c2.9-2,8.2-1.9,9.7,0.9
 c0.8,1.5,0.4,3.2,1.3,4.6c2.4,3.4,8.8,1.2,13.8,1.4c7.6,0.3,13,7.4,19.5,9.4c2.6,0.8,4.9-1.9,3.8-4.3c-0.4-0.9-0.9-1.7-1.3-2.6
 c-1.1-1.9,0.1-4.3,2.2-4.7c3.6-0.6,7-2.2,9.2-4.6c2.2-2.5,3.1-5.8,2.4-8.9c-0.5-2.5,1.8-4.6,4.2-3.7c3.1,1.2,6,3.8,8.6,5.8
 c4.9,3.7,11.9,5.4,18.5,4.6c4.7-0.6,9-2.3,13.6-3.2c12-2.2,24.4,1.9,36.6,1.2c13-0.7,27.8-6.3,37.9,0.2c-1.5-0.8-0.4-3,1.5-3.3
 c1.1-0.2,2.1,0,3.2,0.4c0.9,0.3,1.8-0.3,1.9-1.2c0.6-3.3,4.2-6.3,8.3-6.6c0.7-0.1,1.3,0.4,1.5,1.1c0.5,2,1.7,4,3.2,5.7
 c0.5,0.5,1.2,0.6,1.8,0.3c2.2-1.2,4.9-1.8,7.5-1.8c0.9,0,1.5,0.7,1.5,1.6c-0.1,2.1,0.2,4.2,1.1,6.1c0.4,0.9,1.6,1.1,2.3,0.5
 c3.5-3.1,8.1-5.6,13.3-5.8c4.9-0.2,9.9,2.6,11,6.2c0.3,0.9,1.4,1.3,2.2,0.8c2.8-1.9,6.6-2.8,10.2-2.5c0.8,0.1,1.5-0.5,1.6-1.2
 c0.3-1.8,0.5-3.5,0.8-5.3c0.1-0.9,1.1-1.5,2-1.2c4.7,1.7,10.7-1.1,15.1-3.7c5-2.9,12.6-5.6,16.7-1.8c2.9,2.6,7.4,6.2,11.6,5.2
 c4.2-1.1,7.5-3.5,11.5-4.9c5.4-1.8,11.4-1.6,17.2-1.4c9.6,0.4,19.7,0.9,27.7,5.1c3.6,1.9,7.4,4.6,11.6,3.9c2.7-0.5,4.8-2.3,7.4-2.8
 c3.3-0.7,6.6,0.8,9.7,1.9s7.4,1.5,9.4-0.7c0.9-0.9,1.3-2.2,2.4-2.8c2.6-1.6,6.3,0.8,9.5,0.2c2.8-0.6,3.9-3.2,5.8-5
 c4.6-4.2,12.7-2.3,19.2,0.3c0.9,0.4,2-0.3,2-1.3c0,0,0,0,0,0c0.1-1.1,1.2-1.7,2.1-1.2c4.9,2.7,12.6,1.2,17.9-1.7
 c5.8-3.2,10.5-7.7,17-9.7c0.5-0.2,1.1,0,1.5,0.4c2.7,2.8,5.9,5.3,9.6,7.2c1,0.5,2.1-0.2,2.2-1.3c0.1-3.4,0.4-6.9,1-10.4
 c0.1-0.7,0.8-1.3,1.6-1.2c7.5,0.5,14.9,0.9,22.3,1.3c4.7,0.3,10.4,0.1,13.1-2.7c0.5-0.6,1.4-0.7,2-0.2c2.1,1.7,1.7,5.8,4.8,6.4
 c1.3,0.3,2.7-0.3,3.9-0.9c2.8-1.3,5.7-2.6,8.6-4c0.7-0.3,1.5-0.1,1.8,0.5c2.4,3.9,6,7.4,10.2,10c2,1.3,4.7,2.5,7.1,1.8
 c3.1-0.8,4.1-3.8,6.2-5.8c2.8-2.5,7.6-3.1,11.7-2.2c4,0.9,7.6,2.9,11,4.8c7.2,4.2,14.4,8.3,21.6,12.4c3.8,2.2,8.7,4.5,13,3
 c9.1-3.2,21.1,0.9,24.4,8.3c0.3,0.8,1.2,1.2,2,0.8c2.5-1.2,5-2.4,7.4-3.5c0.6-0.3,1.3-0.1,1.8,0.4c1.2,1.4,2.1,2.9,2.8,4.4
 c0.4,0.8,1.4,1.2,2.2,0.7c2.8-1.8,6-3.5,9.6-3.1c2.2,0.3,4.1,1.3,6.1,2.1c5,1.9,11.1,1.7,16-0.4c2.7-1.1,4.9-4.1,3.9-6
 c-0.4-0.8,0.2-1.8,1.1-2c5.1-1.2,11-0.2,14.9,2.7c2.2,1.6,3.8,3.6,6.1,5.1c2.3,1.4,5.8,2.2,8.1,0.7c2.5-1.5,2.6-4.8,5.2-6.2
 c3.8-2,8.2,1.5,12.6,2.4c4,0.8,8.2-0.8,11.1-3.2c2.4-2.1,4-4.7,5.4-7.3c0.4-0.7,1.2-0.9,1.9-0.6c3.8,1.6,7.6,3.3,11.4,5
 c0-1.6,0.1-3.2,0.2-4.8c3.2-0.1,6.4-0.2,9.7-0.3c1.1,0,2-0.7,2.3-1.8c0,0,0,0,0,0c0.4-1.2,1.6-1.9,2.9-1.7c2.2,0.4,4.4,0.6,6.3-0.2
 c1.9-0.7,2.8-2.9,2.1-4.3c-0.6-1.2-0.2-2.7,1-3.3c1.8-1,4-1.3,6.3-0.9c2.6,0.4,4.8,1.7,6.1,3.4c0.8,1.1,2.2,1.3,3.4,0.7
 c2-1.2,4.3-2.1,6.7-2.7c1.2-0.3,2.7-0.5,3.7,0.2c2.4,1.7-1.8,4.3-2.6,6.8c-0.8,2.3,2,4.7,5,5.1c1.9,0.2,3.7,0,5.6-0.5
 c1.5-0.4,2.9,0.6,3.2,2c0.3,1.6,1.1,3,2.6,4.1c2.3,1.8,6.5,2.1,8.8,0.2c3.8-3.1,1.3-10,6.4-11.5c3.8-1.1,6.7,2.3,10.1,4.3
 c1.5,0.9,3.5,0,3.8-1.7c0.2-1.2,0.3-2.5,0.3-3.7c0-1.6,1.5-2.8,3.1-2.5c5.3,1.1,11.3,0.2,15.6-2.7c2.2-1.5,4.5-3.7,7.3-3.4
 c3.7,0.3,6.1,4.5,9.3,5c1.1,0.2,2.2-0.5,2.6-1.6c0.1-0.2,0.2-0.5,0.3-0.7c0.4-1.3,1.7-2,3-1.7c2.6,0.6,5.3,0.6,6.7-1
 c0.6-0.7,0.8-1.6,1.3-2.4c2-2.9,7.6-2.1,11.3-0.5s8.1,3.6,11.7,1.9c1.2-0.6,2.1-1.5,3.1-2.3c4.6-3.8,11.6-5.1,18.2-6.1
 c6.5-1,12.9-2.1,19.4-3.1c3.6-0.6,7.3-1.1,10.8-0.2c3.3,0.9,6.1,3.1,9.6,3.5c5.8,0.6,10.1-3.7,14.5-6.8c6.5-4.5,15-6.5,23.2-8.5
 c1.8-0.4,3.7-0.9,5.6-0.6c2.2,0.3,4,1.5,6,2.4c2.7,1.2,5.9,1.9,9,1.8c1.6,0,2.8-1.6,2.4-3.2c-0.5-1.9-1.9-4-1.1-5.9
 c0.8-1.9,3.5-2.7,5.6-3.8c3.7-2.1,5.6-6,5-9.5c-0.2-1.4,0.7-2.7,2.1-2.9c2.8-0.4,5.5-0.9,8.3-1.2c4.6-0.7,10.8,6.6,15,5
 c1.9-0.6,2.6-2.4,3.3-4c1.5-3.4,3.7-6.7,7.2-8.9c3.6-2.2,8.5-3.1,12.6-1.7c2.8,1,4.9,3,7.8,3.8c2.1,0.6,4.9-0.1,6-1.4
 c0.6-0.7,1.5-1,2.4-0.9c2.3,0.4,5.2-0.7,6.3-2.4c0.7-1,2-1.4,3.2-0.9c1.4,0.6,2.8,1.3,4.3,1.9c1.1,0.5,2.3,1.1,3.6,0.9
 c0.5,0,1-0.3,1.4-0.6c0.6-0.5,1.4-0.8,2.1-0.5c2.9,0.9,7.1-0.9,10.7-1.6c3.1-0.6,6.4-0.1,9.6,0.4c4.4,0.7,8.8,1.4,13.2,2.1
 c1.3,0.2,2.7,0.4,3.7,1.2c1.9,1.5,1.6,4.2,3.8,5.3c1.8,1,4.3,0.2,6.4-0.5c7.7-2.4,16.7-2.3,24.3,0.4c1.4,0.5,2,2.1,1.4,3.5
 c-0.8,1.7-1.4,3.4-1.9,5.2c-0.5,1.9,1.1,3.5,3,3.2c4-0.8,8.3-0.7,12.2,0.2c1.9,0.5,4,1.1,5.7,0.4c2.8-1.2,2.3-4.6,4.2-6.6
 c2.9-3.2,9-1.2,13.8-0.2c7.5,1.5,15.2-0.5,22.2-3.1c1-0.4,2.2-0.1,2.9,0.8c1.5,1.8,3,3.5,5.2,4.8c2.7,1.6,6.5,2.2,9.4,0.9
 c2.6-1.2,3.9-3.6,6.1-5.3c1.4-1,3.8-1.5,5.5-1.1c1,0.3,2.1-0.3,2.7-1.1c2.1-3,8.1-4.5,12.6-3c5.6,1.8,9,6.2,12.7,10
 c3.2,3.3,7.6,6.6,12.7,7c1.1,0.1,1.9,0.9,2.2,1.9c0.8,2.6,4.3,4.6,7.5,3.9c2.4-0.5,4.2-2,6.2-3.1c3.8-2.1,8.9-2.9,13.4-2.1
 c0.4,0.1,0.7,0.1,1.1,0c3.3-0.9,6.1-2.7,8.5-4.7c2.4-2.2,6.2-4,9.5-2.9c1.6,0.6,3.1,2,4.9,1.7c2.3-0.3,2.4-2.6,2.9-4.6
 c0.4-1.6,2.2-2.4,3.7-1.6c3.4,1.8,6.4,3.9,9,6.4c1.5,1.5,4.1,0.5,4.3-1.6c0.2-2.4,0.5-4.7,0.7-7.1c0.2-1.7,0.5-3.6,2.2-4.7
 c2.4-1.5,6-0.3,8.7-1.4c4.1-1.6,3.2-6.5,5.4-9.7c1.4-2,4.2-3.1,7.1-3.1c2.1,0,3.3,2.4,2,4.1c-0.9,1.2-1.5,2.4-1.8,3.7
 c-0.5,2.5,1,5.5,4.1,6.2c1.2,0.2,2.4,0.1,3.6,0.5c5.5,1.5,1.8,8.8,5.2,12.7c2.1,2.5,6.7,2.7,10.4,2.1s7.5-1.7,11.2-1.1
 c4.1,0.7,7.2,3.5,11.2,4.3c5.8,1.2,12.7-1.8,18-0.5c1.6,0.4,2.8,1.9,2.7,3.6c-0.2,3.5-0.1,7.1,1.9,10.2c2.4,3.8,8.4,6.4,13.1,4.4
 c2.5-1,4.2-3.2,6.9-4c3.2-0.9,6.9,1.2,7.7,3.7c0.2,0.7,1.1,0.8,1.6,0.3c0.8-0.8,1.5-1.6,2.2-2.4c0.3-0.3,0.7-0.4,1.1-0.2
 c1.6,0.8,4,0.5,5.3-0.6c0.4-0.4,1-0.3,1.4,0.2c2.4,3.4,7.1,5.8,12.1,6c0.1,0,0.3,0,0.4,0.1c5.2,2.7,14.4-1.4,18.5,2.3
 c2.1,1.9,2.5,5.4,5.5,5.6c2.9,0.3,4.6-2.9,7.5-3.4c1.1-0.2,2.3,0,3.4,0.2c18.5,3.7,37,7.4,55.5,11.1c5.2,1,10.7,2.2,14.1,5.5
 c2.8,2.7,3.6,6.3,5.1,9.6s4.3,6.7,8.6,7.3c5.2,0.6,10.3-3,15.3-1.7c1.7,0.5,3.2,1.5,4.5,2.6c5.9,4.6,11.7,9.3,17.4,14.1
 c0.6,0.5,1.4,0.1,1.5-0.6c0.5-3.5,0.9-7,1.4-10.4c0.1-0.4,0.4-0.8,0.9-0.8c0.6-0.1,1.3,0,1.9,0.1c0.7,0.1,1.3-0.5,1.1-1.2
 c0,0,0,0,0,0c-0.2-0.8,0.6-1.5,1.3-1.1c1.8,0.9,3.8,1.5,5.8,1.8c0.5,0.1,1-0.3,1.1-0.9c0.3-4.1,0.4-8.2,0.7-12.3
 c0-0.4,0.3-0.8,0.7-0.9c16.2-3.6,31.6-9.3,45.7-16.6c5.7-3,13.4-6.3,18.9-3c4.1,2.5,6.4,8.4,11.4,7.4c2.4-0.4,3.8-2.4,5.7-3.7
 c3.7-2.4,8.9-2.1,13.6-1.8c13.8,0.7,27.7-0.8,40.8-4.3c0.5-0.1,0.8-0.7,0.6-1.3c-0.7-1.9,1-4.2,3.4-5c2.7-0.7,5.6-0.1,8.1,1
 c0.3,0.1,0.5,0.4,0.5,0.8c0.2,2.6,6,4.9,9.4,4.7c3.6-0.1,7.4-1.2,10.6,0.1c4.1,1.7,4.7,6.1,6.3,9.5c1.7,3.4,7.6,6.5,10.4,3.5
 c1.1-1.2,1.1-3.1,2.6-4.1c3.9-2.5,8,4.4,12.9,3.9c4-0.4,4.7-5.2,8.4-6.7c3.2-1.4,7.1,0.5,9.2,2.9c2.1,2.3,3.5,5.2,6.2,7
 c2.5,1.6,6.9,1.8,8.6-0.1c0.3-0.4,0.9-0.4,1.3-0.1c1.2,1,2.6,1.8,4.3,2.3c0.4,0.1,0.9-0.1,1.1-0.5c0.5-1.1,1.1-2.3,1.6-3.4
 c0.3-0.7,1.3-0.7,1.7,0c2.9,5.1,11,7.5,18.3,8.9c0.6,0.1,1.2-0.4,1.1-1c-0.4-2.8,3.6-5.7,7.5-6.2c4.5-0.6,9.1,0.4,13.6-0.2
 c8.8-1.3,16.4-9,24.8-6.5c2.1,0.7,3.9,1.9,5.9,2.8c6.1,2.7,14.4,1.5,19-2.7c0.4-0.4,1.1-0.3,1.4,0.1c2.1,3,3.5,6.3,4.1,9.8
 c0.1,0.7,0.9,1,1.4,0.6c5.1-3.4,11.9-5.3,18.6-5c4.4,0.2,10,1,12.7-1.6c0.3-0.3,0.8-0.3,1.1-0.1c1.7,1,3.3,2.1,5,3.2
 c0.4,0.2,0.9,0.2,1.2-0.2c0.9-1,1.9-2,2.9-3c0.3-0.3,0.9-0.4,1.3-0.1c3.1,2.5,6.2,5.1,9.3,7.6c0.5,0.4,1.3,0.2,1.5-0.4
 c0.6-2,0.8-4.1,0.5-6.2c-0.1-0.7,0.5-1.2,1.1-1.1c11,2.3,17.6,11.7,22.6,20c0.6,0.9,1.3,2,2.6,2.3c1.7,0.4,3.3-0.7,4.6-1.5
 c10.7-7.1,25.7-7.9,39.5-8.3c4.8-0.2,10.4,0,13.5,2.7c0.4,0.3,0.9,0.4,1.3,0.3c1.3-0.4,2.8-0.2,4,0.3c0.8,0.4,1.8,0,2.1-0.8
 c0.2-0.5,0.3-1,0.5-1.5c0.3-0.8,1.1-1.2,1.9-0.9c3,1.2,6.8,1,9.7-0.4c0.5-0.2,1-0.2,1.5,0.1c2.1,1.4,4.2,2.9,6.3,4.3
 c0.7,0.5,1.6,1.1,2.6,0.9c1.6-0.2,1.8-1.9,2.6-3c2.3-3.2,8.2-1.6,12.3,0.1c3.8,1.5,10.2,2,11.4-1.2c0.1-0.3,0.4-0.6,0.7-0.7
 c3.7-1.9,7.6-3,11.3-4.9c0.5-0.2,1.1-0.2,1.5,0.1c2.5,1.9,5.3,3.5,8.2,4.9c0.6,0.3,1.4,0.6,2.2,0.5c2-0.2,2.1-2.5,2.9-4.1
 c1.4-2.9,6.2-3.5,10.1-3.7c5.8-0.2,11.6-0.5,17.3-0.7c2.3-0.1,4.9-0.3,6.4-1.7c1.2-1.2,1.4-2.8,1.4-4.3c0-3-0.5-6.1-1.4-9.1
 c-0.4-1.2,0.8-2.2,1.9-1.8c2.4,0.9,5,1.6,7.6,1.6c3.2-0.1,6.5-1.6,7.6-3.8c0.3-0.7,1.1-1.1,1.9-0.8c0.4,0.1,0.8,0.3,1.2,0.4
 c0.8,0.3,1.8-0.2,2-1c0-0.1,0-0.2,0.1-0.3c0.1-0.8,0.9-1.3,1.7-1.1c1.5,0.2,2.9,0.5,4.4,0.7c0.8,0.1,1.6-0.4,1.7-1.3
 c0.3-1.8,0.4-3.5,0.4-5.3c0-1,1-1.7,2-1.4c1.6,0.6,3.2,1.1,4.9,1.4c0.8,0.2,1.6-0.4,1.7-1.2c0.3-1.5,0.6-3.1,0.8-4.6
 c0.1-0.8,0.8-1.3,1.6-1.2c8,0.8,16.2,0.7,24.2-0.4c1.1-0.2,2,1,1.5,2.1c-1.8,4.1-3.9,8-6.2,11.9c-0.7,1.2,0.6,2.7,1.9,2.1
 c4.5-2,9-4.1,13.5-6.1c4.8-2.1,10.2-5.2,11.6-9.2c0.3-1,1.5-1.4,2.3-0.7c1.3,1.1,2.7,2.1,4.4,2.8c2.4,0.9,5.6,0.8,7.4-0.7
 c0.4-0.4,1-0.5,1.6-0.3c4,1.8,8.9,3.3,12.7,1.5c1.6-0.8,2.7-2,4.1-3c2.9-2,6.9-2.8,10.6-2c0.3,0.1,0.5,0,0.8-0.1
 c6.4-2.2,15.7,1.9,19.9,6.3c1.3,1.5,3.1,3.4,5.3,2.8c2.5-0.7,2-3.9,4-5.1c3.3-2,8.1,2.6,11.6,1c2.8-1.2,1.9-5.1,4.4-6.6
 c4.1-2.5,11.1,3.6,14.5,0.5c0.8-0.8,1-1.9,2-2.5c1.9-1.3,4.6,0.4,6.8,1.3c3.9,1.7,9.2,0.7,11.9-2c0.6-0.6,1.5-0.5,2.1,0
 c2.5,2.4,7.3,3.1,11.4,2.4c4.7-0.8,8.9-2.9,13.6-3.6c3.2-0.5,6.5-0.3,9.6-0.9c2.4-0.5,4.8-1.5,6.1-3.1c0.6-0.7,1.6-0.7,2.2-0.1
 c0.8,0.9,1.7,1.8,2.5,2.8c0.5,0.5,1.3,0.7,1.9,0.3c0.2-0.1,0.3-0.2,0.5-0.4c0.6-0.5,1.4-0.5,2,0c1.9,1.6,5.7,1.7,7.7,0.1
 c0.6-0.5,1.4-0.5,1.9,0c1.4,1.2,2.7,2.4,4.1,3.6c0.4,0.4,1.1,0.5,1.6,0.2c0.2-0.1,0.3-0.2,0.5-0.3c0.8-0.5,1.8-0.2,2.2,0.6
 c1.2,2.7,2.5,5.2,4.6,7.4c2.7,2.7,6.9,4.7,11.2,4.2c5.8-0.7,9.3-5.2,12.7-9s9-7.5,14.4-5.9c2.1,0.7,3.7,2.1,5.2,3.5
 c3.9,3.9,7.3,8.1,10.1,12.5c0.5,0.9,1.8,0.9,2.4,0.1c3.1-3.9,6.1-7.8,9.2-11.7c0.7-1,2.4-1.9,3.6-1.8c0.7,0.1,1.4-0.2,1.6-0.9
 c0-0.1,0.1-0.2,0.1-0.3c0.4-0.9,1.6-1.2,2.4-0.5c1.5,1.3,3.6,2.3,5.7,2.7c0.8,0.2,1.7-0.4,1.8-1.2c0.2-1.9,3.6-3.8,6.5-3.6
 c3.7,0.3,7.3,2.1,10.9,1.5c2.5-0.4,4.7-2.1,7.3-2.1c3.7,0,6,3,7.9,5.6c1.9,2.5,5,5.3,8.5,4.4c4.3-1.1,9.8,1.5,12.5,4.3
 c1,0.9,2.2,2.2,3.7,1.9c2-0.5,1.5-2.9,2.9-4.2s4-0.7,5.9,0.2c1.8,0.8,4.1,1.8,6,1c1.3-0.5,1.9-1.7,3.2-2.3c3.5-1.6,7.1,2.7,11.1,2.4
 c1.8-0.1,3.5-1.2,5.2-0.9c1.4,0.3,2.4,1.5,3.8,1.8c2.6,0.6,4.6-1.8,7.1-2.7c1.4-0.5,3-0.4,4.4,0.1c0.9,0.3,1.9-0.4,1.9-1.4L3990.3,1
 H0L0.1,197.8z"></path> <path style="fill: #fff" class="elementor-shape-fill ha-shape-divider" d="M0.1,131.7c28.7-4.6,58.2-5.7,87.4-3.5c1.5,0.1,2.5,1.3,2.7,2.8c0.6,5.5,13.3,8.3,20.5,4.6c8.2-4.2,12.8-12,21.6-15.2
 c17.4-6.4,40.2,8.1,56.5,2.4c1.8-0.6,3.8,0.4,4.1,2.3c2.5,14.9,24.6,25,44.8,26.1c22,1.2,44-4.2,66-3.1c15.7,0.8,31.1,5,44,12.1
 c3.1,1.7,6.1,3.6,9.9,4c5.3,0.7,10.3-1.6,15.2-3.5c15.4-5.7,33.8-6.6,50-2.3c22,5.8,44.6,20.7,65.3,12.5
 c11.8-4.8,18.4-16.1,31.3-18.5c6.1-1.1,12.5,0.1,18.6,1.2c37.2,7.3,74.5,14.4,111.7,21.6c0.4,0.1,0.7,0,1.1-0.2
 c8.7-4.4,20.4-4.8,29.7-1.2c0.7,0.3,1.4,0.1,1.9-0.4c12.8-14.2,31.1-25.5,51.8-31.8c0.5-0.2,1.1-0.1,1.5,0.3
 c12.2,8.8,31.9,10.5,46.2,3.9c4-1.8,7.6-4.3,11.9-5.6c4.7-1.5,9.8-1.6,14.7-2c24.4-2.2,47.1-13.8,59.6-30.2c0.6-0.8,1.8-0.8,2.6-0.1
 c5.7,6,15.3,9.9,24.9,9.4c9.8-0.5,19.1-5.7,22.3-12.9c0.4-0.8,1.2-1.2,2-0.9c18.6,5.8,39.5,7.2,59,3.6c0.3-0.1,0.6,0,0.8,0.1
 c9.9,3.4,22.4,2.1,30.7-3.4c4.3-2.8,13-1.7,18.6-2.8c0.6-0.1,1.2,0.1,1.6,0.5c2.7,3,6.7,5.3,11.3,6.3c0.3,0.1,0.7,0,1-0.1
 c6.1-2.6,12.9-4,19.7-4.4c23.7-1.2,49.3,10.9,70.3,3c1.2-0.5,2.5-0.1,3.4,0.8c8.5,8.8,25.7,9.7,40.3,9.4c4.7-0.1,9.3-0.1,14-0.2
 c11.6-0.1,25.4-1.6,31.6-8.4c1.2-1.4,3.4-1.4,4.6,0c13.5,14.6,47.3,15.7,57,32c0.7,1.2,2.3,1.8,3.7,1.3c2.8-0.9,5.6-1.8,8.4-2.7
 c0.5-0.1,0.9-0.1,1.3,0.2c2.9,2.1,5.9,4.3,8.8,6.5c0.6,0.4,1.3,0.4,1.9-0.1c5.3-4.7,11.6-9.4,19.5-9.6c3.2-0.1,6.5,0.7,9.6,0
 c2.4-0.6,4.4-2,6.2-3.4c8.6-6.4,17.1-12.9,25.7-19.2c0.6-0.4,1.4-0.4,1.9,0.2c9,9.4,22.9,15.9,37.9,17.7c14.8,1.9,29.9-0.7,43.8-4.9
 c0.4-0.1,0.7-0.4,0.9-0.8c3.9-7.8,9.1-14.6,13-22.4c0.2-0.4,0.6-0.7,1.1-0.8c17.5-2.5,35.1-5,52.6-7.5c3.8-0.6,7.6-1.1,10.9-2.6
 c3.2-1.4,5.7-3.6,8.9-5.1c12.7-6,28.4,1.5,40.1,8.6c11.7,7,27.9,14.2,40.2,7.7c4.5-2.4,8.1-6.4,13.5-6.5c2.4,0,4.7,0.9,6.9,1.7
 c11.7,4.9,22.5,11.1,31.7,18.5c2.9,2.3,5.8,4.8,9.5,6.3c4.3,1.7,9.2,1.9,13.9,2.7c8.6,1.5,16.2,5.2,24.7,6.9
 c8.1,1.7,17.8,1.1,23.2-3.8c0.5-0.4,1.2-0.5,1.8-0.1c8.8,6.2,23.4,3.7,35,0.7c8.8-2.3,17.6-4.6,26.3-6.9c3.3-0.8,6.9-1.7,10.1-0.7
 c6,1.9,8.9,9.6,15.1,8.1c3.6-0.9,4.6-4.4,6.3-7c1.6-2.4,6.8-4.1,8.4-1.9c0.2,0.3,0.5,0.5,0.9,0.6c15,2.5,31.9,7.1,43.5,14.8
 c0.5,0.4,1.3,0.3,1.8-0.1c2.6-2.2,5.1-4.3,7.7-6.5c0.4-0.4,1.1-0.5,1.6-0.2c20.3,10.2,48.1,10.7,69,1.2c6.2-2.8,14.6-6.3,20.1-3.8
 c1.3,0.6,2.8,0,3.4-1.3c4.1-9.3,8.1-18.6,12.3-27.8c0.9-2,1.9-4.2,4.1-5.6c7.7-4.7,18.5,5.3,27.6,4.5c1.5-0.1,2.5-1.7,2.1-3.2
 c-2.2-9,8.1-19,20.1-18.7c10.2,0.3,18.2,6.4,24.9,12.6c0.9,0.9,2.4,0.9,3.3,0.1c10-8.5,24.7-13.7,39.6-13.8c0.9,0,1.7,0.4,2.1,1.1
 c3.5,5.3,7,10.7,10.5,16c1,1.5,3.1,1.5,4.2,0.1c4-5.5,8.1-11,12.1-16.5c0.7-0.9,1.9-1.3,3-0.9c2.4,0.9,4.8,1.8,7.2,2.7
 c0.6,0.2,1.3,0.2,2-0.1c11.9-5.1,28.8,0.4,37.9,8.8c1,0.9,2.5,0.8,3.4-0.1c3-2.8,9.7-3.2,14.9-3.4c5-0.2,11.2-2.5,12.5-5.6
 c0.5-1.2,1.9-1.7,3.1-1.3c10.5,3.3,24.8-3,26.5-11.7c0.3-1.5,1.8-2.4,3.3-2c5.3,1.7,11.2,2.2,16.8,1.2c0.8-0.1,1.7,0.1,2.3,0.7
 c5.6,5.9,16.2,8.7,25.3,6.6c0.8-0.2,1.5-0.7,1.8-1.4c1.9-4.8,11.5-5.4,18.4-4.3c18.4,3.1,36.1,8.2,52.6,15.2
 c1.4,0.6,3.1-0.2,3.5-1.7c1.6-6.4,10.4-11.6,19.1-11.4c8.7,0.2,16.5,4.3,21.6,10c1.1,1.3,3.1,1.1,4-0.3c2.1-3.4,4.7-6.6,7.6-9.6
 c0.9-1,2.4-1.1,3.4-0.2c14.4,11.8,36.2,18,57,15.9c4.2-0.4,8.6-1,12.5-0.2c0.9,0.2,1.8-0.1,2.4-0.8c9.9-10.6,19.7-21.3,29.5-31.9
 c2.4-2.6,5.1-5.4,9-6.6c6.7-2,14,1.6,19.2,5.5c5.1,3.9,10.2,8.6,17.3,9.5c5.8,0.7,12.3-3.3,12.2-7.4c0-1.5,1-2.7,2.5-2.7
 c4.8-0.1,9.6-0.3,14.4-0.4c14.3-0.4,29.9,7.5,33.2,18.1c0.4,1.2,1.5,1.9,2.7,1.8c5.4-0.5,10.6-2.2,14.8-4.9c1.3-0.8,3-0.7,4.2,0.4
 c3.5,3.2,6.5,6.7,8.8,10.5c1,1.5,3,2,4.6,1.2c2.6-1.4,5.2-2.8,7.8-4.2c0.5-0.3,1.2,0,1.3,0.6c1,3.5,2.3,7,5.8,9.1
 c2.9,1.8,7.7,1.7,10.3,0c0.5-0.3,1.2-0.1,1.4,0.4c0.6,1.8,2.8,3.4,5.2,3.4c16.5,3.5,32.9,7,49.3,10.5c11,2.3,22.3,4.8,31.3,10.3
 c4.8,2.9,8.9,6.5,14.4,8.5c5,1.7,11.8,1.5,15.2-1.6c0.5-0.4,1.2-0.3,1.5,0.3c4.1,8.8,20.5,13,30.5,7.3c0.4-0.2,0.9-0.1,1.2,0.2
 c3.9,4.9,11.6,7.6,18.9,6.6c7.1-0.9,13.2-5.5,14.8-10.9c0.2-0.5,0.7-0.8,1.2-0.6c10,4.1,18.1,10.7,23,18.5c0.1,0.2,0.4,0.4,0.6,0.4
 c2,0.3,4.1-0.3,5.6-1.3c0.4-0.3,1-0.3,1.3,0.1c3.9,4.9,10.6,10.2,17.1,7.6c5.9-2.3,5.4-8.9,7.3-13.9c5.6-14.6,35.5-17.5,41-31.5
 c0.3-0.7,1.3-0.8,1.7-0.1c3,5,7.9,9.2,13.9,12c0.4,0.2,1,0,1.2-0.4c1.9-3.5,3.8-7.1,5.7-10.6c0.2-0.3,0.5-0.5,0.8-0.5
 c4.1-0.1,7.4-2.7,10.4-5c15.8-12.4,36.2-21.4,58.1-25.5c0.2,0,0.4,0,0.6,0.1c8.7,4.5,14.8,11.8,16.2,19.9c0.1,0.7,0.8,1,1.4,0.6
 c4.8-3.1,11.5-4.2,17.5-3c0.4,0.1,0.8-0.1,1-0.4c2.7-4.6,8.2-8.3,14.5-9.6c2.3-0.7,4.5,1,5.8,2.7c1.2,1.6,2.8,3.6,5.2,3.5
 c0.2,0,0.4,0,0.5-0.1c2.9,11.8,21.6,22.5,37,21.3c0.2,0,0.4,0,0.6,0.2c2.6,2,5.2,4,7.8,6c0.3,0.3,0.8,0.3,1.1,0
 c2.8-2,5.1-4.5,6.7-7.3c0.3-0.6,1.1-0.6,1.5-0.1c1.9,2.3,4.3,4.4,7.2,5.9c0.2,0.1,0.4,0.1,0.7,0.1c7.3-1.7,10.4-10.6,18.1-10.6
 c7.6,0,14.9,8.6,20.3,5.6c0.5-0.3,1.1-0.1,1.4,0.4c3.2,7.6,4.5,15.8,3.7,23.8c-0.1,0.6,0.5,1.1,1.1,1c15.3-2.4,32.2,4,39.2,14.8
 c1.1,1.7,2,3.5,4,4.5c5.9,2.8,10.9-4,11.9-9.6c0.1-0.6,0.6-0.9,1.2-0.7c8.3,2.7,19-1.2,22.3-7.6c0.6-1.1,1.9-1.5,3.1-1.1
 c5.7,2.3,14.4-3.1,16.5-8.7c2.4-6.4,2.6-14.2,9.7-17.7c0.2-0.1,0.5-0.2,0.7-0.2c17.5,0.7,34.6,6,48.1,14.7c0.4,0.3,0.9,0.3,1.4,0.1
 c7.9-3.4,18.1-3.5,26-0.1c0.2,0.1,0.5,0.1,0.8,0.1c15.5-2.1,32.9-6,44.3-14.6c0.2-0.2,0.5-0.3,0.8-0.3c12.4-0.6,24.8,0.5,36.6,3.2
 c0.5,0.1,1,0,1.3-0.3c2.6-2.4,5.2-4.7,7.8-7.1c0.6-0.6,1.6-0.5,2.1,0.1c2.4,2.9,5.6,5.4,9.3,7.2c0.8,0.4,1.7,0.1,2-0.7
 c2.2-5.3,6.3-10.1,11.7-13.8c0.9-0.6,2-0.1,2.2,0.9c0.9,3.7,7.7,6.6,12.8,5.1c6.2-1.8,9.4-7.2,10.9-12.3c1.3-4.4,1.9-9.1,4.5-13
 c0.5-0.8,1.8-1,2.4-0.2c3.4,4.4,10.2,7.1,16.8,6.5c6.5-0.5,12.3-4.3,14.5-9.1c0.4-0.9,1.5-1.1,2.3-0.5c16.5,13.5,33,26.9,49.5,40.4
 c0.6,0.4,1.3,0.4,1.9,0c1.9-1.6,3.9-3.3,5.8-5c0.6-0.5,1.5-0.4,2,0.1c4.2,4.4,9.9,7.9,16.3,10.2c0.6,0.2,1.3,0,1.7-0.6
 c1-1.4,1.9-2.8,2.9-4.2c0.3-0.4,0.7-0.6,1.1-0.6c8.3-0.6,16.6-1.2,25-1.9c2.3-0.2,4.9-0.4,6.7-1.6c2-1.2,2.8-3.3,3.5-5.2
 c2.3-6.8,4.7-13.5,7-20.2c0.3-0.9,1.4-1.3,2.2-0.8c2.6,1.8,9.3,0.7,12.3-1.9c3.9-3.5,8.6-8.4,14-6.6c2.6,0.9,3.9,3.1,5.9,4.6
 c1.6,1.3,4.5,1.9,6.3,1c0.4-0.2,0.8-0.2,1.2-0.1c4.7,1.9,10.4,4.4,14.3,5.8c5.6,2.1,12.1,4.2,17.8,2.3c2.7-0.9,4.9-2.6,7.5-3.6
 c8.8-3.3,19.3,2.1,28.8,0.4c3.6-0.6,6.9-2.2,10.6-2.2c8,0.2,13.2,8,21.2,8c8.2,0,13.3-8.4,21.6-8.7c8-0.2,12.7,7,19.9,9.9
 c7.9,3.2,17.7,0.3,25-3.7s13.7-9.2,22.1-11.6c2-0.5,4.1-0.9,6.1-0.6c0.4,0.1,0.8,0.2,1.6,0.3c0.9,0.1,1.7-0.6,1.7-1.5l0.1-66.4L0,0
 L0.1,131.7z"></path> </svg></div>

</div>


<!-- Terceiro Container Ok -->
<div class="container-fluid">

    <div class="text-center f-size-2-2 boxArrow">
        <i class="fa-solid fa-circle-arrow-down"></i>
    </div>


    <div class="text-center">
        <div class="f-size-2-2 mt-6 text-weight-bolder montserrat lh-1 text25">
            <u>Sem medos, sem crenças limitantes</u>, com<br class="hideElement"> corpo e mente em equilíbrio!
        </div>
        <div class="f-size-1-1 text-weight-bold interSemi mt-2 text16">
            Desbloqueie a sua mente com a meditação e ganhe o controle da sua mente e da sua vida.
        </div>
    </div>

    <div class="row mt-6 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 d-flex justify-content-center align-items-center centerOnMobile mx-0 set-transition-down-up" id="secdSection">

        <div class="text-center cardThirdContainer col-lg-4 col-md-4 col-sm-12 col-12">
            <img src="./assets/portaisInteriores/portaisInteriores/container-2-1.webp" alt="" class="card-img imgContainer">
            <div class="mt-3 text-center textCard f-size-1-2 montserrat text-weight-medium text16">
                Já pensou como seria se você pudesse<br class="hideText"> <b>atingir o controle da sua mente e<br class="hideText"> emoções</b> com práticas simples e<br class="hideText"> guiadas de meditação?
            </div>
        </div>

        <div class="d-flex flex-column cardThirdContainer col-lg-4 col-md-4 col-sm-12 col-12 align-items-center">
            <img src="./assets/portaisInteriores/portaisInteriores/container-2-2.webp" alt="" class="card-img imgContainer">
            <div class="mt-3 text-center textCard f-size-1-2 montserrat text-weight-medium text16">
                Já imaginou como seria se você<br class="hideText"> soubesse <b>como realmente funcionam<br class="hideText"> seus portais internos</b> para viver em<br class="hideText"> paz e equilíbrio a vida conturbada de<br class="hideText"> hoje?
            </div>
        </div>

        <div class="d-flex flex-column cardThirdContainer col-lg-4 col-md-4 col-sm-12 col-12 align-items-center">
            <img src="./assets/portaisInteriores/portaisInteriores/container-2-3.webp" alt="" class="card-img imgContainer">
            <div class="mt-3 text-center textCard f-size-1-2 montserrat text-weight-medium text16">
                Como seria <b>viver com mais calma,</b><br class="hideText"> sem estresse e sem insônia? Controle<br class="hideText"> do seu estado de espírito, da<br class="hideText"> ansiedade gerada no dia a dia e ser<br class="hideText"> dona de si mesma?
            </div>
        </div>
    </div>


    <div class="text-center">
        <div class="f-size-2-2 mt-5 text-weight-bolder montserrat text25">
            A Meditação é a <span class="roxoClaro">grande chave!</span>
        </div>
        <div class="f-size-1-1 text-weight-bold interSemi text16">
            Você pode acessar os portais de seus 7 portais internos através dessa prática e <br class="hideElement">equilibrar seu corpo e a sua mente ainda hoje.
        </div>
    </div>
    
    <br><br>
</div>
<!--  -->

<!-- Quarto Container -->
<div class="container-fluid terceiroBackground pt-5 d-flex justify-content-center">
    <div class="row col-xl-8 col-lg-12 col-md-12 col-sm-12 col-12 mt-5 justify-content-center containerItens"> 
        <div class="text-center text-color-white">
            <div class="f-size-2-2 text-weight-bolder montserrat lh-38px text25">
            Quais as vantagens de acessar os <br class="hideElement">7 portais através da meditação?
            </div>
            <div class="f-size-1-1 mt-1 text-weight-bold interSemi text16">
                Entenda o que acontece quando você aprende a meditação de forma correta e que funciona para qualquer um.
            </div>
        </div>
        <div class="row mt-4 marginTop">
            <div class="col-md-12 col-sm-12 col-12">
                <div class="row mt-3 justify-content-center">
                    <div class="col-md-4 col-sm-12 col-12 cardVantagens text-center">
                        <i class="fa-regular fa-circle-check azulCiano f-size-3"></i>
                        <div class="text-color-white f-size-1-2 mt-3 montserrat text-weight-medium">
                            Mais amor incondicional, amor <br class="hideElement"> próprio, e autoconhecimento <br class="hideElement"> aflorados, com maior sentimento de <br class="hideElement"> valorização;
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12 col-12 cardVantagens text-center">
                        <i class="fa-regular fa-circle-check azulCiano f-size-3"></i>
                        <div class="text-color-white f-size-1-2 mt-3 montserrat text-weight-medium">
                            Aumento do desenvolvimento <br class="hideElement">
                            pessoal, acessando o que há de <br class="hideElement">
                            melhor e mais divino dentro de você,  <br class="hideElement"> mesmo que nunca tenha meditado <br class="hideElement">
                            antes;
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12 col-12 cardVantagens text-center">
                        <i class="fa-regular fa-circle-check azulCiano f-size-3"></i>
                        <div class="text-color-white f-size-1-2 mt-3 montserrat text-weight-medium">
                            Expansão da sua consciência e um <br class="hideElement">
                            tato sensitivo mais apurado para o <br class="hideElement">
                            que ocorre à sua volta;
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-4 marginTop">
            <div class="col-md-12">
                <div class="row mt-3 justify-content-center">
                    <div class="col-md-4 cardVantagens text-center">
                        <i class="fa-regular fa-circle-check azulCiano f-size-3"></i>
                        <div class="text-color-white f-size-1-2 mt-3 montserrat text-weight-medium">
                            Despertar seu poder interno através <br class="hideElement"> da meditação, simplesmente por se <br class="hideElement"> conectar em equilíbrio com a sua <br class="hideELement"> mente e o seu corpo;
                        </div>
                    </div>
                    <div class="col-md-4 cardVantagens text-center">
                        <i class="fa-regular fa-circle-check azulCiano f-size-3"></i>
                        <div class="text-color-white f-size-1-2 mt-3 montserrat text-weight-medium">
                            Reduzir o seu estresse, controlar a sua<br class="hideElement">
                             ansiedade, aliviar a sua depressão e<br class="hideElement">
                              melhorar a sua insônia de forma<br class="hideElement">
                               prática, acessível e eficiente;
                        </div>
                    </div>
                    <div class="col-md-4 cardVantagens text-center">
                        <i class="fa-regular fa-circle-check azulCiano f-size-3"></i>
                        <div class="text-color-white f-size-1-2 mt-3 montserrat text-weight-medium">
                            Alinhar o seu estado físico, emocional,<br class="hideElement">
                             mental e energético que são os<br class="hideElement">
                              pilares para a sua vida fluir melhor,<br class="hideElement">
                               através da meditação;
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-4 marginTop">
            <div class="col-md-12">
                <div class="row mt-3 justify-content-center">
                    <div class="col-md-4 cardVantagens text-center">
                        <i class="fa-regular fa-circle-check azulCiano f-size-3"></i>
                        <div class="text-color-white f-size-1-2 mt-3 montserrat text-weight-medium">
                            Parar de sentir medo e apreensão<br class="hideElement">
                             tendo como sua principal aliada a<br class="hideElement">
                              prática e o hábito da meditação,<br class="hideElement">
                               mesmo que esteja começando agora;
                        </div>
                    </div>
                    <div class="col-md-4 cardVantagens text-center">
                        <i class="fa-regular fa-circle-check azulCiano f-size-3"></i>
                        <div class="text-color-white f-size-1-2 mt-3 montserrat text-weight-medium">
                            Melhorar a memória, concentração e<br class="hideElement">
                             consciência, através de um maior<br class="hideElement">
                              alinhamento da sua mente com a<br class="hideElement">
                               meditação;
                        </div>
                    </div>
                    <div class="col-md-4 cardVantagens text-center">
                        <i class="fa-regular fa-circle-check azulCiano f-size-3"></i>
                        <div class="text-color-white f-size-1-2 mt-3 montserrat text-weight-medium">
                            Sono melhor e mais profundo, como<br class="hideElement">
                             mostram os estudos científicos sobre<br class="hideElement">
                              meditação;
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-4 marginTop">
            <div class="col-xl-12 col-lg-12 col-md-12">
                <div class="row mt-3 justify-content-center">
                    <div class="col-xl-5 col-lg-4 col-md-4 cardVantagens text-center">
                        <i class="fa-regular fa-circle-check azulCiano f-size-3"></i>
                        <div class="text-color-white f-size-1-2 mt-3 montserrat text-weight-medium">
                            Melhor controle sobre suas emoções:<br class="hideElement">
                             contenha impulsos que podem colocar<br class="hideElement">
                              abaixo sua vida pessoal e profissional;
                        </div>
                    </div>
                    <div class="col-xl-5 col-lg-4 col-md-4 cardVantagens text-center">
                        <i class="fa-regular fa-circle-check azulCiano f-size-3"></i>
                        <div class="text-color-white f-size-1-2 mt-3 montserrat text-weight-medium">
                            Ter mais criatividade, pois agora você flui,<br class="hideElement">
                             está alinhada energeticamente e conectada<br class="hideElement">
                              com seu Ser Divino.
                        </div>
                        <br><br><br class="hideBr"><br class="hideBr"><br>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--  -->

<!-- Swiper Ok-->
<div class="container-fluid">
    <div class="text-center pt-4 text-black">
        <div class="f-size-2-2 text-weight-bolder montserrat text25">
            Veja o que dizem as alunas
        </div>
        <div class="f-size-1-1 text-weight-bold interSemi text16">
            Elas estão vivendo a transformação de acessar os seus portais internos através da meditação.
        </div>
        <div class="swiper mySwiper mt-4">
            <div class="swiper-wrapper">
                <div class="swiper-slide swiperImg setWidthSlider"><img src="./assets/portaisInteriores/portaisInteriores/swiper-1.webp"></div>
                <div class="swiper-slide swiperImg setWidthSlider"><img src="./assets/portaisInteriores/portaisInteriores/swiper-2.webp"></div>
                <div class="swiper-slide swiperImg setWidthSlider"><img src="./assets/portaisInteriores/portaisInteriores/swiper-3.webp"></div>
                <div class="swiper-slide swiperImg setWidthSlider"><img src="./assets/portaisInteriores/portaisInteriores/swiper-4.webp"></div>
                <div class="swiper-slide swiperImg setWidthSlider"><img src="./assets/portaisInteriores/portaisInteriores/swiper-5.webp"></div>
            </div>
        </div>
    </div>
    <br class="hideElement"><br class="hideElement">
</div>
<!--  -->

<!-- OK -->
<div class="container-fluid greyBackground pt-2 positionRelative">

    <div class="text-center pt-2">
        <div class="f-size-2-3 mt-5 text-weight-bolder monteserrat text25">
            Quais são os 7 portais sagrados que <span class="roxoClaro">irão te destravar?</span>
        </div>
        <div class="f-size-1-1 text-weight-bold interSemi text16">
            Conheça os 7 portais que você abrirá a partir do momento que descobrir os segredos destas meditações.
        </div>
    </div>
    <div class="row justify-content-center mt-4 sectionModule">
        <div class="col-md-3 col-lg-4 col-xl-4 d-flex flex-column align-items-center text-center moduloContainer vermelho lh-1 marginRight margin0">
            <p class="f-size-1-7 mb-0 montserrat text-weight-bolder text-white text22">MAIS CORAGEM,<br>
            POSICIONAMENTO E<br>
            DIREÇÃO
            </p>
            <img src="./assets/portaisInteriores/portaisInteriores/cardColoridoVermelho.webp" class="mt-4">
            <p class="montserrat mt-4  f-size-1-4 text-weight-bold lh-30px text-white">Muladhara ou chakra raíz, é o <br> primeiro chakra.</p>
            <p class="montserrat f-size-1-2 cinza mt-2 text-weight-medium txtModule text-white">Situado na base da coluna<br> 
            vertebral e representa a ligação<br>
             do ser humano com o planeta<br>
              Terra. Na cor vermelha, ele rege<br>
               nosso instinto de sobrevivência e<br>
                questões relacionadas com<br>
                 medos, preocupações, dinheiro e<br>
                  trabalho.</p>
        </div>

        <div class="col-md-3 col-lg-4 col-xl-4 d-flex flex-column align-items-center text-center moduloContainer laranja lh-1 marginRight margin0">
            <p class="f-size-1-7 mb-0 montserrat text-weight-bolder text-white text22">
                MAIOR LIBIDO,<br>
                 SENSUALIDADE E<br>
                  DOMÍNIO DO PRAZER
            </p>
            <img src="./assets/portaisInteriores/portaisInteriores/cardColoridoLaranja.webp" class="mt-4">
            <p class="montserrat mt-4  f-size-1-4 text-weight-bold lh-30px text-white">Swadhisthana ou Chakra<br> umbilical, é o segundo<br> chakra.</p>
            <p class="montserrat f-size-1-2 cinza mt-2 text-weight-medium txtModule text-white">Situado no baixo-ventre,<br>
             abrangendo toda a região onde<br>
              ficam localizados os órgãos<br>
               genitais. Neste local, a energia é<br>
                responsável pelos desejos,<br>
                 sensações, prazer e sexualidade.</p>
            <p class="montserrat f-size-1-2 cinza mt-2 text-weight-medium txtModule text-white">
                Na cor laranja, ele também é<br>
                 relacionado com a energia da<br>
                  reprodução e da criação.</p>
        </div>
        
        <div class="col-md-3 col-lg-4 col-xl-4 d-flex flex-column align-items-center text-center moduloContainer amarelo lh-1 marginRight margin0 cardMarginTop">
            <p class="f-size-1-7 mb-0 montserrat text-weight-bolder text-white text22">ENTUSIASMO E<br>
             COMPROMETIMENTO
            </p>
            <img src="./assets/portaisInteriores/portaisInteriores/cardColoridoAmarelo.webp" class="mt-4">
            <p class="montserrat mt-4  f-size-1-4 text-weight-bold lh-30px text-white">Manipura ou Chakra do plexo<br> solar, é o terceiro chakra.</p>
            <p class="montserrat f-size-1-2 cinza mt-2 text-weight-medium txtModule text-white">Esse é o chakra mais relacionado<br>
             com a nossa personalidade e<br>
              nosso ego. Na cor amarela, este<br>
               chakra é o nosso sol, nossa<br>
                energia mais pessoal e força de vontade.</p>
            <p class="montserrat f-size-1-2 cinza mt-2 text-weight-medium txtModule text-white">
            Este chakra bem equilibrado,<br>
             desenvolve a autoconfiança da<br>
              pessoa.


            
        </div>
    </div>

    <div class="row justify-content-center mt-2 sectionModule2">
        <div class="col-md-3 col-lg-3 col-xl-3 d-flex flex-column align-items-center text-center moduloContainer section2 verde lh-1 margin0 marginRight">
            <p class="f-size-1-7 mb-0 montserrat text-weight-bolder text-white text22">AMOR PRÓPRIO,<br>
             COMPAIXÃO E SAÚDE
            </p>
            <img src="./assets/portaisInteriores/portaisInteriores/cardColoridoVerde.webp" class="mt-4">
            <p class="montserrat mt-4  f-size-1-4 text-weight-bold lh-30px text-white">Anahata ou Chakra cardíaco,<br>
             é o quarto chakra.</p>
            <p class="montserrat f-size-1-2 cinza mt-2 text-weight-medium txtModule text-white">É o chakra do coração,<br>
             responsável pelo amor que<br>
              sentimos pelos outros e por nós<br>
               mesmos. Também é o chakra da<br>
                gratidão e generosidade que<br>
                 não se limita apenas ao amor<br>
                  pela outra pessoa, mas pela<br>
                   humanidade toda.
            </p>
            <p class="montserrat f-size-1-2 cinza mt-2 text-weight-medium txtModule text-white">
                Na cor verde, que é a cor da<br>
                 saúde, nos mostra a grande<br>
                  ligação entre emoções e saúde.<br>
                   Por isso também trabalha o<br> equilíbrio do sistema<br>
                    imunológico.


            </p>
        </div>

        <div class="col-md-3 col-lg-3 col-xl-3 d-flex flex-column align-items-center text-center moduloContainer section2 margin0 azul lh-1">
            <p class="f-size-1-7 mb-0 montserrat text-weight-bolder text-white text22">COMUNICAÇÃO<br>
             VERDADEIRA
            </p>
            <img src="./assets/portaisInteriores/portaisInteriores/cardColoridoAzul.webp" class="mt-4">
            <p class="montserrat mt-4  f-size-1-4 text-weight-bold lh-30px text-white">Vishuddha ou chakra<br> laríngeo, é o quinto chakra.</p>
            <p class="montserrat f-size-1-2 cinza mt-2 text-weight-medium txtModule text-white">Situado na garganta e<br>
             representa a comunicação e<br>
              criatividade. Na cor azul,<br>
               quando bem equilibrado mostra<br>
                a clareza e compreensão da<br>
                 pessoa ao conversar, se<br>
                  mostrando disposta a ouvir e<br>
                   falar.
            </p>
            <p class="montserrat f-size-1-2 cinza mt-2 text-weight-medium txtModule text-white">
            Está relacionado também com a<br>
             nossa comunicação interna com<br>
              nós mesmos.
            </p>
        </div>
    </div>

    <div class="row justify-content-center mt-2 sectionModule2">
        <div class="col-md-3 col-lg-3 col-xl-3 d-flex flex-column align-items-center text-center moduloContainer roxo lh-1 marginRight margin0">
            <p class="f-size-1-7 mb-0 montserrat text-weight-bolder text-white text22">100% DE FOCO E<br> CONCENTRAÇÃO
            </p>
            <img src="./assets/portaisInteriores/portaisInteriores/cardColoridoRoxo.webp" class="mt-4">
            <p class="montserrat mt-4  f-size-1-4 text-weight-bold lh-30px text-white">Ajna ou chakra frontal, é o<br> sexto chakra.</p>
            <p class="montserrat f-size-1-2 cinza mt-2 text-weight-medium txtModule text-white">Conhecido como o chakra do<br>
             terceiro olho, está ligado à<br>
              concentração, meditação e<br>
               intuição. Na cor roxa, quando<br>
                bem equilibrado nos torna mais<br>
                 presentes e conectados.</p>
        </div>

        <div class="col-md-3 col-lg-3 col-xl-3 d-flex flex-column align-items-center text-center moduloContainer lh-1 margin0">
            <p class="f-size-1-7 mb-0 montserrat text-weight-bolder text22">ACESSE SUA<br>
             ESPIRITUALIDADE
            </p>
            <img src="./assets/portaisInteriores/portaisInteriores/cardColoridoBranco.webp" class="mt-4">
            <p class="montserrat mt-4  f-size-1-4 text-weight-bold lh-30px">Sahasrara ou chakra<br>
             coronário, é o sétimo chakra.</p>
            <p class="montserrat f-size-1-2 mt-2 text-weight-medium txtModule">Representado pela flor de lótus<br>
             na cor cristalina, está situado na<br>
              cabeça e mostra a nossa<br>
               conexão com uma consciência<br>
                mais elevada, ou melhor, um<br>
                 plano espiritual.</p>
            <p class="montserrat f-size-1-2 mt-2 text-weight-medium txtModule">É este o canal que nos conecta<br>
             com a meditação e com a<br>
              energia do Reiki e nos conecta<br>
               com a espiritualidade.</p>
        </div>
    </div>

    <br><br><br>
</div>

<div class="container-fluid">
    <!-- Container Reportagem OK -->
    <div class="text-center pt-2">
        <div class="f-size-2-2 mt-5 text-weight-bolder monteserrat lh-38px text25">
            É comprovado que a meditação ajuda milhares de <br class="hideElement"> pessoas nos dias de hoje...
        </div>
        <div class="f-size-1-1 text-weight-bold interSemi mt-2 text16">
            Pessoas do mundo todo já tiveram a sua vida impactada positivamente pela meditação.
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="row mt-4 col-xl-9 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 cardReportagem">
                <a class="mouseHoverReportagens" onclick="openLightBox(0)">
                    <img src="./assets/portaisInteriores/reportagemSuperInteressante.webp" alt="" class="card-img imgReportagem">
                </a>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 cardReportagem">
                <a class="mouseHoverReportagens" onclick="openLightBox(1)">
                    <img src="./assets/portaisInteriores/reportagemGE.webp" alt="" class="card-img imgReportagem">
                </a>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 cardReportagem">
                <a class="mouseHoverReportagens" onclick="openLightBox(2)">
                    <img src="./assets/portaisInteriores/reportagemVeja.webp" alt="" class="card-img imgReportagem">
                </a>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 cardReportagem">
                <a class="mouseHoverReportagens" onclick="openLightBox(3)">
                    <img src="./assets/portaisInteriores/reportagemUNA.webp" alt="" class="card-img imgReportagem">
                </a>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 cardReportagem">
                <a class="mouseHoverReportagens" onclick="openLightBox(4)">
                    <img src="./assets/portaisInteriores/reportagemG1.webp" alt="" class="card-img imgReportagem">
                </a>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 cardReportagem">
                <a class="mouseHoverReportagens" onclick="openLightBox(5)">
                    <img src="./assets/portaisInteriores/reportagemSuperInteressante2.webp" alt="" class="card-img imgReportagem">
                </a>
            </div>
        </div>
    </div>
    <div class="hr mt-7"></div>


    <div class="row">
        <div class="text-center mt-4">
            <div class="f-size-2-2 text-weight-bolder montserrat mt-5 lh-1 text25">
                A Meditação é a chave da transformação para a sua<br class="hideElement"> vida em poucos passos...
            </div>
            <div class="f-size-1-1 text-weight-bold interSemi mt-2 text16">
             O caminho para acessar seu poder interior é muito mais simples do que você imagina…
            </div>
        </div>
    </div>
        
        <div class=" mt-4 d-flex flex-xl-row flex-lg-row flex-md-row flex-column flex-sm-column justify-content-center col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="d-flex flex-column align-items-center col-xl-2 col-lg-3 col-md-3 col-sm-12 col-12 cardPrancheta primeiroBackground">
                <img src="./assets/portaisInteriores/portaisInteriores/cardPrancheta-1.webp" class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 cardPranchetaImg" alt="">
                <p class="text-white text-weight-medium montserrat text-center mt-4 f-size-1-2 textCardPrancheta">
                Mantenha o corpo leve e<br class="hideText">
                 com disposição para o dia a<br class="hideText">
                  dia, enquanto sua mente se<br class="hideText">
                   mantém preparada para<br class="hideText">
                    lidar com qualquer<br class="hideText">
                     situação e tomada de<br class="hideText">
                      decisão que sejam<br class="hideText">
                       necessárias.
                </p>
            </div>
            <div class="d-flex flex-column align-items-center col-xl-2 col-lg-3 col-md-3 col-sm-12 col-12 cardPrancheta primeiroBackground ms-2 marginMobile">
                <img src="./assets/portaisInteriores/portaisInteriores/cardPrancheta-2.webp" class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 cardPranchetaImg" alt="">
                <p class="text-white text-weight-medium montserrat text-center mt-4 f-size-1-2 textCardPrancheta">
                Destravar os 7 Portais<br class="hideText">
                 Internos através da<br class="hideText">
                  meditação faz com que<br class="hideText">
                   você liberte seu potencial<br class="hideText">
                    para ter coragem, foco e<br class="hideText">
                     ímpeto para conquistar o<br class="hideText">
                      que deseja.
                </p>
            </div>
            <div class="d-flex flex-column align-items-center col-xl-2 col-lg-3 col-md-3 col-sm-12 col-12 cardPrancheta primeiroBackground mx-2 marginMobile">
                <img src="./assets/portaisInteriores/portaisInteriores/cardPrancheta-3.webp" class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 cardPranchetaImg" alt="">
                <p class="text-white text-weight-medium montserrat text-center mt-4 f-size-1-2 textCardPrancheta">
                Você saberá exatamente<br class="hideText">
                 como meditar da maneira<br class="hideText">
                  correta, quais as etapas,<br class="hideText">
                   quais as posições e tudo o<br class="hideText">
                    que é necessário para sair<br class="hideText">
                     do zero e já ter resultados.
                </p>
            </div>
            <div class="d-flex flex-column align-items-center col-xl-2 col-lg-3 col-md-3 col-sm-12 col-12 cardPrancheta primeiroBackground marginMobile">
                <img src="./assets/portaisInteriores/portaisInteriores/cardPrancheta-4.webp" class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 cardPranchetaImg" alt="">
                <p class="text-white text-weight-medium montserrat text-center mt-4 f-size-1-2 textCardPrancheta">
                Um passo a passo para<br class="hideText">
                 aprender como elevar sua<br class="hideText">
                  espiritualidade, aflorando<br class="hideText">
                   seus sentidos e intuição
                </p>
            </div>
        </div>

        <div class="text-center mt-7">
            <div class="f-size-2-2 text-weight-bolder montserrat lh-1 text25">
                Meditação em ação e os 7 portais abertos?<br class="hideElement">
                    É isso o<br class="hideBr showBr"> que acontecerá na prática
            </div>
            <div class="f-size-1-1 text-weight-bold mt-3 interSemi text16">
                Veja o que acontece com você depois que a meditação torna-se a chave dos 7 portais<br>
                    internos.
            </div>
        </div>

        <div class="d-flex flex-column align-items-center justify-content-center col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mt-4">

            <div class="d-xl-flex d-lg-flex d-md-flex d-sm-block d-block cardAntesDepois greyBackground col-xl-8 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="d-flex flex-column align-items-center col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 cardInterno">
                    <p class="text-weight-medium inter f-size-1-1">Antes</p>
                    <img src="./assets/portaisInteriores/portaisInteriores/cardAntes-1.webp" class="card-img imgAntesDepois" alt="">
                </div>
                <div class="d-flex flex-column align-items-center col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 cardInterno">
                    <p class="roxo titulotextosChakras text-center text-weight-bolder montserrat f-size-1-7 lh-1">Coragem e direção na<br class="hideText"> vida</p>
                    <img src="./assets/portaisInteriores/portaisInteriores/cardColoridoVermelho.webp" alt="">
                    <p class="textosChakras vermelho text-weight-bolder f-size-1-7 text-center">Chakra Muladhara</p>
                    <i aria-hidden="true" class="fas fa-arrow-right vermelho hideIcon f-size-3"></i>
                </div>
                <div class="d-flex flex-column align-items-center col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 cardInterno montserrat">
                    <p class="text-weight-medium inter f-size-1-1">Depois</p>
                    <img src="./assets/portaisInteriores/portaisInteriores/cardDepois-1.webp" class="card-img imgAntesDepois" alt="">
                </div>
            </div>

            <div class="d-xl-flex d-lg-flex d-md-flex d-sm-block d-block cardAntesDepois greyBackground col-xl-8 col-lg-12 col-md-12 col-sm-12 col-12 mt-2">
                <div class="d-flex flex-column align-items-center col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 cardInterno">
                    <p class="text-weight-medium inter f-size-1-1">Antes</p>
                    <img src="./assets/portaisInteriores/portaisInteriores/cardAntes-2.webp" class="card-img imgAntesDepois" alt="">
                </div>
                <div class="d-flex flex-column align-items-center col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 cardInterno">
                    <p class="roxo titulotextosChakras text-center text-weight-bolder montserrat f-size-1-7 lh-1">Libido com a energia do<br class="hideText">
                     feminino trabalhada</p>
                    <img src="./assets/portaisInteriores/portaisInteriores/cardColoridoLaranja.webp" alt="">
                    <p class="laranja textosChakras  text-weight-bolder f-size-1-7 text-center">Chakra Swadhisthana</p>
                    <i aria-hidden="true" class="fas fa-arrow-right laranja hideIcon f-size-3"></i>
                </div>
                <div class="d-flex flex-column align-items-center col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 cardInterno montserrat">
                    <p class="text-weight-medium inter f-size-1-1">Depois</p>
                    <img src="./assets/portaisInteriores/portaisInteriores/cardDepois-2.webp" class="card-img imgAntesDepois" alt="">
                </div>
            </div>

            <div class="d-xl-flex d-lg-flex d-md-flex d-sm-block d-block cardAntesDepois greyBackground col-xl-8 col-lg-12 col-md-12 col-sm-12 col-12 mt-2">
                <div class="d-flex flex-column align-items-center col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 cardInterno">
                    <p class="text-weight-medium inter f-size-1-1">Antes</p>
                    <img src="./assets/portaisInteriores/portaisInteriores/cardAntes-3.webp" class="card-img imgAntesDepois" alt="">
                </div>
                <div class="d-flex flex-column align-items-center col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 cardInterno">
                    <p class="roxo titulotextosChakras text-center text-weight-bolder montserrat f-size-1-7 lh-1">Autoconfiança e energia<br class="hideText">
                     para realizar tarefas do<br class="hideText">
                      dia a dia</p>
                    <img src="./assets/portaisInteriores/portaisInteriores/cardColoridoAmarelo.webp" alt="">
                    <p class="amarelo textosChakras  text-weight-bolder f-size-1-7 text-center">Chakra Maniupra</p>
                    <i aria-hidden="true" class="fas fa-arrow-right amarelo hideIcon f-size-3"></i>
                </div>
                <div class="d-flex flex-column align-items-center col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 cardInterno montserrat">
                    <p class="text-weight-medium inter f-size-1-1">Depois</p>
                    <img src="./assets/portaisInteriores/portaisInteriores/cardDepois-3.webp" class="card-img imgAntesDepois" alt="">
                </div>
            </div>

            <div class="d-xl-flex d-lg-flex d-md-flex d-sm-block d-block cardAntesDepois greyBackground col-xl-8 col-lg-12 col-md-12 col-sm-12 col-12 mt-2">
                <div class="d-flex flex-column align-items-center col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 cardInterno">
                    <p class="text-weight-medium inter f-size-1-1">Antes</p>
                    <img src="./assets/portaisInteriores/portaisInteriores/cardAntes-4.webp" class="card-img imgAntesDepois" alt="">
                </div>
                <div class="d-flex flex-column align-items-center col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 cardInterno">
                    <p class="roxo titulotextosChakras text-center text-weight-bolder montserrat f-size-1-7 lh-1">Amor próprio e Equilíbrio<br class="hideText">
                     Emocional</p>
                    <img src="./assets/portaisInteriores/portaisInteriores/cardColoridoVerde.webp" alt="">
                    <p class="verde textosChakras  text-weight-bolder f-size-1-7 text-center">Chakra Anahata</p>
                    <i aria-hidden="true" class="fas fa-arrow-right verde hideIcon f-size-3"></i>
                </div>
                <div class="d-flex flex-column align-items-center col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 cardInterno montserrat">
                    <p class="text-weight-medium inter f-size-1-1">Depois</p>
                    <img src="./assets/portaisInteriores/portaisInteriores/cardDepois-4.webp" class="card-img imgAntesDepois" alt="">
                </div>
            </div>

            <div class="d-xl-flex d-lg-flex d-md-flex d-sm-block d-block cardAntesDepois greyBackground col-xl-8 col-lg-12 col-md-12 col-sm-12 col-12 mt-2">
                <div class="d-flex flex-column align-items-center col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 cardInterno">
                    <p class="text-weight-medium inter f-size-1-1">Antes</p>
                    <img src="./assets/portaisInteriores/portaisInteriores/cardAntes-5.webp" class="card-img imgAntesDepois" alt="">
                </div>
                <div class="d-flex flex-column align-items-center col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 cardInterno">
                    <p class="roxo titulotextosChakras text-weight-bolder montserrat f-size-1-7 lh-1">Confiança</p>
                    <img src="./assets/portaisInteriores/portaisInteriores/cardColoridoAzul.webp" alt="">
                    <p class="azul textosChakras  text-weight-bolder f-size-1-7 text-center">Chakra Vishuddha</p>
                    <i aria-hidden="true" class="fas fa-arrow-right azul hideIcon f-size-3"></i>
                </div>
                <div class="d-flex flex-column align-items-center col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 cardInterno montserrat">
                    <p class="text-weight-medium inter f-size-1-1">Depois</p>
                    <img src="./assets/portaisInteriores/portaisInteriores/cardDepois-5.webp" class="card-img imgAntesDepois" alt="">
                </div>
            </div>

            <div class="d-xl-flex d-lg-flex d-md-flex d-sm-block d-block cardAntesDepois greyBackground col-xl-8 col-lg-12 col-md-12 col-sm-12 col-12 mt-2">
                <div class="d-flex flex-column align-items-center col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 cardInterno">
                    <p class="text-weight-medium inter f-size-1-1">Antes</p>
                    <img src="./assets/portaisInteriores/portaisInteriores/cardAntes-6.webp" class="card-img imgAntesDepois" alt="">
                </div>
                <div class="d-flex flex-column align-items-center col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 cardInterno">
                    <p class="roxo titulotextosChakras text-center text-weight-bolder montserrat f-size-1-7 lh-1">Intuição para saber o que<br class="hideText">
                     é melhor para si e para<br class="hideText">
                      sua vida</p>
                    <img src="./assets/portaisInteriores/portaisInteriores/cardColoridoRoxo.webp" alt="">
                    <p class="roxoCard textosChakras  text-weight-bolder f-size-1-7 text-center">Chakra Ajna</p>
                    <i aria-hidden="true" class="fas fa-arrow-right roxoCard hideIcon f-size-3"></i>
                </div>
                <div class="d-flex flex-column align-items-center col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 cardInterno montserrat">
                    <p class="text-weight-medium inter f-size-1-1">Depois</p>
                    <img src="./assets/portaisInteriores/portaisInteriores/cardDepois-6.webp" class="card-img imgAntesDepois" alt="">
                </div>
            </div>

            <div class="d-xl-flex d-lg-flex d-md-flex d-sm-block d-block cardAntesDepois greyBackground col-xl-8 col-lg-12 col-md-12 col-sm-12 col-12 mt-2">
                <div class="d-flex flex-column align-items-center col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 cardInterno">
                    <p class="text-weight-medium inter f-size-1-1">Antes</p>
                    <img src="./assets/portaisInteriores/portaisInteriores/cardAntes-7.webp" class="card-img imgAntesDepois" alt="">
                </div>
                <div class="d-flex flex-column align-items-center col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 cardInterno">
                    <p class="roxo titulotextosChakras text-center text-weight-bolder montserrat f-size-1-7 lh-1">Contato com<br class="hideText">
                     Espiritualidade e<br class="hideText">
                      Ancestralidade</p>
                    <img src="./assets/portaisInteriores/portaisInteriores/cardColoridoBranco.webp" alt="">
                    <p class="cinza textosChakras  text-weight-bolder f-size-1-7 text-center">Chakra Sahasrara</p>
                    <i aria-hidden="true" class="fas fa-arrow-right cinza hideIcon f-size-3"></i>
                </div>
                <div class="d-flex flex-column align-items-center col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 cardInterno montserrat">
                    <p class="text-weight-medium inter f-size-1-1">Depois</p>
                    <img src="./assets/portaisInteriores/portaisInteriores/cardDepois-7.webp" class="card-img imgAntesDepois" alt="">
                </div>
            </div>
        </div>

        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 d-flex flex-column align-items-center mt-4">
            <div class="col-xl-6 col-lg-8 col-md-8 col-sm-12 col-12 text-weight-medium cardCiano f-size-1-2 montserrat text-center text16">
                <div class="text-center"><span class="text-weight-bolder">Isso é o que Os 7 Portais Interiores permite que você faça. </span>Por meio de<br class="hideElement hideText">
                 poderosas ferramentas energéticas que você pode usar quando precisar.</div>
                <br>
                O conhecimento, prática e meditações para fluírem os Chakras, são<br class="hideElement hideText">
                 ferramentas poderosas de <span class="text-weight-bolder">autocuidado e autocura.</span>
                <br><br>
                Com isso, você pode escolher a prática física e/ou meditação do Chakra<br class="hideElement hideText">
                 através do que precisa equilibrar (e deixar fluir) em você e,<br class="hideElement hideText">
                  consequentemente, <span class="text-weight-bolder">melhorar na sua vida.</span>
            </div>
        </div>
    <br><br>
</div>


<div class="container-fluid backgroundRoxoLinear text-color-white text-center">
    <div class="row justify-content-center">
        <div class="col-xl-6 col-lg-12 col-md-12 containerItens">
            <div class="row mt-6">
                <div class="col-md-6">
                    <div class="card segundoBackground cardIcons align-items-center">
                        <img src="./assets/portaisInteriores/portaisInteriores/iconTempo.webp" class="card-img iconsThirdContainer" alt="">
                        <div class="text-weight-bolder f-size-1-7 azulCiano">
                            Sem perder seu tempo
                        </div>
                        <div class="f-size-1-2 text-weight-medium mt-3">
                            Por que ficar procurando locais para<br class="hideElement hideText">
                             fazer aula, se você tem um método<br class="hideElement hideText">
                              validado aqui, na frente da sua tela?<br class="hideElement hideText">
                            <span class="text-weight-bolder">Estarei ao seu lado, na sua caminhada<br class="hideElement hideText">
                             de evolução.</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 marginTopIcons">
                    <div class="card segundoBackground cardIcons align-items-center">
                        <img src="./assets/portaisInteriores/portaisInteriores/iconSuporte.webp" class="card-img iconsThirdContainer" alt="">
                        <div class="text-weight-bolder f-size-1-7 azulCiano">
                            Suporte e área de membros
                        </div>
                        <div class="f-size-1-2 text-weight-medium mt-3">
                            O mais importante de tudo é <span class="text-weight-bolder">receber<br class="hideElement hideText">
                             um bom acompanhamento e estar<br class="hideElement hideText">
                              próxima de quem compartilha de sua<br class="hideElement hideText">
                               mesma visão.</span> Acesso livre para suporte<br class="hideElement hideText">
                                e área de membros engajada.
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-4 marginTopIcons">
                <div class="col-md-6">
                    <div class="card segundoBackground cardIcons align-items-center">
                        <img src="./assets/portaisInteriores/portaisInteriores/iconChance.webp" class="card-img iconsThirdContainer" alt="">
                        <div class="text-weight-bolder f-size-1-7 azulCiano">
                            Chance única
                        </div>
                        <div class="f-size-1-2 text-weight-medium mt-3">
                            Essa oferta especial é única e <span class="text-weight-bolder">pode sair<br class="hideElement hideText">
                             do ar ou aumentar muito de valor a<br class="hideElement hideText">
                              qualquer instante.</span>
                        </div>
                    </div>
                </div> 
                <div class="col-md-6 marginTopIcons">
                    <div class="card segundoBackground cardIcons align-items-center">
                        <img src="./assets/portaisInteriores/portaisInteriores/iconVida.webp" class="card-img iconsThirdContainer" alt="">
                        <div class="text-weight-bolder f-size-1-7 azulCiano">
                            Vida real
                        </div>
                        <div class="f-size-1-2 text-weight-medium mt-3">
                        <span class="text-weight-bolder">Te ensino como realmente funciona<br class="hideElement hideText">
                         no dia a dia</span>, e como colocar em prática<br class="hideElement hideText">
                          de forma fácil, sem enrolação.
                        </div>
                    </div>
                </div>
            </div>

            <div class="row mt-6">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <img src="./assets/portaisInteriores/biancaMeditacao.webp" alt="" class="card-img mt-6">
                </div>
                <div class="col-md-2"></div>
            </div>
        </div>
    </div>
    <br><br>
    <div class="elementor-shape elementor-shape-top" data-negative="false"> <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1000 20" preserveAspectRatio="none"> <path class="svg02 claro" d="M0,0v3c0,0,393.8,0,483.4,0c9.2,0,16.6,7.4,16.6,16.6c0-9.1,7.4-16.6,16.6-16.6C606.2,3,1000,3,1000,3V0H0z"></path> </svg></div>
</div>

<div class="container-fluid">
    <div class="text-center mt-7">
        <div class="f-size-2-2 text-weight-bolder montserrat lh-1 text25">
            O que você recebe no <span class="roxoClaro">Treinamento 7 portais internos?</span>
        </div>
        <div class="f-size-1-1 text-weight-bold mt-3 interSemi text16">
            Aqui está tudo o que você terá acesso dentro do treinamento que te mostra como a meditação será a chave para a sua vida fluir melhor. </div>
    </div>

    <div class="d-xl-flex d-lg-flex d-md-flex d-sm-block d-block mt-4 justify-content-center col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            
        <div class="d-flex flex-column greyBackground align-items-center col-xl-2 col-lg-3 col-md-3 col-sm-12 col-12 text-center cardTreinamentos">
            <img src="./assets/portaisInteriores/portaisInteriores/cardTreinamento-1.webp" class="card-img imgCardTreinamentos" alt="">
            <p class="roxoClaro montserrat f-size-1-7 lh-1 text-weight-bolder mt-4">Meditação do<br class="hideText"> 
            Fluxo de Portais</p>
            <p class="montserrat f-size-1-2">1 meditação para cada 1<br class="hideText">
                dos 7 chakras e 1 para<br class="hideText">
                equilíbrio de todos os<br class="hideText">
                chakras ao mesmo<br class="hideText">
                tempo.</p>
        </div>

        <div class="d-flex flex-column greyBackground align-items-center col-xl-2 col-lg-3 col-md-3 col-sm-12 col-12 text-center cardTreinamentos">
            <img src="./assets/portaisInteriores/portaisInteriores/cardTreinamento-2.webp" class="card-img imgCardTreinamentos" alt="">
            <p class="roxoClaro montserrat f-size-1-7 lh-1 text-weight-bolder mt-4">Yoga do Destravar<br class="hideText">
                Energético</p>
            <p class="montserrat f-size-1-2">1 prática de yoga para<br class="hideText">
                cada 1 dos 7 chakras e 1<br class="hideText">
                para equilíbrio de todos<br class="hideText">
                    os chakras.</p>
        </div>

        <div class="d-flex flex-column greyBackground align-items-center col-xl-2 col-lg-3 col-md-3 col-sm-12 col-12 text-center cardTreinamentos">
            <img src="./assets/portaisInteriores/portaisInteriores/cardTreinamento-3.webp" class="card-img imgCardTreinamentos" alt="">
            <p class="roxoClaro montserrat f-size-1-7 lh-1 text-weight-bolder mt-4">Desbloqueio do<br class="hideText">
                Poder da mente<br class="hideText">
                e do corpo</p>
            <p class="montserrat f-size-1-2">Entenda como regular,<br class="hideText">
                equilibrar e destravar o<br class="hideText">
                poder da sua mente e<br class="hideText">
                do seu corpo através da<br class="hideText">
                meditação.</p>
        </div>

        <div class="d-flex flex-column greyBackground align-items-center col-xl-2 col-lg-3 col-md-3 col-sm-12 col-12 text-center cardTreinamentos">
            <img src="./assets/portaisInteriores/portaisInteriores/cardTreinamento-4.webp" class="card-img imgCardTreinamentos" alt="">
            <p class="roxoClaro montserrat f-size-1-7 lh-1 text-weight-bolder mt-4">Atuar contra os<br class="hideText">
                males do século</p>
            <p class="montserrat f-size-1-2">A meditação é a chave<br class="hideText">
                para te garantir um<br class="hideText">
                corpo e mente mais<br class="hideText">
                saudáveis e te mostrar<br class="hideText">
                como lidar com a<br class="hideText">
                    ansiedade, insônia,<br class="hideText">
                    estresse e depressão.</p>
        </div>
    </div>
</div>

<div class="container-fluid mt-6">
    <br class="showElement"><br class="showElement">
    <div class="text-center mt-1">
        <div class="f-size-2-2 text-weight-bolder montserrat lh-1 text25">
            Se inscreva hoje e garanta <span class="roxoClaro">4 bônus exclusivos</span>
        </div>
        <div class="f-size-1-1 text-weight-bold mt-3 interSemi text16">
        Tudo o que você precisa para alcançar o seu potencial na meditação de forma rápida e fácil.
        </div>
    </div>
    <div class="showElement mt-n15"></div>  
    <div class="row centerOnMobile mt-7">
        <div class="col-md-1 col-xl-2"></div>
        <div class="col-md-3 col-xl-2 mt-5 d-flex justify-content-center justify-content-md-start align-items-end">
            <img src="./assets/portaisInteriores/livroOleos.webp" alt="" class="card-img livrosBanners">
        </div>
        <div class="col-md-5 mt-6">
            <button class="btn btn-sm buttonBonus f-size-0-7 text-color-white text-weight-bold">BÔNUS 1</button>
            <div class="f-size-2-2 text-weight-bolder montserrat">
                Essências Naturais de Poder
            </div>
            <div class="f-size-1-2 mt-2 text-weight-medium lh-25px">
                Aula + Manual para você saber como potencializar sua vibração <br class="hideElement"> energética. Descubra como usar cada óleo essencial para o tipo de <br class="hideElement"> Chakra específico e manter maiores períodos de equilíbrio.
            </div>
        </div>
        <div class="col-md-2 mt-10 f-size-1-7 blocoValorPromocional text-weight-bold text-center">
            Valor normal: <br class="hideElement">
            <span class="text-red positionRelative">
                R$ 197,00
                <svg xmlns="http://www.w3.org/2000/svg" class="svgRedStroke" viewBox="0 0 500 150" preserveAspectRatio="none"><path class="pathRedStroke" d="M13.5,15.5c131,13.7,289.3,55.5,475,125.5"></path></svg>
            </span>
            Valor <br class="hideElement"> nesta oferta: <br class="hideElement">
            <div class="azulCiano text-weight-extrabold f-size-1-8">GRÁTIS</div>
        </div>
    </div>
    <br><br><br>
</div>

<div class="container-fluid primeiroBackground text-color-white">
    <div class="row centerOnMobile">
        <div class="col-md-1 col-xl-2"></div>
        <div class="col-md-3 col-xl-2 mt-5 d-flex justify-content-center justify-content-md-start align-items-end">
            <img src="./assets/portaisInteriores/livro25passos.webp" alt="" class="card-img livrosBanners">
        </div>
        <div class="col-md-5 mt-6">
            <button class="btn btn-sm buttonBonus f-size-0-7 text-weight-bold text-color-white montserrat">BÔNUS 2</button>
            <div class="f-size-2-2 text-weight-bolder montserrat">
                25 Passos Para Iniciantes na <br class="hideElement"> Meditação
            </div>
            <div class="f-size-1-2 mt-2 text-weight-medium lh-25px">
                Livro digital com minha linha Checklist pessoal, com as minhas <br class="hideElement"> melhores dicas de como meditar de verdade! Aplique esses passos <br class="hideElement"> e avance 20 anos de experiência em Meditação.
            </div>
        </div>
        <div class="col-md-2 mt-10 f-size-1-7 text-weight-bold text-center blocoValorPromocional">
            Valor normal: <br class="hideElement">
            <span class="text-red positionRelative">
                R$ 97,00
                <svg xmlns="http://www.w3.org/2000/svg" class="svgRedStroke" viewBox="0 0 500 150" preserveAspectRatio="none"><path class="pathRedStroke" d="M13.5,15.5c131,13.7,289.3,55.5,475,125.5"></path></svg>
            </span>
            Valor <br class="hideElement"> nesta oferta: <br class="hideElement">
            <div class="azulCiano text-weight-extrabold f-size-1-8">GRÁTIS</div>
        </div>
    </div>
    <br><br><br>
</div>

<div class="container-fluid">
    <div class="row centerOnMobile">
        <div class="col-md-1 col-xl-2"></div>
        <div class="col-md-3 col-xl-2 mt-5 d-flex justify-content-center justify-content-md-start align-items-end">
            <img src="./assets/portaisInteriores/livroLive.webp" alt="" class="card-img livrosBanners">
        </div>
        <div class="col-md-5 mt-6">
            <button class="btn btn-sm buttonBonus f-size-0-7 text-weight-bold text-color-white montserrat">BÔNUS 3</button>
            <div class="f-size-2-2 text-weight-bolder montserrat">
                Live com a Sannyasini Kritika Giri Ji
            </div>
            <div class="f-size-1-2 mt-2 text-weight-medium lh-25px">
                Áudio da live super especial no aniversário de Buddha com a <br class="hideElement"> Sannyasini Kritika Giri Ji. Esse é um dos meus presentes mais <br class="hideElement"> verdadeiros para você.
            </div>
        </div>
        <div class="col-md-2 mt-10 f-size-1-7 text-weight-bold text-center blocoValorPromocional">
            Valor normal: <br class="hideElement">
            <span class="text-red positionRelative">
                R$ 97,00
                <svg xmlns="http://www.w3.org/2000/svg" class="svgRedStroke" viewBox="0 0 500 150" preserveAspectRatio="none"><path class="pathRedStroke" d="M13.5,15.5c131,13.7,289.3,55.5,475,125.5"></path></svg>
            </span>
            Valor <br class="hideElement"> nesta oferta: <br class="hideElement">
            <div class="azulCiano text-weight-extrabold f-size-1-8">GRÁTIS</div>
        </div>
    </div>
    <br><br><br>
</div>

<div class="container-fluid primeiroBackground text-color-white">
    <div class="row centerOnMobile">
        <div class="col-md-1 col-xl-2"></div>
        <div class="col-md-3 col-xl-2 mt-5 d-flex justify-content-center justify-content-md-start align-items-end">
            <img src="./assets/portaisInteriores/livroJornada.webp" alt="" class="card-img livrosBanners">
        </div>
        <div class="col-md-5 mt-6">
            <button class="btn btn-sm buttonBonus f-size-0-7 text-weight-bold text-color-white montserrat">BÔNUS 4</button>
            <div class="f-size-2-2 text-weight-bolder montserrat">
                45 Práticas Guiadas em Áudio
            </div>
            <div class="f-size-1-2 mt-2 text-weight-medium lh-25px">
                Serei sua guia para te acompanhar durante os 4 passos primordiais <br class="hideElement"> que você precisa saber para ir do absoluto zero até o estado mais <br class="hideElement"> elevado da meditação.
            </div>
        </div>
        <div class="col-md-2 mt-10 f-size-1-7 text-weight-bold text-center blocoValorPromocional">
            Valor normal: <br class="hideElement">
            <span class="text-red positionRelative">
                R$ 297,00
                <svg xmlns="http://www.w3.org/2000/svg" class="svgRedStroke" viewBox="0 0 500 150" preserveAspectRatio="none"><path class="pathRedStroke" d="M13.5,15.5c131,13.7,289.3,55.5,475,125.5"></path></svg>
            </span>
            Valor <br class="hideElement"> nesta oferta: <br class="hideElement">
            <div class="azulCiano text-weight-extrabold f-size-1-8">GRÁTIS</div>
        </div>
    </div>
    <br><br><br>
</div>


<!-- <div class="container-fluid primeiroBackground">
    <div class="row text-center">
        <div class="col-xl-12 mt-5 mb-5">
            <button class="btn btn-lg cianoButton cianShinyBorderButtons mt-4 f-size-1-2 text-weight-bold montserrat">SIM! QUERO DOMINAR A PRÁTICA DA YOGA</button>
            <div class="text-weight-bold mt-2 f-size-1-1 inter text-color-white">
                Aperte no botão acima para garantir a sua vaga
            </div>
        </div>
    </div>
    <br>
</div> -->

<!-- Swiper 2 OK -->
<div class="container-fluid">
    <div class="text-center mt-7">
        <div class="f-size-2-2 text-weight-bolder montserrat lh-1-4 text25">
            <span class="roxoClaro">Veja o que é possível conquistar</span> <br class="hideElement"> acessando seus 7 portais através da Meditação
        </div>
        <div class="f-size-1-1 text-weight-bold interSemi mt-2 text16">
            Logo no início já é possível sentir os resultados expressivos e positivos.
        </div>
    </div>
    <br class="showElement">
    <br class="showElement">
    <div class="row">
    <div class="swiper mySwiper mt-6">
            <div class="swiper-wrapper">
                <div class="swiper-slide setWidthSlider"><img src="./assets/portaisInteriores/portaisInteriores/swiper-2-1.webp"></div>
                <div class="swiper-slide setWidthSlider"><img src="./assets/portaisInteriores/portaisInteriores/swiper-2-2.webp"></div>
                <div class="swiper-slide setWidthSlider"><img src="./assets/portaisInteriores/portaisInteriores/swiper-2-3.webp"></div>
                <div class="swiper-slide setWidthSlider"><img src="./assets/portaisInteriores/portaisInteriores/swiper-2-4.webp"></div>
                <div class="swiper-slide setWidthSlider"><img src="./assets/portaisInteriores/portaisInteriores/swiper-2-5.webp"></div>
            </div>
        </div>
    </div>
    <br class="hideElement"><br class="hideElement">
</div>

<div class="container-fluid primeiroBackground">
    <div class="row text-center">
        <div class="col-md-4"></div>
        <div class="col-md-4 mt-5">
            <div class="card cianShinyBorderCard">
                <div class="card-body">
                    <div class="row text-center">
                        <div class="col-xl-1"></div>
                        <div class="col-xl-10">
                            <div class="f-size-1-7 text-weight-bolder montserrat lh-30px text25">
                                Turma especial por <span class="roxoClaro">tempo <br> limitado!</span>
                            </div>
                            <div class="f-size-1 text-weight-medium inter mt-2 text16">
                                Aproveite enquanto pode, pois esta oferta vai <br class="hideElement"> realmente sumir a qualquer instante.
                            </div>
                            <img src="./assets/portaisInteriores/imgOferta.webp" alt="" class="card-img mt-3 mb-2">

                            <div class="text-center f-size-1 lh-1-2 mt-3">
                                O equilíbrio da sua mente e corpo através da <br class="hideElement">
                                meditação <span class="text-weight-bold">começa agora.</span> Garantia de <br class="hideElement">
                                Desconto exclusivo para este produto, e <br class="hideElement">
                                <u>apenas nesta página exclusiva.</u>
                            </div>

                            <div class="text-center roxoClaro f-size-1-5 text-weight-bold mt-3">
                                O que você poderá levar hoje:
                            </div>

                            <!-- ROWS BONUS -->
                            <div class="row">
                                <div class="col-9 col-md-10 mt-4 colCardTituloOferta">
                                    <div class="f-size-0-95 d-flex justify-items-start align-items-end montserrat textOferta text-weight-bold">
                                        Treinamento 7 portais internos &nbsp;<span class="dotSpan"></span>
                                    </div>
                                </div>
                                <div class="col-3 col-md-2 colCardPrecoOferta d-flex align-items-end justify-content-end f-size-1-2 text-weight-bold montserrat">
                                <span class="text-red ms-3 priceOferta"><s>R$ 97</s></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-9 col-md-10 mt-4 colCardTituloOferta">
                                    <div class="f-size-0-95 d-flex justify-items-start align-items-end montserrat textOferta text-weight-bold">
                                        Essências Naturais de Poder &nbsp;<span class="dotSpan"></span>
                                    </div>
                                </div>
                                <div class="col-3 col-md-2 colCardPrecoOferta d-flex align-items-end justify-content-end f-size-1-2 text-weight-bold montserrat">
                                <span class="text-red ms-3 priceOferta"><s>R$ 297</s></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-9 col-md-10 mt-4 colCardTituloOferta">
                                    <div class="f-size-0-95 d-flex justify-items-start align-items-end montserrat textOferta text-weight-bold">
                                        25 Passos para facilitar a meditação &nbsp;<span class="dotSpan"></span>
                                    </div>
                                </div>
                                <div class="col-3 col-md-2 colCardPrecoOferta d-flex align-items-end justify-content-end f-size-1-2 text-weight-bold montserrat">
                                <span class="text-red ms-3 priceOferta"><s>R$ 69</s></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-9 col-md-10 mt-4 colCardTituloOferta">
                                    <div class="f-size-0-95 d-flex justify-items-start align-items-end montserrat textOferta text-weight-bold">
                                        Live com a Sannyasini Kritika Girl Ji &nbsp;<span class="dotSpan"></span>
                                    </div>
                                </div>
                                <div class="col-3 col-md-2 colCardPrecoOferta d-flex align-items-end justify-content-end f-size-1-2 text-weight-bold montserrat">
                                <span class="text-red ms-3 priceOferta"><s>R$ 147</s></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-9 col-md-10 mt-4 colCardTituloOferta">
                                    <div class="f-size-0-95 d-flex align-items-end justify-items-start montserrat textOferta maiorTexto text-weight-bold">
                                        Jornada do Zero à Meditação Curativa &nbsp;<span class="dotSpan"></span>
                                    </div>
                                </div>
                                <div class="col-3 col-md-2 colCardPrecoOferta d-flex align-items-end justify-content-end f-size-1-2 text-weight-bold montserrat">
                                <span class="text-red ms-3 priceOferta"><s>R$ 497</s></span>
                                </div>
                            </div>
                            <!-- END ROW BONUS -->

                            <div class="text-center mt-4 f-size-1-3 text-weight-bold textValorCortadoCard">
                                <i>
                                    de: <span class="text-red"><s>R$ 1.385,00</s></span> por apenas:
                                </i>
                            </div>

                            <div class="text-center mt-2 text-weight-bolder f-size-3">
                                <span class="parcelamentoOferta">
                                    12x de 
                                </span>
                                <span class="">R$ 9,70</span>
                            </div>
                            
                            <div class="text-center text-weight-medium f-size-1-3">
                                ou R$97,00 à vista
                            </div>

                            <a href="https://sun.eduzz.com/400372" target="__blank" class="btn btn-lg mt-4 f-size-1-1 text-weight-bolder text-color-white btnCardOferta montserrat">QUERO ACESSAR MEUS 7 PORTAIS</a>
                            <div class="f-size-0-8 mt-3 text-weight-bold inter">
                                *Aproveite enquanto esta oferta ainda está disponível*
                            </div>
                            <img src="./assets/portaisInteriores/compraSegura.webp" alt="" class="card-img mt-3 mb-2">
                        </div>
                        <div class="col-xl-1"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4"></div>
    </div>
    <br><br><br><br>

    <div class="elementor-shape elementor-shape-top" data-negative="false"> <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1000 20" preserveAspectRatio="none"> <path class="svg02" d="M0,0v3c0,0,393.8,0,483.4,0c9.2,0,16.6,7.4,16.6,16.6c0-9.1,7.4-16.6,16.6-16.6C606.2,3,1000,3,1000,3V0H0z"></path> </svg></div>
</div>

<div class="container-fluid">
    <div class="row justify-content-center flex-xl-row flex-lg-row flex-md-row-reverse flex-sm-column-reverse flex-column-reverse">
        <div class="col-lg-1 col-md-1 col-xl-2 hideText"></div>
        <div class="col-md-6 col-lg-5 col-xl-5 col-sm-12 col-12 mt-10 marginTopGarantia">
            <div class="f-size-2-2 text-weight-bolder montserrat lh-1 textCenter text25">
            O treinamento 7 portais internos é um
            <br class="hideElement"> Investimento <span class="roxoClaro">com risco zero.</span>
            </div>
            <div class="f-size-1-1 text-weight-bold interSemi mt-2 textCenter text16">
            Sua satisfação garantida ou seu dinheiro de volta

            </div>
            <div class="f-size-1-3 mt-3 montserrat lh-25px text-weight-medium textCenter text16">
                Vou entregar em suas mãos <span class="text-weight-bold">7 dias para degustar a vontade.</span>
                <br><br>
                Exatamente! Se, por qualquer motivo, você não gostar, basta me enviar um e-mail dizendo “Quero cancelar” e você terá de volta cada centavo que investiu.
                <br><br>
                Ou seja, se der certo, <span class="text-weight-bold">você vai transformar sua vida.</span> E se, na pior das hipóteses, der errado, você tem o seu dinheiro de volta. Simples assim!
                <br><br>
                Só quero comigo quem realmente se conectar com o que tenho para ensinar.
            </div>
            
        </div>
        <div class="col-sm-10 col-8 col-md-5 col-lg-5 col-xl-4 mt-10 d-flex align-items-center justifyContentCenter">
            <img src="./assets/portaisInteriores/seloGarantia.webp" alt="" class="card-img selo">
        </div>
        <div class="col-xl-1 col-md-1 col-lg-1 hideText"></div>
    </div>
    <br><br><br>
</div>

<div class="container-fluid backgroundRoxoLinear">
    <div class="row mt-6 flex-xl-row flex-lg-row flex-md-row-reverse flex-sm-column-reverse flex-column-reverse">
        <div class="col-lg-1 col-md-1 col-xl-2"></div>
        <div class="col-md-5 col-xl-5 col-lg-6 col-12 col-sm-12 text-color-white">
            <div class="mt-6 f-size-2-2 text-weight-bolder montserrat textCenter text25">
                Bianca Vita
            </div>
            <div class="mt-4 f-size-1-2 textoAboutBianca montserrat text-weight-medium lh-25px textCenter text16">
            Meu objetivo principal, através da prática, é repassar todo meu<br class="hideElement">
             conhecimento e experiência, em ganho de energia e equilíbrio no<br class="hideElement">
              físico, emocional, mental e energético. Para quem busca uma vida<br class="hideElement">
               mais sintonizada e se manter equilibrada(o) em meio ao caos.
            <br><br>
            Sou apaixonada por Yoga. Praticante desde 2001, e professora<br class="hideElement">
             desde 2004. Nasci em São Paulo, mas atualmente moro no Rio de<br class="hideElement">
              Janeiro. Tenho mais de 50 cursos de especializações variadas e<br class="hideElement">
               com professores de diferentes países. Tenho especialização<br class="hideElement">
                também em Yoga para criança.
            <br><br>
            Sou Reikiana nível 3 e tenho uma segunda e mais recente<br class="hideElement">
             formação de Yoga com certificação internacional. Ou seja, estou<br class="hideElement">
              sempre estudando, evoluindo e me reciclando, como praticante e<br class="hideElement">
               profissional.
            </div>
            <div class="row">
                <div class="col-md-10 col-xl-10 col-lg-10 col-12 col-sm-12 mt-4 d-flex d-sm-flex flex-xl-row flex-lg-row flex-md-row flex-sm-column flex-column">
                    <a href="https://www.instagram.com/biancavita/" target="__blank" class="justifyContentCenter btn btnInstagramBianca col-xl-5 col-lg-5 col-md-5 col-sm-5 col-5">
                        <span><i class="fa-brands fa-instagram iconInstagramBianca"></i></span> Instagram
                    </a>
                    <a href="https://www.youtube.com/c/BiancaVitaYoga" target="__blank" class="justifyContentCenter btn btnYoutubeBianca col-xl-5 col-lg-5 col-md-5 col-sm-5 col-5">
                        <span><i class="fa-brands fa-youtube iconYoutubeBianca"></i></span> Youtube
                    </a>
                </div>
            </div>
        </div>
        <div class="mt-7 col-4 col-sm-8 col-md-6 col-lg-3 col-xl-3 imgAboutBianca justifyContentCenter">
        </div>
        <div class="col-xl-1 col-lg-1 col-md-1"></div>
    </div>
    <br><br>

    <div class="elementor-shape elementor-shape-top" data-negative="false"> <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1000 20" preserveAspectRatio="none"> <path class="svg02 claro" d="M0,0v3c0,0,393.8,0,483.4,0c9.2,0,16.6,7.4,16.6,16.6c0-9.1,7.4-16.6,16.6-16.6C606.2,3,1000,3,1000,3V0H0z"></path> </svg></div>
</div>

<div class="container-fluid mt-6">
    <div class="text-center">
        <div class="text-weight-bolder mt-4 f-size-2-2 montserrat text25">
            Perguntas frequentes
        </div>
        <div class="text-weight-bold f-size-1-1 interSemi text16">
            Ainda com dúvidas? Aqui estão as respostas.
        </div>

        <div class="row">
            <div class="col-md-2 col-xl-3"></div>
            <div class="col-md-8 col-xl-6 mt-4 mb-2">
                <?php require('accordionPortais.php') ?>
            </div>
            <div class="col-md-2 col-xl-3"></div>
        </div>

    </div>
    <br><br>
</div>