<div class="container-fluid backgroundInicial">
    <div id="setTransition" class="set-transition-down-up">
        <div class="row">
            <div class="col-3 col-md-5"></div>
            <div class="col-6 col-md-2 text-center mt-3">
                <img src="./assets/iniciantesVsl/logo.webp" alt="" class="card-img">
            </div>
            <div class="col-3 col-md-5"></div>
        </div>
    
        <div class="row mt-4">
            <div class="col-md-2"></div>
            <div class="col-md-8 text-color-white text-center">
                <span class="f-size-2-2 lh-1 text-weight-bolder">
                    Domine a prática secreta que nasceu na Índia <span class="azulCiano">e livre a sua vida</span> dos seus problemas rotineiros.
                </span>
                <div class="f-size-1 text-weight-bold mt-2">
                    Elimine a ansiedade, o estresse, a insônia e outros problemas causados pela sua rotina puxada, <span class="azulCiano">sem precisar sair de casa.</span>
                </div>
                <div class="f-size-1-2 mt-4">
                    <span class="azulCiano f-size-0-9"><i class="fas fa-arrow-down"></i></span>&nbsp; Assista o vídeo Abaixo imediatamente
                </div>
                <br>
            </div>
            <div class="col-md-2"></div>
        </div>
    
        <div class="row">
            <div class="col-xl-3"></div>
            <div class="col-xl-6">
                <lite-vimeo class="cianShinyBorder" videoid="736132303" title="Iniciar Vídeo"></lite-vimeo>
            </div>
            <div class="col-xl-3"></div>
        </div>
    
        <div class="row mt-4">
            <div class="col-md-2"></div>
            <div class="col-md-8 text-center text-color-white">
                <div class="f-size-1-2">
                    <span class="azulCiano f-size-0-9"><i class="fas fa-volume-up"></i></span>&nbsp; Confira se o seu áudio está ligado
                </div>
                <div class="mt-3 f-size-1-3">
                    Aprenda como retomar o controle da sua vida e das suas emoções através da Yoga da Vida Real <span class="text-weight-bolder">com apenas 15 minutos por dia</span> e sem gastar rios de dinheiro.
                </div>
    
                <a href="https://checkout.biancavita.com/checkout/62caccbd557b525a5f60cf79" class="btn btn-lg mt-4 cianoButton text-weight-bold f-size-1-1 cianShinyBorderButtons">
                    <div class="d-flex align-items-center justify-content-center mt-1">
                        SIM! QUERO ENTRAR PARA O TREINAMENTO
                    </div>
                </a>
            </div>
            <div class="col-md-2"></div>
        </div>
    </div>

    <br>
    <br><br class="hideElement"><br class="hideElement">
    <div class="elementor-shape elementor-shape-bottom" data-negative="false"> <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 283.5 19.6" preserveAspectRatio="none"> <path class="elementor-shape-fill" style="opacity:0.33" d="M0 0L0 18.8 141.8 4.1 283.5 18.8 283.5 0z"></path> <path class="elementor-shape-fill" style="opacity:0.33" d="M0 0L0 12.6 141.8 4 283.5 12.6 283.5 0z"></path> <path class="elementor-shape-fill" style="opacity:0.33" d="M0 0L0 6.4 141.8 4 283.5 6.4 283.5 0z"></path> <path class="elementor-shape-fill" d="M0 0L0 1.2 141.8 4 283.5 1.2 283.5 0z"></path> </svg></div>
</div>