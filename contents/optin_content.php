<div class="container-fluid">
    <div class="row mt-8 rowPrincipal">
        <div class="col-lg-2"></div>
        <div class="col-lg-8">
            <form method="POST" action="https://mabarros.activehosted.com/proc.php" id="_form_41_" class="_form _form_41 _inline-form  _dark" novalidate>
                <input type="hidden" name="u" value="41" />
                <input type="hidden" name="f" value="41" />
                <input type="hidden" name="s" />
                <input type="hidden" name="c" value="0" />
                <input type="hidden" name="m" value="0" />
                <input type="hidden" name="act" value="sub" />
                <input type="hidden" name="v" value="2" />
                <input type="hidden" name="or" value="7994876aff952639da8b61af63ef2602" />

                <input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" id="fullname" name="fullname" placeholder="Nome" required>

                <input type="text" class="form-control mt-2" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" id="email" name="email" placeholder="E-mail" required>

                <input type="text" class="form-control mt-2" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" id="phone" name="phone" pattern="[0-9()#&+*-=.]+" placeholder="DDD + Cel" required>

                <input type="hidden" id="field[30][time]" name="field[30][time]" placeholder="" />
                <input type="hidden" id="field[31]" name="field[31]" value="" placeholder="" />
                <input type="hidden" id="field[32]" name="field[32]" value="" placeholder="" />
                <input type="hidden" id="field[33]" name="field[33]" value="" placeholder="" />
                <input type="hidden" id="field[34]" name="field[34]" value="" placeholder="" />
                <input type="hidden" id="field[35]" name="field[35]" value="" placeholder="" />

                <button type="submit" id="_form_41_submit" class="btn btn-primary mt-2 col-12 f-size-0-9 text-weight-medium purpleColor">QUERO FAZER MINHA INSCRIÇÃO</button>
            </form>
        </div> <div class="col-lg-2"></div>
    </div>
</div>