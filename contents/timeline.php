<div class="wrapper">
    <div class="centerLine">
        <a href="#" class="scroll-icon"></a>
    </div>
    <div class="row divIconTop justify-content-sm-center justify-content-xs-start justify-content-sm-start hideElement">
        <i class="icon-top fa-solid fa"></i>
        <br>
    </div>
    <div class="row row-1">
        <div class="showElement timelineTitle f-size-1-6 azulCiano text-weight-bolder montserrat mt-3 ml-3">Área de Membros Exclusiva</div>
        <section>
        <i class="icon fa-solid fa-laptop"></i>
        <p class="text-weight-medium f-size-1-3 text-start cardText d-flex justify-content-center">
            No momento em que você<br class="hideElement"> adquirir o treinamento Yoga Para<br class="hideElement"> Iniciantes, você receberá no seu<br class="hideElement"> e-mail uma confirmação com<br class="hideElement"> login e senha de acesso exclusivo<br class="hideElement"> para começar a sua jornada.
        </p>
        </section>
        <div class="timelineTitle f-size-1-6 azulCiano text-weight-bolder montserrat mt-3 ml-3 hideElement">Área de Membros Exclusiva</div>
    </div>
    <div class="row row-2">
        <div class="showElement timelineTitle f-size-1-6 azulCiano text-weight-bolder montserrat mt-3 ml-3">Aulas Dinâmicas e 100% Online</div>
        <section>
        <i class="icon fa-regular fa-circle-check"></i>
        <p class="text-weight-medium f-size-1-3 text-start d-flex justify-content-center">
            Com isso em mãos você terá<br class="hideElement"> acesso a aulas objetivas, práticas<br class="hideElement"> e direto ao ponto. Podendo<br class="hideElement"> assistir quando quiser, de onde<br class="hideElement"> quiser e a qualquer hora que<br class="hideElement"> quiser para facilitar a execução<br class="hideElement"> do seu plano.
        </p>
        </section>
        <div class="timelineTitle2 f-size-1-6 azulCiano text-weight-bolder montserrat mt-3 ml-3 hideElement">Aulas Dinâmicas e 100% Online</div>
    </div>
    <div class="row row-1">
        <div class="showElement timelineTitle f-size-1-6 azulCiano text-weight-bolder montserrat mt-3 ml-3">Suporte Completo na Plataforma</div>
        <section>
        <i class="icon fas fa-star"></i>
        <p class="text-weight-medium f-size-1-3 text-start d-flex justify-content-center">
            Algum problema ou feedback<br class="hideElement"> importante para tratar conosco?<br class="hideElement"> Apenas comente na plataforma<br class="hideElement"> ou entre em contato direto e nós<br class="hideElement"> te auxiliamos em tudo o que<br class="hideElement"> você precisar referente ao seu<br class="hideElement"> objetivo.
        </p>
        </section>
        <div class="timelineTitle3 f-size-1-6 azulCiano text-weight-bolder montserrat mt-3 ml-3 hideElement">Suporte Completo na Plataforma</div>
    </div>
</div>