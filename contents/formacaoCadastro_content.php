<div class="h-100 container-fluid">
    <div class="row">
        <div class="col-md-4 col-lg-4 col-sl-5"></div>
        <div class="col-md-4 col-lg-4 col-sl-2 mt-5 text-center">
            <img src="assets/formacaoCadastro/LOGO-2.webp" alt="" class="card-img logoImg">
        </div>
        <div class="col-md-4 col-lg-4 col-sl-5"></div>
    </div>
    <div class="row">
        <div class="col-md-1 col-lg-1 col-sl-2"></div>
        <div class="col-md-10 col-lg-10 col-sl-8 mt-5 text-center">
            <div class="f-size-1-5 blueColor text-weight-bold">
            Seja uma professora certificada pela Classical Yoga International de Varanasi na Índia 🇮🇳
            </div>
            <div class="f-size-1-4 purpleColor mt-2">
            e aprenda tudo que você precisa pra viver da sua paixão pelo Yoga, conseguindo alunos fiéis e não passando mais perrengues financeiros.
            </div>

            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <div class="mt-4 text-center sizeVideo" id="loadvideo">
                        <lite-youtube videoid="HBij8yYLajQ" style="background-image: url('assets/formacaoCadastro/hqdefault.webp');">
                            <a href="#loadvideo" class="lty-playbtn" title="Iniciar Video">
                                <span class="lyt-visually-hidden">Video Iniciado</span>
                            </a>
                        </lite-youtube>
                    </div>
                </div>
                <div class="col-md-2"></div>
            </div>


            <br><br>

            <a href="https://leadzap.me/17196/FBV" target="__blank">
                <button class="btn btn-site f-size-1-4 text-weight-bold">
                    Eu quero ficar por dentro!
                </button>
            </a>

            <br><br>
            <br><br>
            <br><br>
            <br><br>
        

        </div>
        <div class="col-md-1 col-lg-1 col-sl-2"></div>
    </div>
</div>