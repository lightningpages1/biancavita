<div class="container-fluid primeiroBackground positionRelative">

    <div class="row">
        <div class="col-2 col-md-4 col-xl-5"></div>
        <div class="col-8 col-md-4 col-xl-2 text-center mt-3">
            <img src="./assets/yogaIniciantes/logo.webp" alt="" class="card-img logoYoga">
        </div>
        <div class="col-2 col-md-4 col-xl-5"></div>
    </div>

    <div class="row mt-4 justify-content-center">
        <!-- <div class="col-md-2 col-xl-3"></div> -->
        <div class="col-md-8 col-xl-8 containerTitle text-color-white text-center">
            <span class="f-size-2-2 lh-1 text-weight-bolder montserrat titleText">
                Domine a prática secreta que nasceu na Índia <span class="azulCiano">e <br class="hideElement"> livre a sua vida</span> dos seus problemas rotineiros.
            </span>
            <div class="f-size-1-1 text-weight-bold mt-2 inter titleText2 containerTitle2">
                Elimine a ansiedade, o estresse, a insônia e outros problemas causados pela sua rotina puxada, <span class="azulCiano">sem <br class="hideElement"> precisar sair de casa.</span>
            </div>
        </div>
        <!-- <div class="col-md-2 col-xl-3"></div> -->
    </div>

    <div class="row mt-4 set-transition-down-up justify-content-center" id="setTransition">
        <div class="col-md-8 col-xl-5">
            <lite-vimeo class="cianShinyBorderButtons" videoid="736132303" title="Iniciar Vídeo"></lite-vimeo>
        </div>
    </div>


    <div class="row mt-4 justify-content-center">
        <!-- <div class="col-md-2 col-xl-3"></div> -->
        <div class="text-center text-color-white">
            <div class="mt-1 f-size-1-3 montserrat lh-25px textVideo text-weight-medium">
                Aprenda como retomar o controle da sua vida e das suas emoções através da Yoga da <br class="hideElement"> Vida Real <span class="text-weight-bolder">com apenas 15 minutos por dia</span> e sem gastar rios de dinheiro.
            </div>
        </div>
        <!-- <div class="col-md-2 col-xl-3"></div> -->
    </div>

    <br><br><br><br>

    <div class="elementor-shape elementor-shape-bottom" data-negative="false"> <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 283.5 19.6" preserveAspectRatio="none"> <path class="svgInicial" style="opacity:0.33" d="M0 0L0 18.8 141.8 4.1 283.5 18.8 283.5 0z"></path> <path class="svgInicial" style="opacity:0.33" d="M0 0L0 12.6 141.8 4 283.5 12.6 283.5 0z"></path> <path class="svgInicial" style="opacity:0.33" d="M0 0L0 6.4 141.8 4 283.5 6.4 283.5 0z"></path> <path class="svgInicial" d="M0 0L0 1.2 141.8 4 283.5 1.2 283.5 0z"></path> </svg></div>

</div>

<div class="container-fluid segundoBackground positionRelative">
    <div class="row text-color-white justify-content-center align-items-start">
        <div class="containerIconesIniciais col-md-3 col-xl-3 mt-6 d-flex align-items-center justify-content-start">
            <div>
                <div class="">
                    <img src="./assets/yogaIniciantes/handMoneyIcon.webp" alt="" class="card-img">
                </div>
            </div>
            <div class="ms-2 containerHeight">
                <span class="azulCiano montserrat f-size-1-7 text-weight-bolder">7 dias grátis</span>
                <p class="textosIconesIniciais text-weight-medium">Experimente por 7 dias gratuitos sem<br> compromisso e sem pagar nada por isso.</p>
            </div>
        </div>

        <div class="containerIconesIniciais col-md-3 col-xl-3 mt-6 d-flex align-items-center justify-content-start">
            <div>
                <div class="">
                    <img src="./assets/yogaIniciantes/securityIcon.webp" alt="" class="card-img">
                </div>
            </div>
            <div class="ms-2 containerHeight">
                <span class="azulCiano montserrat f-size-1-7 text-weight-bolder">Pagamento 100% seguro</span>
                <p class="textosIconesIniciais text-weight-medium">Somos parceiros da maior e melhor<br> plataforma de pagamentos do Brasil.</p>
            </div>
        </div>

        <div class="containerIconesIniciais col-md-3 col-xl-3 mt-6 d-flex align-items-center justify-content-start">
            <div>
                <div class="">
                    <img src="./assets/yogaIniciantes/acessoImediatoIcon.webp" alt="" class="card-img">
                </div>
            </div>
            <div class="ms-2 containerHeight">
                <span class="azulCiano montserrat f-size-1-7 text-weight-bolder">Acesso imediato</span>
                <p class="textosIconesIniciais text-weight-medium">Seu acesso ao material será enviado ao seu<br> e-mail logo após processarmos o<br> pagamento.</p>
            </div>
        </div>
    </div>

    <br><br><br><br>
    <br><br><br><br>

    <div class="elementor-shape elementor-shape-bottom" data-negative="false"> <svg xmlns="http://www.w3.org/2000/svg" class="svgPalacio" viewBox="0 0 4080 400" preserveAspectRatio="none"> <path class="svgSecundario ha-shape-divider" d="M1412,60.3c2.6,0.4,5.6,0.7,9,1.1c0,0,0,0,0,0c177.3,22.9,254.6,47.3,271.9,53.4c2.1,0.7,3.3,2.7,3.3,4.9
 c0,0,0,0,0,0c0,5.1,3.3,9.4,7.8,11.1c0,0.1,0,0.1,0,0.2c0,1.7,1.1,3.2,2.7,3.8l0.6,1.7c-0.1,0.2-0.2,0.3-0.2,0.5
 c0,0.4,0.2,0.7,0.6,0.9l0.4,5.4c0,0,0,0,0,0c0,0,0.1,0,0.1,0l0.4-5.4c0.3-0.2,0.6-0.5,0.6-0.9c0-0.2-0.1-0.4-0.2-0.5l0.6-1.7
 c1.6-0.6,2.7-2,2.7-3.8c0-0.1,0-0.1,0-0.2c3.5-1.3,6.3-4.2,7.3-7.8v13.7h-0.2c-0.5,0-0.9,0.4-0.9,0.9v0.5c0,0.5,0.4,0.9,0.9,0.9h1.3
 c-0.1,0.3-0.1,0.7-0.1,1.1c0,2.5,1.3,4.7,3.3,5.9c1.2,0.8,2.3,1.7,3.1,2.9c-0.2,0.1-0.3,0.4-0.3,0.6c0,0.3,0.2,0.6,0.5,0.7l0.2,3.2
 h0.2l0.2-3.2c0.3-0.1,0.5-0.4,0.5-0.7c0-0.2-0.1-0.4-0.3-0.6c0.8-1.2,1.9-2.1,3.1-2.9c2-1.2,3.3-3.4,3.3-5.9c0-0.4,0-0.7-0.1-1.1
 h1.2c0.5,0,0.9-0.4,0.9-0.9v-0.5c0-0.5-0.4-0.9-0.9-0.9h-0.2v-13.1h3c-0.8,1.5-1.2,3-1.2,4.7c0,5.7,5.2,10.6,12.9,13.4
 c5,1.8,9.3,4.2,12.7,7.1c-0.1,0.2-0.2,0.5-0.2,0.8c0,0.7,0.4,1.4,1.1,1.7l0.5,7.2h0.5l0.5-7.2c0.6-0.3,1.1-0.9,1.1-1.7
 c0-0.3-0.1-0.6-0.2-0.8c3.3-2.9,7.6-5.3,12.7-7.1c3.5-1.2,6.3-2.9,8.5-4.9v3.3h-0.5v2.5h-0.9v3.2h1.4v24.8h-0.5v2.5h-0.9v3.2h1.5
 c0.1,2.7,0.2,5.5,0.6,8.1c0,0,3.3,21.8,4,21.8c0.6,0,4.1-21.6,4.1-21.6c0.4-2.7,0.6-5.6,0.7-8.4h1.5V173h-0.9v-2.5h-0.5v-24.8h1.4v0
 c4.1,11.9,15,20.5,28.1,21.3v1.1c0,0.4,0.3,0.7,0.7,0.7l0.6,0l0.5,11.9c-1.3-0.4-2.7-0.6-4.2-0.4c-2.4,0.4-4.3,1.8-5.6,3.6
 c1-0.6,2.2-1.1,3.5-1.2c4.1-0.5,8.1,2.1,9.2,6.1c0.8,2.6,0.2,5.3-1.2,7.3c2.9-1.8,4.5-5.2,3.9-8.8c-0.5-3-2.6-5.3-5.2-6.4l0.5-12
 h0.6c0.2,0,0.4-0.1,0.5-0.2c0.1-0.1,0.2-0.3,0.2-0.5v-1.2c13.1-0.9,23.9-9.8,27.8-21.8v25.5h-0.7v3.2h-1.2v4.1h1.9v31.8h-0.7v3.2
 h-1.2v4.1h1.9c0.1,3.5,0.3,7,0.8,10.4c0,0,4.3,28,5.1,28c0.8,0,5.2-27.6,5.2-27.6c0.5-3.5,0.7-7.1,0.8-10.8h2v-4.1h-1.2v-3.2h-0.7
 v-31.8h1.9v-4.1h-1.2v-3.2h-0.7v-12c8.4,16.5,23.6,28.2,41.5,30.3v2.4c0,1.2,0.6,2.1,1.3,2.1h2.5l1.8,18h0.8l1.8-18h2.5
 c0.7,0,1.3-1,1.3-2.1V189c13.9-1.7,26.3-9.2,35-20.2c2.3,21.5,14.7,39.3,47.5,52.5c21.4,8.6,28.6,21.3,30.9,27.3
 c-3.4,0.4-6,2.1-6,4.1c0,1.9,2.3,3.5,5.4,4l0.5,13.6c-1,0.3-1.7,0.9-1.7,1.6c0,0.7,0.8,1.3,1.9,1.7l0.5,13.3
 c-4.8-1-9.9-0.9-14.9,0.8c-7.5,2.5-13.2,7.9-16.4,14.4c2.7-2.3,5.8-4.1,9.3-5.3c14.8-5,30.8,3,35.8,17.7c2.5,7.3,1.8,14.9-1.3,21.3
 c8.5-7.3,12.2-19.2,8.4-30.5c-3.2-9.4-10.7-16-19.6-18.3l0.8-13.5c1.4-0.2,2.4-0.9,2.4-1.8c0-0.8-0.9-1.5-2.1-1.7l0.8-13.5
 c3.2-0.5,5.5-2.1,5.5-4c0-2.1-2.8-3.8-6.4-4.1c2.3-6,9.5-18.7,30.9-27.3c34.9-14,46.7-33.3,47.8-56.7c8.8,13.3,22.4,22.5,38.1,24.4
 v2.4c0,1.2,0.6,2.1,1.3,2.1h2.5l1.8,18h0.8l1.8-18h2.5c0.7,0,1.3-1,1.3-2.1V189c19.7-2.4,36.2-16.3,43.8-35.6v17.2h-0.7v3.2h-1.2
 v4.1h1.9v31.8h-0.7v3.2h-1.2v4.1h1.9c0.1,3.5,0.3,7,0.8,10.4c0,0,4.3,28,5.1,28c0.8,0,5.2-27.6,5.2-27.6c0.5-3.5,0.7-7.1,0.8-10.8h2
 v-4.1h-1.2v-3.2h-0.7v-31.8h1.9v-4.1h-1.2v-3.2h-0.7v-21.8c4.8,10.2,14.9,17.4,26.8,18.1v1.1c0,0.4,0.3,0.7,0.7,0.7l0.6,0l0.5,11.9
 c-1.3-0.4-2.7-0.6-4.2-0.4c-2.4,0.4-4.3,1.8-5.6,3.6c1-0.6,2.2-1.1,3.5-1.2c4.1-0.5,8.1,2.1,9.2,6.1c0.7,2.6,0.2,5.2-1.2,7.3
 c2.9-1.8,4.5-5.2,3.9-8.8c-0.5-3-2.6-5.3-5.2-6.4l0.5-12h0.6c0.2,0,0.4-0.1,0.5-0.2c0.1-0.1,0.2-0.3,0.2-0.5v-1.2
 c12.9-0.9,23.6-9.5,27.6-21.2h1.1v24.8h-0.5v2.5h-0.9v3.2h1.5c0.1,2.7,0.2,5.5,0.6,8.1c0,0,3.3,21.8,4,21.8c0.6,0,4.1-21.6,4.1-21.6
 c0.4-2.7,0.6-5.6,0.7-8.4h1.5V173h-0.9v-2.5h-0.5v-24.8h1.4v-3.2h-0.9v-2.5h-0.5v-2.9c2.1,1.8,4.9,3.4,8.1,4.5
 c5.1,1.8,9.3,4.2,12.7,7.1c-0.1,0.2-0.2,0.5-0.2,0.8c0,0.7,0.5,1.4,1.1,1.7l0.5,7.2h0.5l0.5-7.2c0.6-0.3,1.1-0.9,1.1-1.7
 c0-0.3-0.1-0.6-0.2-0.8c3.3-2.8,7.6-5.3,12.7-7.1c7.7-2.8,12.9-7.7,12.9-13.4c0-1.6-0.4-3.2-1.2-4.7h3v13.1h-0.2
 c-0.5,0-0.9,0.4-0.9,0.9v0.5c0,0.5,0.4,0.9,0.9,0.9h1.2c-0.1,0.3-0.1,0.7-0.1,1.1c0,2.5,1.3,4.7,3.3,5.9c1.2,0.8,2.3,1.7,3.1,2.9
 c-0.2,0.1-0.3,0.4-0.3,0.6c0,0.3,0.2,0.6,0.5,0.7l0.2,3.2h0.2l0.2-3.2c0.3-0.1,0.5-0.4,0.5-0.7c0-0.2-0.1-0.4-0.3-0.6
 c0.8-1.2,1.9-2.1,3.1-2.9c2-1.2,3.3-3.4,3.3-5.9c0-0.4,0-0.7-0.1-1.1h1.3c0.5,0,0.9-0.4,0.9-0.9v-0.5c0-0.5-0.4-0.9-0.9-0.9h-0.2
 V123c1,3.6,3.8,6.5,7.3,7.8c0,0.1,0,0.1,0,0.2c0,1.7,1.1,3.2,2.7,3.8l0.6,1.7c-0.1,0.2-0.2,0.3-0.2,0.5c0,0.4,0.2,0.7,0.6,0.9
 l0.4,5.4c0,0,0,0,0.1,0c0,0,0,0,0,0l0.4-5.4c0.3-0.2,0.6-0.5,0.6-0.9c0-0.2-0.1-0.4-0.2-0.5l0.6-1.7c1.5-0.6,2.7-2,2.7-3.8
 c0-0.1,0-0.1,0-0.2c4.5-1.6,7.8-6,7.8-11.1c0,0,0,0,0,0c0-2.1,1.3-3.9,3.3-4.3c17.8-3.7,95.5-19.6,211.1-39.8
 c15.4-2.4,30.3-4.7,44.7-6.7C3423.5-42.8,4001.1,65,4080,97.3V0L0,0l0,114.3C150.5,40.5,891.4-2.2,1412,60.3z M1723.1,130.7
 c0,1.8-0.5,3.4-1.3,3.9c-0.7-0.5-1.3-2-1.3-3.9v-7.2h2.6V130.7z M1730.6,131.5c0,2-1.4,3.7-3.2,4.3c-1.9-0.5-3.2-2.2-3.2-4.3v-8h4.9
 v0.1h1.6V131.5z M1734.3,130.7c0,1.8-0.5,3.4-1.3,3.9c-0.7-0.5-1.3-2-1.3-3.9v-7.1h2.6V130.7z M2348.3,130.7c0,1.8-0.5,3.4-1.3,3.9
 c-0.7-0.5-1.3-2-1.3-3.9v-7.1h2.6V130.7z M2355.8,131.5c0,2-1.4,3.7-3.2,4.3c-1.9-0.5-3.2-2.2-3.2-4.3v-7.9h1.6v-0.1h4.9V131.5z
 M2359.5,130.7c0,1.8-0.5,3.4-1.3,3.9c-0.7-0.5-1.3-2-1.3-3.9v-7.2h2.6V130.7z"></path> </svg></div>

</div>

<div class="container-fluid">

    <div class="text-center f-size-2-2 boxArrow">
        <i class="fa-solid fa-arrow-down"></i>
    </div>

    <div class="text-center">
        <div class="f-size-2-2 mt-4 text-weight-bolder monteserrat">
            O que é a Yoga da Vida Real?
        </div>
        <div class="f-size-1-1 text-weight-bold inter">
            A verdadeira Yoga que nunca te contaram antes…
        </div>
    </div>

    <div class="row mt-6 align-items-center centerOnMobile">
        <div class="col-md-1 col-xl-2"></div>
        <div class="col-md-5 col-xl-4">

            <div class="showElement mt-n15"></div>

            <div class="f-size-2-2 mt-4 text-weight-bold montserrat">
                Além do <span class="positionRelative">
                    Tapetinho
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 500 150" class="svgNuvem" preserveAspectRatio="none"><path class="pathNuvem" d="M3,146.1c17.1-8.8,33.5-17.8,51.4-17.8c15.6,0,17.1,18.1,30.2,18.1c22.9,0,36-18.6,53.9-18.6 c17.1,0,21.3,18.5,37.5,18.5c21.3,0,31.8-18.6,49-18.6c22.1,0,18.8,18.8,36.8,18.8c18.8,0,37.5-18.6,49-18.6c20.4,0,17.1,19,36.8,19 c22.9,0,36.8-20.6,54.7-18.6c17.7,1.4,7.1,19.5,33.5,18.8c17.1,0,47.2-6.5,61.1-15.6"></path></svg>
                </span>
            </div>

            <div class="mt-4 f-size-1-1 montserrat text-weight-medium">
                Chega de procurar informações com quem quer apenas te <br class="hideElement">ensinar “uma posição” ou um “truque” novo. Aqui, com o <br class="hideElement">Yoga da Vida Real, <span class="text-weight-bolder">a sua mudança vai para além do<br class="hideElement"> tapete, vai para a sua vida.</span>
            </div>

        </div>
        <div class="col-md-5 col-xl-4">
            <img src="./assets/yogaIniciantes/container1-1.webp" alt="" class="card-img imgRounded">
        </div>
        <div class="col-md-1 col-xl-2"></div>
    </div>

    <div class="row mt-6 align-items-center reverse centerOnMobile">
        <div class="col-md-1 col-xl-2"></div>
        <div class="col-md-5 col-xl-4">
            <img src="./assets/yogaIniciantes/container1-2.webp" alt="" class="card-img imgRounded">
        </div>
        <div class="col-md-5 col-xl-4">

            <div class="f-size-2-2 montserrat mt-4 text-weight-bold">
                Transformação <span class="positionRelative">
                    completa
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 500 150" class="svgNuvem" preserveAspectRatio="none"><path class="pathNuvem" d="M3,146.1c17.1-8.8,33.5-17.8,51.4-17.8c15.6,0,17.1,18.1,30.2,18.1c22.9,0,36-18.6,53.9-18.6 c17.1,0,21.3,18.5,37.5,18.5c21.3,0,31.8-18.6,49-18.6c22.1,0,18.8,18.8,36.8,18.8c18.8,0,37.5-18.6,49-18.6c20.4,0,17.1,19,36.8,19 c22.9,0,36.8-20.6,54.7-18.6c17.7,1.4,7.1,19.5,33.5,18.8c17.1,0,47.2-6.5,61.1-15.6"></path></svg>
                </span>
            </div>

            <div class="mt-4 f-size-1-1 montserrat text-weight-medium">
                Muita gente procura o Yoga para resolver apenas um <br class="hideElement">problema, mas o Yoga da vida real é mais que isso. Você <br class="hideElement">terá <span class="text-weight-bolder">uma transformação completa da sua realidade,</span> <br class="hideElement">com menos estresse, mais tranquilidade, e mais leveza.
            </div>

        </div>
        <div class="showElement mt-n15"></div>
        <div class="col-md-1 col-xl-2"></div>
    </div>

    <div class="row mt-6 align-items-center centerOnMobile">
        <div class="col-md-1 col-xl-2"></div>
        <div class="col-md-5 col-xl-4">
            <div class="showElement mt-n15"></div>

            <div class="f-size-2-2 montserrat mt-4 text-weight-bold">
                Adaptado a <span class="positionRelative">
                    sua realidade
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 500 150" class="svgNuvem" preserveAspectRatio="none"><path class="pathNuvem" d="M3,146.1c17.1-8.8,33.5-17.8,51.4-17.8c15.6,0,17.1,18.1,30.2,18.1c22.9,0,36-18.6,53.9-18.6 c17.1,0,21.3,18.5,37.5,18.5c21.3,0,31.8-18.6,49-18.6c22.1,0,18.8,18.8,36.8,18.8c18.8,0,37.5-18.6,49-18.6c20.4,0,17.1,19,36.8,19 c22.9,0,36.8-20.6,54.7-18.6c17.7,1.4,7.1,19.5,33.5,18.8c17.1,0,47.2-6.5,61.1-15.6"></path></svg>
                </span>
            </div>

            <div class="mt-4 f-size-1-1 montserrat text-weight-medium">

                Você <span class="text-weight-bolder">NÃO</span> precisa deixar as suas responsabilidades de lado <br class="hideElement">para realizar a prática. Com a Yoga da vida real, a sua rotina <br class="hideElement">é levada em consideração, e você pode encaixar a Yoga <span class="text-weight-bolder">em <br class="hideElement">quaisquer 15 minutos livres do seu dia-a-dia.</span>
            </div>

        </div>
        <div class="col-md-5 col-xl-4">
            <img src="./assets/yogaIniciantes/container1-3.webp" alt="" class="card-img imgRounded">
        </div>
        <div class="col-md-1 col-xl-2"></div>
    </div>
    
    <br><br><br>
</div>

<div class="container-fluid primeiroBackground pt-5 positionRelative">
    <div class="text-center text-color-white">
        <div class="f-size-2-2 text-weight-bolder montserrat">
            Conheça todos os benefícios que a Yoga pode somar na sua vida
        </div>
        <div class="f-size-1-1 text-weight-bold inter">
            Os resultados já podem aparecer nas suas primeiras ações de prática.
        </div>
    </div>
    <div class="col-xl-12 row mt-4">
        <div class="col-md-1"></div>
        <div class="col-md-5 text-white">
            <ul class="textUL">
                <li class="d-flex align-items-center">
                    <span><i aria-hidden="true" class="fas fa-check azulCiano f-size-1-5"></i></span>
                    <span><b>Alcançar uma vida mais feliz</b> e com menos problemas<br class="hideElement"> te atingindo o tempo inteiro;</span>
                </li>
                <li class="mt-4 d-flex align-items-center">
                    <span><i aria-hidden="true" class="fas fa-check azulCiano f-size-1-5"></i></span>
                    <span>Ter uma postura melhor te proporciona <b>menos dores<br class="hideElement"> no curto e no longo prazo;</b></span>
                </li>
                <li class="mt-4 d-flex align-items-center">
                    <span><i aria-hidden="true" class="fas fa-check azulCiano f-size-1-5"></i></span>
                    <span><b>Reduzir a insônia, ter uma mente saudável</b> e em paz<br class="hideElement"> consigo mesma;</span>
                </li>
                <li class="mt-4 d-flex align-items-center">
                    <span><i aria-hidden="true" class="fas fa-check azulCiano f-size-1-5"></i></span>
                    <span>Recuperar a sua <b>autoestima, autoconfiança</b> e<br class="hideElement"> segurança de uma mulher forte e empoderada;</span>
                </li>
                <li class="mt-4 d-flex align-items-center">
                    <span><i aria-hidden="true" class="fas fa-check azulCiano f-size-1-5"></i></span>
                    <span>Atingir um novo nível de felicidade com<br class="hideElement"> <b>relacionamentos harmoniosos e sem mais brigas<br class="hideElement"> desnecessárias;</b></span>
                </li>
                <li class="mt-4 d-flex align-items-center">
                    <span><i aria-hidden="true" class="fas fa-check azulCiano f-size-1-5"></i></span>
                    <span><b>Eliminar as energias negativas</b> e colocar grande parte<br class="hideElement"> da sua vida no eixo correto;</span>
                </li>
                <li class="mt-4 d-flex align-items-center">
                    <span><i aria-hidden="true" class="fas fa-check azulCiano f-size-1-5"></i></span>
                    <span>Um <b>estilo de vida mais saudável,</b class="hideElement"> calmo, revigorante e<br class="hideElement"> que te faz viver melhor;</span>
                </li>
                <li class="mt-4 d-flex align-items-center">
                    <span><i aria-hidden="true" class="fas fa-check azulCiano f-size-1-5"></i></span>
                    <span><b>Aumente o seu foco</b> para os pontos positivos da vida e<br class="hideElement"> tenha mais equilíbrio sentimental;</span>
                </li>
                <li class="mt-4 d-flex align-items-center">
                    <span><i aria-hidden="true" class="fas fa-check azulCiano f-size-1-5"></i></span>
                    <span>Resgate a sua força interior adormecida e <b>tenha mais<br class="hideElement"> consciência ativa</b> da sua transformação como pessoa;</span>
                </li>
                <li class="mt-4 d-flex align-items-center">
                    <span><i aria-hidden="true" class="fas fa-check azulCiano f-size-1-5"></i></span>
                    <span>Melhore a sua flexibilidade, todas as articulações e<br class="hideElement"> <b>nunca mais se sinta travada,</b> independente da sua<br class="hideElement"> idade;</span>
                </li>
            </ul>
        </div>
        <div class="col-md-5 text-center"><img class="imgContainerUl" src="./assets/yogaIniciantes/container-2.webp" alt=""></div>
        <div class="col-md-1"></div>
    </div>

    <div class="text-center mt-4 text-color-white">
        <div class="f-size-2-2 text-weight-bolder montserrat">
            Veja o que nossas alunas estão dizendo...
        </div>
        <div class="f-size-1-1 text-weight-bold inter">
            A melhor forma de começar na Yoga é aqui.
        </div>
        <div class="swiper mySwiper mt-4">
            <div class="swiper-wrapper">
                <div class="swiper-slide"><img src="./assets/yogaIniciantes/container-2-feedback-1.webp"></div>
                <div class="swiper-slide"><img src="./assets/yogaIniciantes/container-2-feedback-2.webp"></div>
                <div class="swiper-slide"><img src="./assets/yogaIniciantes/container-2-feedback-3.webp"></div>
                <div class="swiper-slide"><img src="./assets/yogaIniciantes/container-2-feedback-4.webp"></div>
                <div class="swiper-slide"><img src="./assets/yogaIniciantes/container-2-feedback-5.webp"></div>
                <div class="swiper-slide"><img src="./assets/yogaIniciantes/container-2-feedback-6.webp"></div>
            </div>
        </div>
        <div class="text-center text-color-white mt-7">
            <div class="f-size-2-2 text-weight-bolder montserrat">
                Como funciona o treinamento Yoga para Iniciantes?
            </div>
            <div class="f-size-1-1 text-weight-bold inter">
                Centenas de mulheres já estão vivendo sem ansiedade, sem estresse e sem insônia.
            </div>
        </div>

        <div class="row">
            <div class="col-md-1 col-xl-2"></div>
            <div class="col-md-10 col-xl-8">
                <?php require('timeline.php') ?>
            </div>
            <div class="col-md-1 col-xl-2"></div>
        </div>
    </div>
    <br><br><br>

    <div class="elementor-shape elementor-shape-top" data-negative="false"> <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1000 20" preserveAspectRatio="none"> <path class="svg02" d="M0,0v3c0,0,393.8,0,483.4,0c9.2,0,16.6,7.4,16.6,16.6c0-9.1,7.4-16.6,16.6-16.6C606.2,3,1000,3,1000,3V0H0z"></path> </svg></div>
</div>


<div class="container-fluid">
    <div class="row">
        <div class="text-center mt-4">
            <div class="f-size-2-2 text-weight-bolder montserrat mt-5">
                O treinamento Yoga para iniciantes por dentro
            </div>
            <div class="f-size-1-1 text-weight-bold inter">
                Conheça o passo-a-passo que te levará à sua nova vida.
            </div>
        </div>
    </div>

    <div class="row justify-content-center mt-4 sectionModule1">
        <div class="col-md-3 col-xl-3 d-flex flex-column align-items-center text-center moduloContainer lh-1 marginRight">
            <img src="./assets/yogaIniciantes/container-3-modulo-1.webp" class="moduloImg pt-4">
            <p class="montserrat moduloTitulo text-weight-medium mt-4 mb-2">MÓDULO 01</p>
            <p class="montserrat roxo f-size-1-7 text-weight-bolder  lh-30px">A Prática da Forma<br class="hideElement"> Certa</p>
            <p class="montserrat cinza mt-2 text-weight-medium txtModule">Aqui nós iremos te colocar no rumo certo<br> para o seu sucesso na Yoga. Você dará os<br> seus passos iniciais de uma forma simples e<br> fácil.</p>
            <p class="montserrat cinza text-weight-medium txtModule">Aqui você aprenderá a escolher a aura certa,<br> como a meditação funciona, como aliviar<br> sua postura, como posicionar as mãos,<br> como respirar melhor, entre várias outras<br> coisas.</p>
        </div>
        <div class="col-md-3 col-xl-3 d-flex flex-column align-items-center text-center moduloContainer lh-1 marginRight">
            <img src="./assets/yogaIniciantes/container-3-modulo-2.webp" class="moduloImg pt-4">
            <p class="montserrat moduloTitulo text-weight-medium mt-4 mb-2">MÓDULO 02</p>
            <p class="montserrat roxo f-size-1-7 text-weight-bolder lh-30px">Adaptações Para <br>Necessidades Específicas</p>
            <p class="montserrat cinza mt-2 text-weight-medium txtModule">A Yoga é para todos! E é aqui que você terá<br> toda a certeza disso. Muita gente deixa de<br> praticar Yoga por algum problema pessoal,<br> e aqui nós eliminaremos esses problemas.</p>
            <p class="montserrat cinza text-weight-medium txtModule">Você irá descobrir adaptações da Yoga para<br> inúmeros tipos de problemas, como Hérnia,<br> problemas no joelho, problemas de<br> equilíbrio, nos punhos, grávidas, momentos<br> de TPM etc.</p>
        </div>
        <div class="col-md-3 col-xl-3 d-flex flex-column align-items-center text-center moduloContainer lh-1">
            <img src="./assets/yogaIniciantes/container-3-modulo-3.webp" class="moduloImg pt-4">
            <p class="montserrat moduloTitulo text-weight-medium mt-4 mb-2">MÓDULO 03</p>
            <p class="montserrat roxo f-size-1-7 text-weight-bolder lh-30px">Evoluindo com Yoga</p>
            <p class="montserrat cinza text-weight-medium mt-2 txtModule">Agora que você já está começando a<br> realizar a prática da Yoga com mais<br> qualidade e constância, chegou a hora de<br> evoluir ainda mais o seu conhecimento na<br> área.</p>
            <p class="montserrat cinza text-weight-medium txtModule">Você aprenderá inúmeros outros conceitos<br> que podem te ajudar na sua vida rotineira,<br> como o conceito de Adho Mukha, posturas<br> do guerreiro, props, saudações, retroflexões,<br> e muito mais.</p>
        </div>
    </div>

    <div class="row justify-content-center mt-2 sectionModule2">
        <div class="col-md-3 col-xl-3 d-flex flex-column align-items-center text-center moduloContainer lh-1 marginRight2">
            <img src="./assets/yogaIniciantes/container-3-modulo-4.webp" class="moduloImg pt-4">
            <p class="montserrat moduloTitulo text-weight-medium mt-4 mb-2">MÓDULO 04</p>
            <p class="montserrat roxo f-size-1-7 text-weight-bolder lh-30px">Dominando a Energia <br>Ying</p>
            <p class="montserrat cinza text-weight-medium mt-2 txtModule">Aqui os resultados da Yoga já estarão se<br> apresentando muito fortes na sua vida, e você<br> poderá potencializá-los ainda mais!</p>
            <p class="montserrat cinza text-weight-medium txtModule">Você terá aulas completas e exclusivas sobre a<br> energia Ying, e como utilizá-la na Yoga para se<br> acalmar, relaxar, restaurar e se reconectar.</p>
        </div>
        <div class="col-md-3 col-xl-3 d-flex flex-column align-items-center text-center moduloContainer lh-1 marginLeft2">
            <img src="./assets/yogaIniciantes/container-3-modulo-5.webp" class="moduloImg pt-4">
            <p class="montserrat moduloTitulo text-weight-medium mt-4 mb-2">MÓDULO 05</p>
            <p class="montserrat roxo f-size-1-7 text-weight-bolder lh-30px">Dominando a Energia<br class="hideElement"> Yang</p>
            <p class="montserrat cinza text-weight-medium mt-2 txtModule">Assim como a energia Ying é importante, a<br> Yang também é. Se você deseja ter equilíbrio na sua vida, ambas precisam estar presentes.</p>
            <p class="montserrat cinza text-weight-medium txtModule">Você irá aprender técnicas e conceitos da Yoga para te levar energia, disposição e despertar físico na sua vida. Te trazendo uma vida mais alegre e boa de se viver.</p>
        </div>
    </div>
    

    <div class="row">
        <div class="text-center mt-7">
            <div class="f-size-2-2 text-weight-bolder montserrat">
                4 Presentes <span class="positionRelative">
                    especiais
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 500 150" class="svgNuvem" preserveAspectRatio="none"><path class="pathNuvem" d="M3,146.1c17.1-8.8,33.5-17.8,51.4-17.8c15.6,0,17.1,18.1,30.2,18.1c22.9,0,36-18.6,53.9-18.6 c17.1,0,21.3,18.5,37.5,18.5c21.3,0,31.8-18.6,49-18.6c22.1,0,18.8,18.8,36.8,18.8c18.8,0,37.5-18.6,49-18.6c20.4,0,17.1,19,36.8,19 c22.9,0,36.8-20.6,54.7-18.6c17.7,1.4,7.1,19.5,33.5,18.8c17.1,0,47.2-6.5,61.1-15.6"></path></svg>
                </span> liberados
            </div>
            <div class="f-size-1-1 text-weight-bold mt-3 inter">
                Os presentes abaixo são limitados apenas para quem garantir a vaga nessa turma.
            </div>
        </div>
    </div>
    <br><br>
</div>

<div class="container-fluid backgroundPresentes">
    <div class="row centerOnMobile">
        <div class="col-md-1 col-xl-2"></div>
        <div class="col-md-3 col-xl-2 mt-5">
            <img src="./assets/yogaIniciantes/container-4-bonus-1.webp" alt="" class="card-img livrosBanners">
        </div>
        <div class="col-md-8 mt-6">
            <button class="btn btn-sm buttonBonus f-size-0-7 text-color-white montserrat">BÔNUS 1</button>
            <div class="f-size-2-2 text-weight-bolder montserrat">
                Guia De Yoga
            </div>
            <div class="f-size-1-3 mt-2 text-weight-medium lh-25px">
                Este livro será <span class="text-weight-bold">a sua base de conhecimento da Yoga.</span> Você aprenderá sobre os mitos da <br class="hideElement">prática, seus benefícios, história, principais linhas, mudras, bandhas, como escolher seu <br class="hideElement">tapete e acessórios, e muito mais.
            </div>
            <div class="f-size-1-9 mt-5 text-weight-medium montserrat">
                De <span class="text-red positionRelative">
                    R$ 97,00
                    <svg xmlns="http://www.w3.org/2000/svg" class="svgRedStroke" viewBox="0 0 500 150" preserveAspectRatio="none"><path class="pathRedStroke" d="M13.5,15.5c131,13.7,289.3,55.5,475,125.5"></path></svg>
                </span> Hoje sai de GRAÇA para você.
            </div>
        </div>
    </div>
    <br><br><br>
</div>

<div class="container-fluid">
    <div class="row centerOnMobile">
        <div class="col-md-1 col-xl-2"></div>
        <div class="col-md-3 col-xl-2 mt-5">
            <img src="./assets/yogaIniciantes/container-4-bonus-2.webp" alt="" class="card-img livrosBanners">
        </div>
        <div class="col-md-8 mt-6">
            <button class="btn btn-sm buttonBonus f-size-0-7 text-color-white montserrat">BÔNUS 2</button>
            <div class="f-size-2-2 text-weight-bolder montserrat">
                Jornada do Zero a Meditação
            </div>
            <div class="f-size-1-3 mt-2 text-weight-medium lh-25px">
                Com esse bônus você terá <span class="text-weight-bold">o passo-a-passo completo para começar a meditar do zero</span>, ou <br class="hideElement">seja, mesmo que você não tenha nenhuma experiência, assim você consegue começar a <br class="hideElement">meditar o mais rápido possível
            </div>
            <div class="f-size-1-9 mt-5 text-weight-medium montserrat">
                De  <span class="text-red positionRelative">
                    R$ 297,00
                    <svg xmlns="http://www.w3.org/2000/svg" class="svgRedStroke" viewBox="0 0 500 150" preserveAspectRatio="none"><path class="pathRedStroke" d="M13.5,15.5c131,13.7,289.3,55.5,475,125.5"></path></svg>
                </span> Hoje sai de GRAÇA para você.
            </div>
        </div>
    </div>
    <br><br><br>
</div>

<div class="container-fluid backgroundPresentes">
    <div class="row centerOnMobile">
        <div class="col-md-1 col-xl-2"></div>
        <div class="col-md-3 col-xl-2 mt-5">
            <img src="./assets/yogaIniciantes/container-4-bonus-3.webp" alt="" class="card-img livrosBanners">
        </div>
        <div class="col-md-8 mt-6">
            <button class="btn btn-sm buttonBonus f-size-0-7 text-color-white montserrat">BÔNUS 3</button>
            <div class="f-size-2-2 text-weight-bolder montserrat">
                As 25 Chaves da Meditação
            </div>
            <div class="f-size-1-3 mt-2 text-weight-medium lh-25px">
                Aqui <span class="text-weight-bold montserrat">você irá descobrir 25 pontos essenciais para se meditar</span> com o máximo de qualidade. <br>Com esse bônus, você poderá atingir a paz interior que a prática te traz da forma mais fácil <br class="hideElement">possível, sem dificuldade.
            </div>
            <div class="f-size-1-9 mt-5 text-weight-medium montserrat">
                De  <span class="text-red positionRelative">
                    R$ 69,00
                    <svg xmlns="http://www.w3.org/2000/svg" class="svgRedStroke" viewBox="0 0 500 150" preserveAspectRatio="none"><path class="pathRedStroke" d="M13.5,15.5c131,13.7,289.3,55.5,475,125.5"></path></svg>
                </span> Hoje sai de GRAÇA para você.
            </div>
        </div>
    </div>
    <br><br><br>
</div>

<div class="container-fluid">
    <div class="row centerOnMobile">
        <div class="col-md-1 col-xl-2"></div>
        <div class="col-md-3 col-xl-2 mt-5">
            <img src="./assets/yogaIniciantes/container-4-bonus-2.webp" alt="" class="card-img livrosBanners">
        </div>
        <div class="col-md-8 mt-6">
            <button class="btn btn-sm buttonBonus f-size-0-7 text-color-white montserrat">BÔNUS 4</button>
            <div class="f-size-2-2 text-weight-bolder montserrat">
                45 Práticas Guiadas em Áudio
            </div>
            <div class="f-size-1-3 mt-2 text-weight-medium lh-25px">
                Meditar, se alongar, e praticar Yoga no geral pode parecer difícil para quem nunca passou <br class="hideElement">pela experiência. Por isso, <span class="text-weight-bold">você terá 45 práticas guiadas para começar a Yoga</span> agora mesmo <br class="hideElement">com a minha ajuda.
            </div>
            <div class="f-size-1-9 mt-5 text-weight-medium montserrat">
                De  <span class="text-red positionRelative">
                    R$ 147,00
                    <svg xmlns="http://www.w3.org/2000/svg" class="svgRedStroke" viewBox="0 0 500 150" preserveAspectRatio="none"><path class="pathRedStroke" d="M13.5,15.5c131,13.7,289.3,55.5,475,125.5"></path></svg>
                </span> Hoje sai de GRAÇA para você.
            </div>
        </div>
    </div>
    <br><br><br>
</div>


<div class="container-fluid primeiroBackground">
    <div class="row text-center">
        <div class="col-xl-12 mt-5 mb-5">
            <a href="#cardOferta" class="btn btn-lg cianoButton cianShinyBorderButtons mt-4 f-size-1-2 text-weight-bold montserrat">SIM! QUERO DOMINAR A PRÁTICA DA YOGA</a>
            <div class="text-weight-bold mt-2 f-size-1-1 inter text-color-white">
                Aperte no botão acima para garantir a sua vaga
            </div>
        </div>
    </div>
    <br>
</div>

<div class="container-fluid">
    <div class="text-center mt-7">
        <div class="f-size-2-2 text-weight-bolder montserrat">
            Faça como elas e inicie imediatamente a sua transformação
        </div>
        <div class="f-size-1-1 text-weight-bold inter">
            Coloque a Yoga em prática AGORA e comece esta nova página da sua vida.
        </div>
    </div>
    <div class="row">
    <div class="swiper mySwiper mt-4">
            <div class="swiper-wrapper">
                <div class="swiper-slide"><img src="./assets/yogaIniciantes/container-5-feedback-1.webp"></div>
                <div class="swiper-slide"><img src="./assets/yogaIniciantes/container-5-feedback-2.webp"></div>
                <div class="swiper-slide"><img src="./assets/yogaIniciantes/container-5-feedback-3.webp"></div>
                <div class="swiper-slide"><img src="./assets/yogaIniciantes/container-5-feedback-4.webp"></div>
                <div class="swiper-slide"><img src="./assets/yogaIniciantes/container-5-feedback-5.webp"></div>
            </div>
        </div>
    </div>

    <br><br>
</div>

<div class="container-fluid primeiroBackground">
    <div class="row text-center">
        <div class="col-md-3"></div>
        <div class="col-md-6 mt-5">
            <div class="card cianShinyBorderCard" id="cardOferta">
                <div class="card-body">
                    <div class="row text-center">
                        <div class="col-xl-1"></div>
                        <div class="col-xl-10">
                            <div class="f-size-2 text-weight-bolder montserrat lh-30px">
                                Oferta especial por<br> tempo limitado!
                            </div>
                            <div class="f-size-1-1 text-weight-medium inter mt-2">
                                Aproveite essa chance especial enquanto pode, pois o valor pode aumentar.
                            </div>
                            <img src="./assets/yogaIniciantes/imgOferta.webp" alt="" class="card-img mt-3 mb-2">

                            <!-- ROWS BONUS -->
                            <div class="row">
                                <div class="col-9 col-md-10 mt-4">
                                    <div class="f-size-0-9 text-weight-bolder d-flex justify-items-start montserrat smallOferta">
                                        Treinamento
                                    </div>
                                    <div class="f-size-1-2 d-flex justify-items-start montserrat textOferta text-weight-medium">
                                        Yoga Para Iniciantes &nbsp;<span class="dotSpan"></span>
                                    </div>
                                </div>
                                <div class="col-3 col-md-2 d-flex align-items-end justify-items-end f-size-1-4 text-weight-medium montserrat">
                                <span class="text-red ms-3 priceOferta"><s>R$ 497</s></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-9 col-md-10 mt-4">
                                    <div class="f-size-0-9 text-weight-bolder d-flex justify-items-start montserrat smallOferta">
                                        BÔNUS 1
                                    </div>
                                    <div class="f-size-1-2 d-flex justify-items-start montserrat textOferta text-weight-medium">
                                        Guia de Yoga &nbsp;<span class="dotSpan"></span>
                                    </div>
                                </div>
                                <div class="col-3 col-md-2 d-flex align-items-end justify-items-end f-size-1-4 text-weight-medium montserrat">
                                <span class="text-red ms-3 priceOferta"><s>R$ 97</s></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-9 col-md-10 mt-4">
                                    <div class="f-size-0-9 text-weight-bolder d-flex justify-items-start montserrat smallOferta">
                                        BÔNUS 2
                                    </div>
                                    <div class="f-size-1-2 d-flex justify-items-start montserrat textOferta text-weight-medium">
                                        Jornada do Zero à meditação &nbsp;<span class="dotSpan"></span>
                                    </div>
                                </div>
                                <div class="col-3 col-md-2 d-flex align-items-end justify-items-end f-size-1-4 text-weight-medium montserrat">
                                <span class="text-red ms-3 priceOferta"><s>R$ 297</s></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-9 col-md-10 mt-4">
                                    <div class="f-size-0-9 text-weight-bolder d-flex justify-items-start montserrat smallOferta">
                                        BÔNUS 3
                                    </div>
                                    <div class="f-size-1-2 d-flex justify-items-start montserrat textOferta text-weight-medium">
                                        25 Passos para facilitar a meditação &nbsp;<span class="dotSpan"></span>
                                    </div>
                                </div>
                                <div class="col-3 col-md-2 d-flex align-items-end justify-items-end f-size-1-4 text-weight-medium montserrat">
                                <span class="text-red ms-3 priceOferta"><s>R$ 69</s></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-9 col-md-10 mt-4">
                                    <div class="f-size-0-9 text-weight-bolder d-flex justify-items-start montserrat smallOferta">
                                        BÔNUS 4
                                    </div>
                                    <div class="f-size-1-2 d-flex justify-items-start montserrat textOferta text-weight-medium">
                                        45 Meditações guiadas em áudio &nbsp;<span class="dotSpan"></span>
                                    </div>
                                </div>
                                <div class="col-3 col-md-2 d-flex align-items-end justify-items-start f-size-1-4 text-weight-medium montserrat">
                                <span class="text-red ms-3 priceOferta"><s>R$ 147</s></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-7 col-md-9 mt-4">
                                    <div class="f-size-1-2 d-flex justify-items-start montserrat textOferta text-weight-medium">
                                        Suporte ativo &nbsp;<span class="dotSpan"></span>
                                    </div>
                                </div>
                                <div class="col-5 col-md-3 d-flex align-items-end justify-items-start f-size-1-2 text-weight-medium montserrat">
                                    <span class="text-red ms-3 textIncalculavel"><s>INVALCULÁVEL</s></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-7 col-md-9 mt-4">
                                    <div class="f-size-1-2 d-flex justify-items-start montserrat textOferta text-weight-medium">
                                        7 dias de garantia &nbsp;<span class="dotSpan"></span>
                                    </div>
                                </div>
                                <div class="col-4 col-md-3 d-flex align-items-end justify-items-end f-size-1-2 text-weight-medium montserrat wordWrap">
                                    <span class="text-red ms-3 textIncalculavel"><s>INVALCULÁVEL</s></span>
                                </div>
                            </div>
                            <!-- END ROW BONUS -->

                            <div class="text-center mt-4 f-size-1-4 text-weight-medium">
                                
                                <span class="text-weight-bolder">R$ 497,00</span> <i>(Treinamento Yoga para Iniciantes)</i> <br class="hideElement"> + <span class="text-weight-bolder">R$ 610,00</span> <i>(Bônus e Conteúdos Especiais)</i>
                                
                            </div>

                            <div class="text-center mt-4 f-size-1-9 text-weight-bold">
                                <i>
                                    de: <span class="text-red"><s>R$ 1.107,00</s></span> por apenas:
                                </i>
                            </div>

                            <div class="text-center mt-2 text-weight-bolder f-size-6">
                                <span class="parcelamentoOferta">
                                    12x de 
                                </span>
                                <span class="">R$ 9,70</span>
                            </div>
                            
                            <div class="text-center text-weight-medium f-size-1-3">
                                ou R$97,00 à vista
                            </div>

                            <a href="https://sun.eduzz.com/657781" class="btn btn-lg mt-4 f-size-1-2 text-weight-bold cianoButton cianShinyBorderButtons montserrat">SIM! QUERO ENTRAR PARA O TREINAMENTO</a>
                            <div class="f-size-1-2 mt-3 text-weight-bold inter">
                                Aperte no botão acima para fazer a sua inscrição agora.
                            </div>
                            <img src="./assets/yogaIniciantes/compraSegura.webp" alt="" class="card-img mt-3 mb-2">
                            <div class="f-size-0-8 mt-3 purpleText montserrat">
                                *As vagas para o Treinamento podem esgotar e o valor de entrada pode aumentar a qualquer momento.*
                            </div>
                        </div>
                        <div class="col-xl-1"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3"></div>
    </div>
    <br><br><br><br>

    <div class="elementor-shape elementor-shape-top" data-negative="false"> <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1000 20" preserveAspectRatio="none"> <path class="svg02" d="M0,0v3c0,0,393.8,0,483.4,0c9.2,0,16.6,7.4,16.6,16.6c0-9.1,7.4-16.6,16.6-16.6C606.2,3,1000,3,1000,3V0H0z"></path> </svg></div>
</div>

<div class="container-fluid">
    <div class="row justify-content-center reverse centerOnMobile">
        <div class="col-md-1 col-xl-2"></div>
        <div class="col-md-6 col-xl-5 mt-10">
            <div class="showElement mt-n15"></div>
            <div class="f-size-2-2 text-weight-bolder montserrat">
                Grátis por 7 dias
            </div>
            <div class="f-size-1-1 text-weight-bold inter">
                Você pode ver como tudo funciona sem pagar nenhum centavo por isso.
            </div>
            <div class="f-size-1-3 mt-3 montserrat lh-25px">
                Sua completa satisfação ou o seu dinheiro de volta. O risco é TODO nosso e você fica livre de qualquer peso na consciência ou desculpa que te impeça de agir.
                <br><br>
                Eu sei o quanto a sua segurança financeira é importante, por isso, você poderá assistir e provar todo o conteúdo previamente disponível do treinamento Yoga Para Iniciantes por 7 dias.
                <br><br>
                Se em até 7 dias, você acreditar que aquilo que você viu não é para você (o que é muito difícil) nós devolveremos todo o seu dinheiro. Você literalmente não tem nada a perder.
            </div>

            <a href="https://sun.eduzz.com/657781" class="btn btn-lg cianoButton cianShinyBorderButtons mt-4 text-weight-bold montserrat f-size-1-2">REALIZAR A INSCRIÇÃO COM 7 DIAS DE TESTE GRÁTIS!</a>
            
        </div>
        <div class="col-md-5 col-xl-4 mt-10">
            <img src="./assets/yogaIniciantes/seloGarantia.webp" alt="" class="card-img selo">
        </div>
        <div class="col-xl-1"></div>
    </div>
    <br><br><br>
</div>

<div class="container-fluid primeiroBackground">
    <br><br><br>
    <div class="text-center text-color-white">
        <div class="f-size-2-2 text-weight-bolder lh-31px montserrat">
            Não tem costume de comprar online?<br>Siga esse passo-a-passo simples.
        </div>
        <div class="f-size-1-1 text-weight-bold inter mt-2">
            Para fazer parte do treinamento <u>Yoga Para Iniciantes</u>, basta os seguintes passos.
        </div>
    </div>

    <div class="row text-color-white justify-content-center align-items-start ">
        <div class="containerIconesIniciais col-md-3 col-xl-3 mt-6 d-flex justify-content-start">
            <div>
                <div class="">
                    <img src="./assets/yogaIniciantes/targetIcon.webp" alt="" class="card-img iconesPassos">
                </div>
            </div>
            <div class="ms-2">
                <span class="azulCiano montserrat f-size-1-7 text-weight-bolder">1º passo</span>
                <p class="montserrat"><b>Aperte no botão que está logo abaixo</b><br> para ir até à página de pagamento que é<br> totalmente segura e completamente<br> privada.</p>
            </div>
        </div>

        <div class="containerIconesIniciais col-md-3 col-xl-3 mt-6 d-flex justify-content-start">
            <div>
                <div class="">
                    <img src="./assets/yogaIniciantes/listIcon.webp" alt="" class="card-img iconesPassos">
                </div>
            </div>
            <div class="ms-2">
                <span class="azulCiano montserrat f-size-1-7 text-weight-bolder">2º passo</span>
                <p class="montserrat">Na página de pagamento, <b>preencha as<br> suas informações</b> completamente e<br> aperte no botão verde para finalizar sua<br> inscrição.</p>
            </div>
        </div>

        <div class="containerIconesIniciais col-md-3 col-xl-3 mt-6 d-flex justify-content-start">
            <div>
                <div class="">
                    <img src="./assets/yogaIniciantes/envelopeIcon.webp" alt="" class="card-img iconesPassos">
                </div>
            </div>
            <div class="ms-2">
                <span class="azulCiano montserrat f-size-1-7 text-weight-bolder">3º passo</span>
                <p class="montserrat">Logo após isso, <b>você receberá um e-<br>mail nosso com o acesso</b> à sua área de<br> membros e as instruções para ter 100%<br> de satisfação dentro do treinamento<br> Yoga Para Iniciantes.</p>
            </div>
        </div>
    </div>

    <div class="text-center mt-4">
        <a href="#cardOferta" class="btn btn-lg cianoButton cianShinyBorderButtons mt-4 text-weight-bold montserrat f-size-1-2">SIGA O #1 PASSO E APERTE AQUI AGORA</a>
        <div class="f-size-1-1 inter mt-2 text-color-white text-weight-bold">
            Aperte no botão acima para realizar a sua matrícula.
        </div>
    </div>

    <div class="row mt-6 justify-content-center align-items-center reverse centerOnMobile">
        <div class="col-md-1"></div>
        <div class="col-md-5 col-xl-7 text-color-white">
            <div class="f-size-2-2 text-weight-bolder montserrat">
                Conheça a sua mentora, Bianca Vita
            </div>
            <div class="f-size-1-1 inter">
                Mais de 20 anos de experiência em Yoga.
            </div>
            <div class="mt-4 f-size-1-2 montserrat text-weight-medium lh-25px">
                <span class="text-weight-bolder">“Comecei na Yoga em 2001</span>, e desde 2004 me tornei professora de Yoga, onde até hoje repasso o meu conhecimento para outras praticantes. Sejam elas iniciantes ou até mesmo avançadas.
                <br><br>
                Atualmente <span class="text-weight-bolder">com mais de 50 cursos e especializações</span>, busco compartilhar a Yoga através da Internet com o máximo de pessoas possível, assim todas que aprendem comigo podem ter em suas vidas todos os benefícios que a Yoga proporciona.
                <br><br>
                Independente do que você queira, se é paz espiritual, tranquilidade ou eliminação das dores, <span class="text-weight-bolder">a Yoga pode te ajudar</span>, <u>e eu quero ser a sua guia</u> para te fazer atingir esse resultado. Confie em mim, eu te digo que a transformação é certa. Namastê!”
                <br><br><br>
                <span class="ephesis f-size-6">Bianca Vita</span>
            </div>
        </div>
        <div class="col-md-6 col-xl-3 imgAboutBianca">
        </div>
        <div class="col-xl-1"></div>
    </div>
    <br><br>
</div>

<div class="container-fluid">
    <div class="text-center">
        <div class="text-weight-bolder mt-4 f-size-2-2 montserrat">
            Perguntas frequentes
        </div>
        <div class="text-weight-bold f-size-1-1 inter">
            Ainda com dúvidas? Aqui estão as respostas.
        </div>

        <div class="row">
            <div class="col-md-2 col-xl-3"></div>
            <div class="col-md-8 col-xl-6 mt-4 mb-2">
                <?php require('accordion.php') ?>
            </div>
            <div class="col-md-2 col-xl-3"></div>
        </div>

        <a href="#cardOferta" class="btn btn-lg cianoButton cianShinyBorderButtons mt-4 text-weight-bold f-size-1-2 montserrat">GARANTIR MINHA VAGA AGORA</a>
        <div class="text-weight-bold mt-2 f-size-1-1">
            Aperte no botão acima para garantir a sua vaga
        </div>

    </div>
    <br><br><br>
</div>