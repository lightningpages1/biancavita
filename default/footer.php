<!-- Footer -->
<footer class="text-center text-lg-start bg-light text-muted">
  <!-- Copyright -->
  <div class="p-4 text-color-white" style="background-color: #080808;">

    <?php if ($dadosFooter['hasText']) : ?>
      <div class="row mb-5">
        <div class="col-md-1 resize-bs-col-md-1-upper"></div>
        <div class="col-md-4 resize-bs-col-md-4-lower laranja-libertar2 f-size-1-2 lh-1">
        "EM HOMAGEM às dores que eu entendo
        e que TODOS NÓS um dia sentimos,
        nasceu o LIBERTAR. Em repúdio eficaz
        à toda crença que já nos impediu de sentir prazer e à todo medo que já consumiu
        nossos corações. Com um milhão de
        retóricas intensas e inenarrável gratidão
        aos que se dispõem à REALMENTE
        se conhecer e amar, apresento o
        melhor caminho à quem anseia dizer:
        ainda bem que eu pude me LIBERTAR." <br><br>
        <div class="text-color-white text-weight-bold neon-behavior-white-light">
          (João Vitor Heringer, o Padrinho.) 
        </div>
        </div>
        <div class="col-md-7"></div>
      </div>
    <?php endif; ?>

    <div class="row text-center">
      <?php if (!$dadosFooter['isFooterImage']) : ?>
        <h1 class="<?= $dadosFooter['footerClass'] ?>"><?= $dadosFooter['footer'] ?></h1>
      <?php else : ?>
        <div class="row">
          <div class="<?= $dadosFooter['colsFooter']['divider'] ?>"></div>
          <div class="<?= $dadosFooter['colsFooter']['middle'] ?>">
            <img class="card-img logo" src="<?= $dadosFooter['footer'] ?>" style="width: 100%; height: 100%">
          </div>
          <div class="<?= $dadosFooter['colsFooter']['divider'] ?>"></div>
        </div>
      <?php endif; ?>
    </div>
    <p class="f-size-0-8 mt-3 text-center">
      Copyright ©️ Excessus Life Change 2022-Present. All Rights Reserved.
    </p>

  </div>
  <!-- Copyright -->
</footer>
<!-- Footer -->