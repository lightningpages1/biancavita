<!-- Footer -->
<footer class="text-center text-lg-start bg-light text-muted">
  <!-- Copyright -->
  <div class="p-3 text-color-white f-size-0-8 text-center" style="background-color: #331264;">
    <div>
      AHAVAH EMPREENDIMENTOS DIGITAIS EIRELI – CNPJ 30.337.874/0001-08 | Todos os direitos reservados.
    </div>
    <div class="row mt-2 text-center">
      <div class="col-md-12">
        <a href="https://biancavita.com/politica-de-privacidade-yoga-para-iniciantes/" target="__blank" class="buttonsFooter">Política de privacidade</a>

        <a href="https://biancavita.com/termos-de-uso-yoga-para-iniciantes/" target="__blank" class="ms-3 buttonsFooter">Termos de uso</a>
      </div>
    </div>
  </div>
  <!-- Copyright -->
</footer>
<!-- Footer -->