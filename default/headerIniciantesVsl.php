<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="initial-scale=1" />

<link rel="icon" type="image/x-icon" href="assets/favicon.ico">

<!-- LITE VIMEO -->
<script type="module" src="./vendor/lite-vimeo/lite-vimeo.js"></script>

<!-- BOOTSTRAP 5 -->
<!-- <link href="./vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<script src="./vendor/bootstrap/js/bootstrap.min.js"></script> -->

<!-- FONT AWESOME -->
<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<link rel="stylesheet" href="./vendor/fontawesome/css/all.min.css"/> -->

<!-- OUR CSS -->
<!-- <link href="./css/general.css" rel="stylesheet"> -->