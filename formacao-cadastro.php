<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <?php require('default/header.php'); ?>
    <title>Formação Viver de Yoga</title>
    <link rel="stylesheet" href="css/formacaoCadastro/formacaoCadastro.css">

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-WZHV2GP');</script>
    <!-- End Google Tag Manager -->
</head>

<body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WZHV2GP"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <?php require('contents/formacaoCadastro_content.php'); ?>
    <?php require('default/scriptsBody.php'); ?>
</body>

</html>