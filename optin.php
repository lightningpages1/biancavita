<?php $todaysDate  = date("Y-m-d"); ?>

<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <title>Lançamento | Yoga Business | Optin – Bianca Vita</title>  

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="initial-scale=1" />

    <link rel="icon" type="image/x-icon" href="assets/favicon.ico">
    <link rel="stylesheet" href="css/optin/cssUsed.css">

</head>

<body>
    <?php require('contents/optin_content.php') ?>

    <script>
        function padTo2Digits(num) {
            return num.toString().padStart(2, '0');
        }
        function formatDate(date) {
            return (
                [
                date.getFullYear(),
                padTo2Digits(date.getMonth() + 1),
                padTo2Digits(date.getDate()),
                ].join('-') +
                ' ' +
                [
                padTo2Digits(date.getHours()),
                padTo2Digits(date.getMinutes()),
                // padTo2Digits(date.getSeconds()),
                ].join(':')
            );
        }
        function getDate() {
            var todayDate = formatDate(new Date());
            document.getElementById('field[30][time]').value = todayDate
        }
        getDate()
    </script>

    <script type="text/javascript">
        window.cfields = {"31":"utm_source","32":"utm_campaign","33":"utm_medium","34":"utm_term","35":"utm_content","30":"data_de_inscrio"};

        window._show_thank_you = function(id, message, trackcmp_url, email) {
            document.querySelector('#_form_41_submit').disabled = false;
            window.location.href = "https://biancavita.com/parabens-yogabusiness/"
            // var form = document.getElementById('_form_' + id + '_'), thank_you = form.querySelector('._form-thank-you');
            // form.querySelector('._form-content').style.display = 'none';
            // thank_you.innerHTML = message;
            // thank_you.style.display = 'block';
            // const vgoAlias = typeof visitorGlobalObjectAlias === 'undefined' ? 'vgo' : visitorGlobalObjectAlias;
            // var visitorObject = window[vgoAlias];
            // if (email && typeof visitorObject !== 'undefined') {
            //     visitorObject('setEmail', email);
            //     visitorObject('update');
            // } else if (typeof(trackcmp_url) != 'undefined' && trackcmp_url) {
            //     // Site tracking URL to use after inline form submission.
            //     _load_script(trackcmp_url);
            // }
            // if (typeof window._form_callback !== 'undefined') window._form_callback(id);
        };

        window._show_error = function(id, message, html) {
            console.log(message)
        };

        window._load_script = function(url, callback) {
            var head = document.querySelector('head'), script = document.createElement('script'), r = false;
            script.type = 'text/javascript';
            script.charset = 'utf-8';
            script.src = url;
            if (callback) {
                script.onload = script.onreadystatechange = function() {
                if (!r && (!this.readyState || this.readyState == 'complete')) {
                    r = true;
                    callback();
                }
                };
            }
            head.appendChild(script);
        };

        (function() {

            if (window.location.search.search("excludeform") !== -1) return false;
            var getCookie = function(name) {
                var match = document.cookie.match(new RegExp('(^|; )' + name + '=([^;]+)'));
                return match ? match[2] : null;
            }
            var setCookie = function(name, value) {
                var now = new Date();
                var time = now.getTime();
                var expireTime = time + 1000 * 60 * 60 * 24 * 365;
                now.setTime(expireTime);
                document.cookie = name + '=' + value + '; expires=' + now + ';path=/';
            }
            
            var addEvent = function(element, event, func) {
                if (element.addEventListener) {
                element.addEventListener(event, func);
                } else {
                var oldFunc = element['on' + event];
                element['on' + event] = function() {
                    oldFunc.apply(this, arguments);
                    func.apply(this, arguments);
                };
                }
            }

            var form_to_submit = document.getElementById('_form_41_');
            var allInputs = form_to_submit.querySelectorAll('input, select, textarea'), tooltips = [], submitted = false;

            var getUrlParam = function(name) {
                var params = new URLSearchParams(window.location.search);
                return params.get(name) || false;
            };

            for (var i = 0; i < allInputs.length; i++) {
                var regexStr = "field\\[(\\d+)\\]";
                var results = new RegExp(regexStr).exec(allInputs[i].name);
                var fieldVal

                if (results != undefined) {
                    fieldVal = getUrlParam(window.cfields[results[1]]);
                } else {
                    fieldVal = false
                }

                if (fieldVal) {
                    allInputs[i].value = fieldVal;
                }
            }
        
            var _form_serialize = function(form){if(!form||form.nodeName!=="FORM"){return }var i,j,q=[];for(i=0;i<form.elements.length;i++){if(form.elements[i].name===""){continue}switch(form.elements[i].nodeName){case"INPUT":switch(form.elements[i].type){case"text":case"number":case"date":case"time":case"hidden":case"password":case"button":case"reset":case"submit":q.push(form.elements[i].name+"="+encodeURIComponent(form.elements[i].value));break;case"checkbox":case"radio":if(form.elements[i].checked){q.push(form.elements[i].name+"="+encodeURIComponent(form.elements[i].value))}break;case"file":break}break;case"TEXTAREA":q.push(form.elements[i].name+"="+encodeURIComponent(form.elements[i].value));break;case"SELECT":switch(form.elements[i].type){case"select-one":q.push(form.elements[i].name+"="+encodeURIComponent(form.elements[i].value));break;case"select-multiple":for(j=0;j<form.elements[i].options.length;j++){if(form.elements[i].options[j].selected){q.push(form.elements[i].name+"="+encodeURIComponent(form.elements[i].options[j].value))}}break}break;case"BUTTON":switch(form.elements[i].type){case"reset":case"submit":case"button":q.push(form.elements[i].name+"="+encodeURIComponent(form.elements[i].value));break}break}}return q.join("&")};

            var form_submit = function(e) {
                e.preventDefault();
                document.querySelector('#_form_41_submit').disabled = true;
                var serialized = _form_serialize(document.getElementById('_form_41_')).replace(/%0A/g, '\\n');
                // console.log(serialized)
                // var err = form_to_submit.querySelector('._form_error');
                // err ? err.parentNode.removeChild(err) : false;
                _load_script('https://mabarros.activehosted.com/proc.php?' + serialized + '&jsonp=true');
                // _load_script('https://dicasdopadrinho.activehosted.com/proc.php?' + serialized + '&jsonp=true');
                return false;
            };
            
            addEvent(form_to_submit, 'submit', form_submit);
        })();

    </script>

</body>

</html>