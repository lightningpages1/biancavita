<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <?php require('default/headerIniciantesVsl.php'); ?>
    <title>Yoga para Iniciantes VSL</title>  
    <link rel="stylesheet" href="css/iniciantesVsl/cssPage.css">
</head>

<body>
    <?php require('contents/iniciantesVsl.php'); ?>


    <script>
        const observer = new IntersectionObserver(entries => {
            entries.forEach(entry => {
                if (entry.isIntersecting) {
                    if (entry.target.classList.contains('set-transition-down-up')) {
                        entry.target.classList.add('transition-down-up');
                    }
                }
            });
        });
        
        // OBERVERS TRANSITIONS
        observer.observe(document.querySelector('#setTransition'));

    </script>
</body>

<?php require('default/footer_v2.php'); ?>

</html>