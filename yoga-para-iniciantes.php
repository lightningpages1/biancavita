<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <?php require('default/headerIniciantesVsl.php'); ?>
    <title>Yoga para Iniciantes</title>  
    <link rel="stylesheet" href="css/yogaIniciantes/preLoad.css">
    <link rel="stylesheet" id="delayedcss" href="css/yogaIniciantes/delayed.css" disabled>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-WZHV2GP');</script>
    <!-- End Google Tag Manager -->

    <!-- CLARITY -->
    <script type="text/javascript">
        (function(c,l,a,r,i,t,y){
            c[a]=c[a]||function(){(c[a].q=c[a].q||[]).push(arguments)};
            t=l.createElement(r);t.async=1;t.src="https://www.clarity.ms/tag/"+i;
            y=l.getElementsByTagName(r)[0];y.parentNode.insertBefore(t,y);
        })(window, document, "clarity", "script", "d2y9scrmph");
    </script>
    <!-- END CLARITY -->
</head>

<body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WZHV2GP"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <?php require('contents/yogaIniciante_content.php'); ?>
    <!-- Swiper JS -->
    <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>

    <!-- Initialize Swiper -->
    <script>
        var swiper = new Swiper(".mySwiper", {
            centeredSlides: true,
            loop: true,
            slidesPerView: "1",
            spaceBetween: 10,
            breakpoints: {
                765: {slidesPerView: "3"}
            },
            navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
            },
            autoplay: {
                slidesPerView: 1,
                disableOnInteraction: false,
            },
        });

        var swiperCellphone = new Swiper(".swiperCellphone", {
            grabCursor: true,
            centeredSlides: true,
            loop: true,
            slidesPerView: "1",
            navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
            },
            autoplay: {
                slidesPerView: 1,
                disableOnInteraction: false,
            },
        });
    </script>

    <script>
        let vw = window.innerWidth * 0.01;
        // document.documentElement.style.setProperty('--vh', `${vh}px`);
        document.documentElement.style.setProperty('--vw', `${vw}px`);
    </script>

    <!-- ANIMACOES -->
    <script>
        const observer = new IntersectionObserver(entries => {
            entries.forEach(entry => {
                if (entry.isIntersecting) {
                    if (entry.target.classList.contains('set-transition-down-up')) {
                        entry.target.classList.add('transition-down-up');
                    }
                }
            });
        });
        
        // OBERVERS TRANSITIONS
        observer.observe(document.querySelector('#setTransition'));

        // ReadyFunction
        document.onreadystatechange = function () {
            if (document.readyState == "interactive") {
                cssToActivate = document.getElementById('delayedcss')
                cssToActivate.removeAttribute('disabled');
            }
        }

    </script>

    <?php require('default/footer_v2.php'); ?>
    <script>
        (function() {
            const utmParamQueryString = new URLSearchParams(window.location.search);
            if (utmParamQueryString.toString()) {
                var navLinks = document.querySelectorAll('a');
                navLinks.forEach(function(item) {
                    if (item.href.indexOf('https://sun.eduzz.com') !== -1) {
                            if (item.href.indexOf('?') === -1) {
                                item.href += '?' + utmParamQueryString.toString();
                            } else {
                                item.href += '&' + utmParamQueryString.toString();
                            }
                    }
                });
            }
        })();
    </script>
</body>


</html>